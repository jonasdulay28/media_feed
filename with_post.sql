-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `geolocation` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-17 12:00:40','San Pablo'),(2,'LSPU Sta. Cruz','Sta Cruz','2018-03-17 12:04:59','2018-03-17 12:05:08','Sta. Cruz'),(3,'LSPU Siniloan','Siniloan','2018-03-18 11:09:35','0000-00-00 00:00:00','Siniloan');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('3grfn3k6aemaue1iau16itecbbdjgg68','::1',1521456301,'__ci_last_regenerate|i:1521456301;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gfpotdem2pnpngrnpt6h5d4dagvgcueu','::1',1521456882,'__ci_last_regenerate|i:1521456882;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('njm4bkjsl74dcsvgdk0ftoe5nhjfbpio','::1',1521457195,'__ci_last_regenerate|i:1521457195;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ivfir41pvdogjgrr4pvi7nad51v9igql','::1',1521457503,'__ci_last_regenerate|i:1521457503;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('hdjaef3uvcvjvmt16irbc65ddd34u6ts','::1',1521457817,'__ci_last_regenerate|i:1521457817;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('a5p5rdbs8p2n0gbkp84t03d834lmga17','::1',1521458139,'__ci_last_regenerate|i:1521458139;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('iao23o4tpql42mf1ab2vl8dofujgg2v3','::1',1521458490,'__ci_last_regenerate|i:1521458490;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('pbm64eb44j3glsn0fsc84ge836hi52fa','::1',1521458793,'__ci_last_regenerate|i:1521458793;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('34gipshqsj6rsmqe5olp1k0k83sp3mq2','::1',1521459097,'__ci_last_regenerate|i:1521459097;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('urn0d8i8eoe2r05vct7ku0bvomamj0qa','::1',1521459408,'__ci_last_regenerate|i:1521459408;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('fq3tl44p2cb2uqbb2erj4go8fnq5levq','::1',1521459745,'__ci_last_regenerate|i:1521459745;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('bbh0oaab1e9ff1cgufrms7iqmg528cva','::1',1521460112,'__ci_last_regenerate|i:1521460112;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('v8h6hqg4vf43e35ge45fhvihpdu3ucd8','::1',1521460817,'__ci_last_regenerate|i:1521460817;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('09cvv39dgbv9jhoo33o3d05e2461akmi','::1',1521461777,'__ci_last_regenerate|i:1521461777;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('cntvsad9tk1aftmhemiqvfl0gaiq3gve','::1',1521462110,'__ci_last_regenerate|i:1521462110;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('8nbadj07trpcgrc3uvsvivcje59mepbg','::1',1521462552,'__ci_last_regenerate|i:1521462552;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('m0iaqau9qggcosgv7gt103mng75thm2p','::1',1521462903,'__ci_last_regenerate|i:1521462903;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('cgg2rf51fv366e5ue3nh1c912b39t6dt','::1',1521463396,'__ci_last_regenerate|i:1521463396;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('27j6jtru4sjdo75j81hcvjmq69do6v6q','::1',1521464180,'__ci_last_regenerate|i:1521464180;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1eupg78i3a39hpefa6atg4lietkr655m','::1',1521465532,'__ci_last_regenerate|i:1521465532;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('r2lavu64pl6dlfav9jt31e5d0j6qpbdd','::1',1521464582,'__ci_last_regenerate|i:1521464582;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('5d0lia53d7vqelgba6q9r2lh5c24grcp','::1',1521465958,'__ci_last_regenerate|i:1521465958;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('e59611j1b9mi710gkbjok105d88q2ult','::1',1521466296,'__ci_last_regenerate|i:1521466296;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('itqolajjbtfopiulopmhdjfhhaqk3cfb','::1',1521466621,'__ci_last_regenerate|i:1521466621;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('lt0hbvbfe0l3t6nguf22h7ko0l06fphl','::1',1521467049,'__ci_last_regenerate|i:1521467049;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ba7eq6n7m4v22rh1c54ojmetdi79no32','::1',1521467484,'__ci_last_regenerate|i:1521467484;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ec1abhb48phi8sgcdm945m7lh3t4s686','::1',1521467868,'__ci_last_regenerate|i:1521467868;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('a7upqiksbudl6lihjl58otlvt4f37f7i','::1',1521468380,'__ci_last_regenerate|i:1521468380;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('tlo054r1esu7r1du29nutn4blh58s8h9','::1',1521468688,'__ci_last_regenerate|i:1521468688;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('jtaqck57vq167n8c47opm5a70dpru81i','::1',1521469068,'__ci_last_regenerate|i:1521469068;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('98pert8bebtshu8jr2mp4cf65jqom398','::1',1521469391,'__ci_last_regenerate|i:1521469391;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('60c17ehhv674dc3ngp8ndc61ati6mgg3','::1',1521469694,'__ci_last_regenerate|i:1521469694;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('7tegvf5ct70euppqcdghbv84n4ses8uf','::1',1521470043,'__ci_last_regenerate|i:1521470043;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('a5k8v7or1sokaa8hbfsdlle6g521u1vd','::1',1521470572,'__ci_last_regenerate|i:1521470572;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('0fcpf2uv62ufur2001ts3lmulfccihpk','::1',1521471117,'__ci_last_regenerate|i:1521471117;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ttvl9grj9ir732mvdhdcc3uk93vdbab8','::1',1521471836,'__ci_last_regenerate|i:1521471836;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('92eu3mk1bog0is88lo7brmaa0m1mpkt9','::1',1521472390,'__ci_last_regenerate|i:1521472390;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('umlfcnbam8jv5b1cri1b27u8c1uvd57e','::1',1521473329,'__ci_last_regenerate|i:1521473329;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('lsjr2up4kaejqpfduos2uq1kk5aj21b9','::1',1521474239,'__ci_last_regenerate|i:1521474239;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('sam90t6ogr915o5sjr3884cqpknm9s9a','::1',1521474799,'__ci_last_regenerate|i:1521474799;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('svhtdhvbb3vb9fnqqvvruk7utbipg500','::1',1521475112,'__ci_last_regenerate|i:1521475112;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ocjmtmjkkljl70la022md3nue8spualu','::1',1521475608,'__ci_last_regenerate|i:1521475608;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('7b616lfs1hbfg86jvc1j7hfrv23ht20p','::1',1521476032,'__ci_last_regenerate|i:1521476032;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('cga4rh2ecfubtacud54q6evaqgqdeu8p','::1',1521476342,'__ci_last_regenerate|i:1521476342;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('i58j6091u01h6hf0clsq7usqdv7s9s6p','::1',1521476739,'__ci_last_regenerate|i:1521476739;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('gvj674e72ne1tpbvrli2tm1qsphoom10','::1',1521476920,'__ci_last_regenerate|i:1521476739;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('n68nk67s2l6h96g0g9earthdvqg3g9gm','::1',1521502035,'__ci_last_regenerate|i:1521502035;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('cnujpbkq776587b0k1qsgc1pr8bjk6c9','::1',1521502888,'__ci_last_regenerate|i:1521502888;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('dfrhut9boi41uda7n1eksj2vi1ip0euf','::1',1521503418,'__ci_last_regenerate|i:1521503418;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('evq548f11gfu2higrn00u16p97f8gdvn','::1',1521503798,'__ci_last_regenerate|i:1521503798;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('rapmp3cc2tii4c84oaujuiik7cjjgian','::1',1521504107,'__ci_last_regenerate|i:1521504107;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('m7bnhul53803h231ltnua7as2jub8had','::1',1521504423,'__ci_last_regenerate|i:1521504423;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('lb7p5o3hefbb3nnp9i1esnqud3q0p3t1','::1',1521505161,'__ci_last_regenerate|i:1521505161;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('n4lvt8j21q2ugigkk9tkfvn8dqkpundv','::1',1521505734,'__ci_last_regenerate|i:1521505734;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('mt5hq1pki1ckg4kl87r4245th91qksmj','::1',1521506208,'__ci_last_regenerate|i:1521506208;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('2u1jrnkp87phancki4sdlt8i7q0sbl2v','::1',1521506534,'__ci_last_regenerate|i:1521506534;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('24pduphh78pks9ms06glspv739qjpmpo','::1',1521506978,'__ci_last_regenerate|i:1521506978;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ft7v5bdvv3ivuc7pah2gc1js93qti4vl','::1',1521507988,'__ci_last_regenerate|i:1521507988;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('pq8bp8m3s6mm850ab9fg8oqib0jppt69','::1',1521508389,'__ci_last_regenerate|i:1521508389;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('trcu05emg7s4edvtdpvuvtlhafkd5cvl','::1',1521508691,'__ci_last_regenerate|i:1521508691;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('kr5jvt2446ts4h5jrkpd6sg3ae0vr25s','::1',1521508820,'__ci_last_regenerate|i:1521508803;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Accounting Office','2018-03-17 13:19:18','0000-00-00 00:00:00'),(2,'Administration Office','2018-03-17 13:19:31','0000-00-00 00:00:00'),(3,'Cashier\'s Office','2018-03-17 13:20:03','0000-00-00 00:00:00'),(4,'College of Law','2018-03-17 13:20:15','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `forward` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `name` varchar(100) NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'Docx','8a3f9487e1d4d0ac96eae8ca19741c29.docx','','2,3','2018-03-19 12:50:05','0000-00-00 00:00:00','Test Doc',0,13),(2,'Image','9dec3faff52a5cb9f331b8acc4e5882e.png','','3','2018-03-19 12:50:24','0000-00-00 00:00:00','Test Image',0,13),(3,'Text','','aaaaa','2,3','2018-03-19 12:50:39','2018-03-19 23:52:34','Test Text',0,13),(4,'Pdf','477c7dd9e5a6053d07a35b3e087d6ac3.pdf','','2','2018-03-19 12:52:18','0000-00-00 00:00:00','Test Pdf',0,13);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_sub_admin`
--

DROP TABLE IF EXISTS `post_sub_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_sub_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_admin` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_sub_admin`
--

LOCK TABLES `post_sub_admin` WRITE;
/*!40000 ALTER TABLE `post_sub_admin` DISABLE KEYS */;
INSERT INTO `post_sub_admin` VALUES (1,12,3,'1,3','2018-03-20 00:41:11','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `post_sub_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(50) DEFAULT NULL,
  `campus_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `position` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-17 09:19:13',NULL,0,0,'',''),(12,'Sub Admin','Sub Admin','Sub Admin','Asdsadasd','123123','sub_admin@gmail.com','$2y$11$.OOHHWwJvkow0u12iSozEOkB674SAM9/fPqxRuw67GgWplN/J9WVq',2,1,'2018-03-19 12:35:53','2018-03-19 12:35:53','sub_admin',2,2,'','Staff'),(13,'Super Admin','Super Admin','Super Admin','Super Admin','12313','super_admin@gmail.com','$2y$11$MctlsLbJCmp4lqD6mRMeJ.PsaK0DebE0KZZ5RTVGrLzUEBpKSs4mW',1,1,'2018-03-19 12:48:47','2018-03-19 12:48:47','super_admin',3,1,'','Director'),(14,'Admin','Admin','Admin','Admin Admin','123414','admin1@gmail.com','$2y$11$jPTpVKxuJ4uVEy/4fg8jEuQN7YLNMupGFID1QcpbTxZ7JfbMo0mmm',3,1,'2018-03-19 12:53:21','2018-03-20 00:54:28','admin',3,3,'','Director'),(15,'Sub Admin 1','Sub Admin 1','Sub Admin 1','Super Admin 123123','12313123','sub_admin1@gmail.com','$2y$11$eOixCY7VpyntM2mLiItc5eROqCnZTedonG03eD1Qe1H/SY8UJ72JO',2,1,'2018-03-19 12:56:11','2018-03-19 12:56:11','sub_admin1',3,2,'','Staff');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-20  9:20:46
