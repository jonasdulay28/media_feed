-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `geolocation` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-17 12:00:40','San Pablo'),(2,'LSPU Sta. Cruz','SSta Cruz','2018-03-17 12:04:59','2018-03-20 05:25:50','Sta. Cruz'),(3,'LSPU Siniloan','Siniloan','2018-03-18 11:09:35','0000-00-00 00:00:00','Siniloan');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('to4gdkkaeltfc0kd5lu4hs0ljl0kqc64','::1',1521593497,'__ci_last_regenerate|i:1521593497;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('35dn3p3v3ak3ru4bui64dmeftro8mjg8','::1',1521593968,'__ci_last_regenerate|i:1521593968;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('t6pquu5uo46jg9jev0ls2ql8o7jvf644','::1',1521594329,'__ci_last_regenerate|i:1521594329;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ltlfgs8pt7gil0qldunmnob2d0jcfff8','::1',1521595518,'__ci_last_regenerate|i:1521595518;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('p5cgpl009vv7ku96iqq63iqmdc2embe2','::1',1521595836,'__ci_last_regenerate|i:1521595836;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('n4euerjs4ivtjjatvv5vubluhojvfpoh','::1',1521596138,'__ci_last_regenerate|i:1521596138;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('h8b7fgfp4aduj20j9e59m78otclv4307','::1',1521596802,'__ci_last_regenerate|i:1521596802;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('sdna0t7sj8ggdgsqv9olaqika07bnuqi','::1',1521597177,'__ci_last_regenerate|i:1521597177;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('rm53n28hqdata70n27ma6ongple2pegf','::1',1521599517,'__ci_last_regenerate|i:1521599517;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('2top093i5p42q2ktsbnroahv113lbqde','::1',1521599870,'__ci_last_regenerate|i:1521599870;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('m5dhte9bje9rhr8gq3ao50c3dfdteshm','::1',1521600231,'__ci_last_regenerate|i:1521600231;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('nkik2m7215a0sqakr6j8rb9snvctst3r','::1',1521601839,'__ci_last_regenerate|i:1521601839;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('85kogeonp6cph2omu02r4l27iicg6b4o','::1',1521603670,'__ci_last_regenerate|i:1521603670;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('0abbclabcjmmjb06o3m8nq3bt6cp4t34','::1',1521605770,'__ci_last_regenerate|i:1521605770;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('prpk09d5946mv4uvo94shvqh6of7oqi9','::1',1521606081,'__ci_last_regenerate|i:1521606081;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('k3s3t97b170fppsrpkuinut0k12uv7ev','::1',1521606571,'__ci_last_regenerate|i:1521606571;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('rl2ukfa8mkktgm3u03lj7uving507aa0','::1',1521606901,'__ci_last_regenerate|i:1521606901;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('7r3j4nnrnvajlgfn0b5aqdee462qvnn9','::1',1521607217,'__ci_last_regenerate|i:1521607217;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1u0o1en5cbgp6ccbomhd9ogfehguhsnv','::1',1521611516,'__ci_last_regenerate|i:1521611516;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('jjpunufehcnbb60q070o5vf89iabj9om','::1',1521612079,'__ci_last_regenerate|i:1521612079;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('jtajjq0n8q70r7g2qrnh7ld0amrhmdmd','::1',1521612406,'__ci_last_regenerate|i:1521612406;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('oou3qt93kaqhcv3mb4gpdhrhfo4agd8l','::1',1521612746,'__ci_last_regenerate|i:1521612746;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('7tsvp7hplm2706s808b9lvvc9ekrbhqf','::1',1521613192,'__ci_last_regenerate|i:1521613192;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('pcnausactnordbtpgt56kk5omlgi0fd8','::1',1521613495,'__ci_last_regenerate|i:1521613495;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('5ean7b3ento6unu2of790v6bu3cnoke5','::1',1521613806,'__ci_last_regenerate|i:1521613806;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('b6af5dttnhgtbten1b2eln29s2r2t1l4','::1',1521614125,'__ci_last_regenerate|i:1521614125;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gmk0paeod54hs5eg7m8fpul1ssg5ovt3','::1',1521614475,'__ci_last_regenerate|i:1521614475;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gq17tvoo1i4ep6ju5uhs0of3q2565riq','::1',1521614967,'__ci_last_regenerate|i:1521614967;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('l48b26cfiqgtfl88g3vpse673q6mg7eu','::1',1521615269,'__ci_last_regenerate|i:1521615269;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('nvqe3f401e3sdmlak8jv5586g7re10g9','::1',1521616051,'__ci_last_regenerate|i:1521616051;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('pt02lieaufi0ce1o39t6bm2fe74qdpfb','::1',1521615595,'__ci_last_regenerate|i:1521615373;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('vgr6optlvc7mvq4int8eanudur9m0qvp','::1',1521616867,'__ci_last_regenerate|i:1521616867;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('dsrlipp6s3431ja700jr67hjgftia1cg','::1',1521616896,'__ci_last_regenerate|i:1521616896;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('2fbljopiiedlopepo0sn1nj8hfv7sn56','::1',1521617479,'__ci_last_regenerate|i:1521617479;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('13k1cccmlo7nfi5v5pgaid79ukbb419a','::1',1521617299,'__ci_last_regenerate|i:1521617299;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1q2l1svm2v3lkttsdnf96rtjo7j93vfj','::1',1521617636,'__ci_last_regenerate|i:1521617636;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('it0eq5b7dmqbii62nm96mda74cus1ue2','::1',1521617870,'__ci_last_regenerate|i:1521617870;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('tv46gi7nqf5l3baoid7k4hudlib5p6k0','::1',1521618552,'__ci_last_regenerate|i:1521618552;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('t4jgggu2lk1cv5rb0lc3v1ehudl3lacc','::1',1521618325,'__ci_last_regenerate|i:1521618325;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('9lbjmo7el6df8rm3dgot2h64kh8kv0fg','::1',1521619283,'__ci_last_regenerate|i:1521619283;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('f824fhbqpm5qd5v7i69u4bn9bu3i0f7t','::1',1521618862,'__ci_last_regenerate|i:1521618862;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('qjm8sn3gg3tq2fcv7eimleujc2br0re3','::1',1521619233,'__ci_last_regenerate|i:1521619233;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('5cfuqt1oorpcfb3rhe9dpkc2accb6h8g','::1',1521619542,'__ci_last_regenerate|i:1521619542;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('01cfms0rte4podhfc11okoi059dhobg6','::1',1521619593,'__ci_last_regenerate|i:1521619593;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('k0efetomig6dbu8fm0hb6868c6a4jev4','::1',1521620035,'__ci_last_regenerate|i:1521620035;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('6fqot5lgs4osahb65bj3n7s31hon6bvo','::1',1521620305,'__ci_last_regenerate|i:1521620305;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('l1v93kf5mqd6mp4ai4lv7k1714jdudvi','::1',1521620337,'__ci_last_regenerate|i:1521620337;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('kfc6uum8b5ckfrh2qi2uko234n3l74kv','::1',1521620607,'__ci_last_regenerate|i:1521620607;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('pvcabr8pjd9u0k5deg2ea04lr4aifcld','::1',1521621389,'__ci_last_regenerate|i:1521621389;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('sr6cut5e8bef0r429fkmtin49e9vh1ve','::1',1521620962,'__ci_last_regenerate|i:1521620962;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('75mlthe4mtc76betfr7m152cea05o0cj','::1',1521621314,'__ci_last_regenerate|i:1521621314;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('e36n00f4e0f5e96acsr4mpe9n8kvs5as','::1',1521621619,'__ci_last_regenerate|i:1521621619;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('rv0msrmmd9k4qcnbq5bing66ab54o81f','::1',1521621892,'__ci_last_regenerate|i:1521621892;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('a09omm62e0cmnbch94g6j8msvj1do3sj','::1',1521621921,'__ci_last_regenerate|i:1521621921;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('bmi24ipdl4dul583p9thulm89imj7qdu','::1',1521622225,'__ci_last_regenerate|i:1521622225;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('c6ugds6jpfjmls3q74k1c5ci39fjq8id','::1',1521622229,'__ci_last_regenerate|i:1521622229;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('n995p5lpm91t9tni58564di1hkknhasl','::1',1521622711,'__ci_last_regenerate|i:1521622711;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('38l0hpvpbdbc29mu7q1k71mtvmjlmk37','::1',1521622710,'__ci_last_regenerate|i:1521622710;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('gmlbbj5ph7s2afefuv3rdk1dkol5gchb','::1',1521623204,'__ci_last_regenerate|i:1521623204;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('f47926dtdng8r4l0pusfjp5d9nd0io0k','::1',1521623196,'__ci_last_regenerate|i:1521623196;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('8hen26pkibvs046v4tmcirjilr0053h7','::1',1521623850,'__ci_last_regenerate|i:1521623850;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('qge7bvv9rqvva0hj58cm3ijcieocslja','::1',1521623861,'__ci_last_regenerate|i:1521623861;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('f2govcagv7s3bpiantrhoa10ucan59o4','::1',1521624173,'__ci_last_regenerate|i:1521624173;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('7ol23ssklu178dmjold2569plp6tp3cj','::1',1521624562,'__ci_last_regenerate|i:1521624562;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('bo6jinv937jsncb5g8qocesutbknf7op','::1',1521624549,'__ci_last_regenerate|i:1521624549;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('mnl56e8ebp4t3rvo1auakrtjveh4427o','::1',1521624944,'__ci_last_regenerate|i:1521624944;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('s8p1ilan7t4resp2b7abr2pco9f1d57p','::1',1521624955,'__ci_last_regenerate|i:1521624955;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('l42tm0vdsdl6s62f9sd2refk2drn05tr','::1',1521625355,'__ci_last_regenerate|i:1521625355;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('rot2dfm881o9d5u22f4hg5h13gunkq96','::1',1521625334,'__ci_last_regenerate|i:1521625334;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('6ril7umr5d6j142foa6rf8gbhgviki2a','::1',1521625707,'__ci_last_regenerate|i:1521625707;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('31uon3sir5sb2kllcaofe7lln5dmkr6u','::1',1521625704,'__ci_last_regenerate|i:1521625704;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('jrksipj5gcbvcpsd5j52uv1kq6b7gsn4','::1',1521627265,'__ci_last_regenerate|i:1521627265;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('tso785h9sesdv7ausaga913d4g0c1dsd','::1',1521626240,'__ci_last_regenerate|i:1521626240;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('5b1ij23rmldm961p0tnqh3edg2ttdeso','::1',1521626691,'__ci_last_regenerate|i:1521626691;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('5cplq7h21k8areoibkgu5ooakf12haa3','::1',1521627308,'__ci_last_regenerate|i:1521627308;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qm19jl1gmgvlbfjmi41krfd4cuhbuj04','::1',1521629139,'__ci_last_regenerate|i:1521629139;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('kdi8fjqtfda7lv6jh5ffk8b3toge32lv','::1',1521628976,'__ci_last_regenerate|i:1521628976;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('r6ed9hpot2r5cpthl62jpnrvnj22vneg','::1',1521629314,'__ci_last_regenerate|i:1521629314;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('p02f4d96imc5qjf1v4h488lvtpiv1edn','::1',1521629499,'__ci_last_regenerate|i:1521629499;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1ep946slajcvea0iitgt52gu9hjmk879','::1',1521629641,'__ci_last_regenerate|i:1521629641;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('iocl4mbccm3ja4omsdau3a5rmfm9el9p','::1',1521629802,'__ci_last_regenerate|i:1521629802;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('qr7g9ulr13g1guhp3urns7234o43e8b5','::1',1521629947,'__ci_last_regenerate|i:1521629947;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('6sp4l21q3q9ubpofludvgagf277vdvo2','::1',1521630292,'__ci_last_regenerate|i:1521630292;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('i65ik1n07gaulg6ujim4jfs1u5mbip8k','::1',1521630302,'__ci_last_regenerate|i:1521630302;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qvnh9bb3duieet1qmgiln8avnl30v4a1','::1',1521630512,'__ci_last_regenerate|i:1521630292;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('5kku6uvmctvv8r436o5e6sqd2ij8kic8','::1',1521631192,'__ci_last_regenerate|i:1521631192;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('2vs8qb9mt777l7c300mkjb5h0lsi8k51','::1',1521631454,'__ci_last_regenerate|i:1521631192;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_to` int(11) NOT NULL,
  `message_from` int(11) NOT NULL,
  `message` text NOT NULL,
  `message_file` varchar(100) DEFAULT NULL,
  `ticket` varchar(50) NOT NULL,
  `status` tinyint(2) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status_from` tinyint(2) DEFAULT NULL,
  `status_to` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,16,13,'asd','','7293184560',0,'2018-03-21 11:07:55',1,1),(2,16,13,'asdadasd','','7293184560',0,'2018-03-21 11:08:28',1,1);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Accounting Office','2018-03-17 13:19:18','0000-00-00 00:00:00'),(2,'Administration Office','2018-03-17 13:19:31','0000-00-00 00:00:00'),(3,'Cashier\'s Office','2018-03-17 13:20:03','0000-00-00 00:00:00'),(4,'College of Law','2018-03-17 13:20:15','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `forward` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `name` varchar(100) NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'Docx','8a3f9487e1d4d0ac96eae8ca19741c29.docx','','2,3','2018-03-19 12:50:05','2018-03-20 06:01:09','Test Doc',1,13,0),(2,'Image','9dec3faff52a5cb9f331b8acc4e5882e.png','','3','2018-03-19 12:50:24','2018-03-20 06:01:09','Test Image',1,13,1),(3,'Text','','aaaaa','2,3','2018-03-19 12:50:39','2018-03-20 06:01:09','Test Text',1,13,1),(4,'Pdf','477c7dd9e5a6053d07a35b3e087d6ac3.pdf','','2','2018-03-19 12:52:18','2018-03-20 06:01:09','Test Pdf',1,13,0),(5,'Text','','12313123123','6','2018-03-20 06:58:03','0000-00-00 00:00:00','Hello World',1,14,1),(8,'Text','','312312312312','4,5,6','2018-03-20 07:01:57','0000-00-00 00:00:00','Admin Post',1,14,1),(9,'Text','','2313123123','2,3,4','2018-03-20 07:13:53','0000-00-00 00:00:00','Sub Admin Test',1,12,0),(10,'','','asdadasdasd','1,2,3','2018-03-20 09:48:25','0000-00-00 00:00:00','Aasdasdasd',1,13,1),(11,'Pdf','363b17388ebbba8f6bbb224bfa98ea6f.pdf','pdf \r\npdf \r\npdf pdf pdf','2','2018-03-20 09:53:47','0000-00-00 00:00:00','Pdf',1,13,0);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_admin`
--

DROP TABLE IF EXISTS `post_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_staff` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_admin`
--

LOCK TABLES `post_admin` WRITE;
/*!40000 ALTER TABLE `post_admin` DISABLE KEYS */;
INSERT INTO `post_admin` VALUES (1,14,4,'4,5','2018-03-20 05:53:09','2018-03-20 06:37:47'),(2,14,8,'4,5,6','2018-03-20 07:01:57','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `post_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_sub_admin`
--

DROP TABLE IF EXISTS `post_sub_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_sub_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_admin` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_sub_admin`
--

LOCK TABLES `post_sub_admin` WRITE;
/*!40000 ALTER TABLE `post_sub_admin` DISABLE KEYS */;
INSERT INTO `post_sub_admin` VALUES (1,12,4,'2,3','2018-03-20 02:43:19','2018-03-20 03:36:09'),(2,12,1,'2,4','2018-03-20 02:48:27','2018-03-20 05:36:22');
/*!40000 ALTER TABLE `post_sub_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(50) DEFAULT NULL,
  `campus_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `position` varchar(30) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-20 08:19:05',NULL,0,0,'','','default.png'),(12,'Sub Admin','Sub Admin','Sub Admin','Asdsadasd','123123','sub_admin@gmail.com','$2y$11$.OOHHWwJvkow0u12iSozEOkB674SAM9/fPqxRuw67GgWplN/J9WVq',2,1,'2018-03-19 12:35:53','2018-03-20 08:19:05','sub_admin',2,2,'','Staff','default.png'),(13,'Super Admin','Super Admin','Super Admin','Super Admin','12313','super_admin@gmail.com','$2y$11$MctlsLbJCmp4lqD6mRMeJ.PsaK0DebE0KZZ5RTVGrLzUEBpKSs4mW',1,1,'2018-03-19 12:48:47','2018-03-20 08:19:05','super_admin',3,1,'','Director','default.png'),(14,'Admin','Admin','Admin','Admin Admin','123414','admin1@gmail.com','$2y$11$jPTpVKxuJ4uVEy/4fg8jEuQN7YLNMupGFID1QcpbTxZ7JfbMo0mmm',3,1,'2018-03-19 12:53:21','2018-03-20 08:19:05','admin',3,3,'','Director','default.png'),(15,'Sub Admin 1','Sub Admin 1','Sub Admin 1','Super Admin 123123','12313123','sub_admin1@gmail.com','$2y$11$eOixCY7VpyntM2mLiItc5eROqCnZTedonG03eD1Qe1H/SY8UJ72JO',2,1,'2018-03-19 12:56:11','2018-03-20 08:19:05','sub_admin1',3,2,'','Staff','default.png'),(16,'Admin2','Admin2','Admin2','Admin2','111111','admin2@gmail.com','$2y$11$dJxN73fHZvyNEcdgzrgxGOxXIWCFC6tDH0Gdg/diZB7XkVLvnnGYC',3,1,'2018-03-20 05:33:21','2018-03-20 08:19:05','123123123',2,1,'','Associate Dean','default.png'),(17,'Staff','Staff','Staff','Staffasdad','123123','staff@gmail.com','$2y$11$9awbNXhCM96ZQihdJ8FONuFpkGvUH8kbDW1usWFAWcod3Mm9DpeKO',6,1,'2018-03-20 06:03:58','2018-03-20 08:19:05','staff',2,1,'','Dean','default.png'),(18,'Staff2','Staff2','Staff2','Staff2','123121323','staff2@gmail.com','$2y$11$fc3Wushz7OFH5f4lq/CmEeIsT/q2fZO0PtHdrtJQx0JR45oumiwbq',6,1,'2018-03-20 06:05:12','2018-03-20 08:19:05','staff2',3,4,'','Dean','default.png'),(21,'Test_image','Test_image','Test_image','12313','12313123','test_image@gmail.com','$2y$11$I1HWnu4Ym68PROSKeYuLa.3B4qFxL2sKafUuZQjiXTZIdelgEuRfy',1,1,'2018-03-20 08:18:46','2018-03-20 08:18:46','test_image',1,1,'','Student','default.png'),(22,'Test_image1','Test_image1','Test_image1','Test_image1','12313','test_image1@mail.com','$2y$11$y6PhNc1U5q4qIkRftbFCoel6PJikZyciAMcs8.yZaa1Th.U4FvlfC',1,1,'2018-03-20 08:23:55','2018-03-20 08:23:55','test_image1',1,1,'','Student','683d7aba961b310bfce2ca7ec088dd1f.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-21 19:26:37
