-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `campus` varchar(100) NOT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `office` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
INSERT INTO `audit_trails` VALUES (1,'Login','Sub Admin, Sub Admin S.  logged in',12,'::1','LSPU Sta. Cruz','2','2018-03-25 09:39:19','Administration Office'),(2,'Login','Super Admin, Super Admin S.  logged in',13,'::1','LSPU Siniloan','1','2018-03-25 09:39:31','Accounting Office'),(3,'Login','Admin, Admin A.  logged in',14,'::1','LSPU Sta. Cruz','3','2018-03-25 09:39:50','Cashier\'s Office'),(4,'Login','Super Admin, Super Admin S.  logged in',13,'::1','LSPU Siniloan','1','2018-03-25 09:40:03','Accounting Office');
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `contact_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-22 09:12:12','111111'),(2,'LSPU Sta. Cruz','SSta Cruz','2018-03-17 12:04:59','2018-03-22 09:12:06','123'),(3,'LSPU Siniloan','Siniloan','2018-03-18 11:09:35','2018-03-22 09:12:20','51'),(4,'LSPU - HELLO','HELLO','2018-03-22 09:32:10','2018-03-22 09:32:24','555');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('39e65bq2cscnu6i4v0op1egmugpbur26','::1',1521898438,'__ci_last_regenerate|i:1521898438;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('5jcf92fbifi59d7of84bg0vk1lptggs6','::1',1521898808,'__ci_last_regenerate|i:1521898808;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('qkqckgf67l62iaebqe70crd59dm3i48t','::1',1521899350,'__ci_last_regenerate|i:1521899350;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('2jpovv3bqsqdpidhp79mof6rjlg5p4a7','::1',1521899679,'__ci_last_regenerate|i:1521899679;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('74opk79iml74pntt2su00i989h0g2vre','::1',1521899980,'__ci_last_regenerate|i:1521899980;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('sda6qrt3gc52q92oo8nbgs886sq7nvgf','::1',1521900446,'__ci_last_regenerate|i:1521900446;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('qc45hptnm1rgqv3k24g36jlfh4guoovh','::1',1521901610,'__ci_last_regenerate|i:1521901607;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('bk18s4380c1bkikdu4nb7l6dk837s0b7','::1',1521901980,'__ci_last_regenerate|i:1521901980;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('mllu8vusjfqlp6hmgth7r88fk15tpc9n','::1',1521902340,'__ci_last_regenerate|i:1521902340;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('jtc9ifnmns0pro1tvi966nsesem2hcbu','::1',1521902765,'__ci_last_regenerate|i:1521902765;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('un2vjp7n87lbrvmn7r584ss4opp1cb5t','::1',1521903134,'__ci_last_regenerate|i:1521903134;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('r78mn2v6is698425bgg783vht6n7islq','::1',1521903954,'__ci_last_regenerate|i:1521903954;id|s:32:\"TStYL0JVdC9uWUlMcTlZUm5sSzZ3UT09\";name|s:60:\"aDVxRC9nWFJqY2U0disxTExYQTdCK0Q4Wm9iS2ZoYmRFb3pSa0F2YzNCRT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('gipdt8hgeg48smkpvq3pgv2blprne5fm','::1',1521904888,'__ci_last_regenerate|i:1521904888;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qpivqkklki9qm72rst6u2f80s5k80pat','::1',1521905792,'__ci_last_regenerate|i:1521905792;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('oebqd3kvqp6hmq28v7angmckdke7320s','::1',1521906145,'__ci_last_regenerate|i:1521906145;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('2racmu9mpmtd6rjmb5g7hjfiuf3kehhs','::1',1521906507,'__ci_last_regenerate|i:1521906507;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('vtj3p5ba4r0grbnopu0dnhuskc6e7t5t','::1',1521906929,'__ci_last_regenerate|i:1521906929;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('k1bqv3hi3cn87fip5cdju6u0j81piu03','::1',1521907234,'__ci_last_regenerate|i:1521907234;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('i1qf06t4f3mnq040ho8s3r4thfle0ean','::1',1521907535,'__ci_last_regenerate|i:1521907535;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('2flh3j56vl8k9aahr19nl4afv60rfvqo','::1',1521907916,'__ci_last_regenerate|i:1521907916;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ok7pm1d6pog1lgtnsjoo35vqihto1l2r','::1',1521908218,'__ci_last_regenerate|i:1521908218;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ouf0qbk1onla5k2ah6ccj335uevtsccl','::1',1521908218,'__ci_last_regenerate|i:1521908218;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('nntpt8dog4vq97f5pmj5o895lt0ulp02','::1',1521968239,'__ci_last_regenerate|i:1521968239;'),('oeav6od61kejirmuol1gfies7rvnqc60','::1',1521969000,'__ci_last_regenerate|i:1521969000;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('5ffb3fvsulcprhjd4ib3r5u75l8lt16p','::1',1521969393,'__ci_last_regenerate|i:1521969393;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gquvbh3kf8oubcgaghtcnorjava1jo76','::1',1521970813,'__ci_last_regenerate|i:1521970813;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_to` int(11) NOT NULL,
  `message_from` int(11) NOT NULL,
  `message` text NOT NULL,
  `message_file` varchar(100) DEFAULT NULL,
  `ticket` varchar(50) NOT NULL,
  `status` tinyint(2) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status_from` tinyint(2) DEFAULT NULL,
  `status_to` tinyint(2) DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,16,13,'ANG BONAK NG TEAMVIEWER','','5943271068',1,'2018-03-21 17:03:46',NULL,NULL,'Bonak TeamViewer','2018-03-21 18:06:41'),(2,13,16,'mag rereply ako bonak talaga ng team viewer!','','5943271068',1,'2018-03-21 17:06:22',0,1,'Bonak TeamViewer','2018-03-21 17:06:53'),(3,16,13,'test 123','','5943271068',1,'2018-03-21 17:08:23',1,1,'Bonak TeamViewer','2018-03-21 17:41:54'),(4,13,16,'latest na to!','','5943271068',1,'2018-03-21 17:42:02',0,1,'Bonak TeamViewer','2018-03-21 17:42:12'),(5,16,12,'new message','','9572016348',1,'2018-03-21 17:42:53',NULL,NULL,'new subject','2018-03-21 19:30:33'),(6,12,16,'rep','','9572016348',1,'2018-03-21 19:30:37',0,1,'new subject','2018-03-22 01:29:46'),(7,16,12,'asdadasdasdad','','9572016348',1,'2018-03-22 01:29:55',1,1,'new subject','2018-03-22 01:30:54'),(8,12,16,'asd123','','9572016348',1,'2018-03-22 01:31:05',0,1,'new subject','2018-03-22 01:34:12'),(9,13,16,'asdadada123','','5943271068',1,'2018-03-22 01:34:07',0,1,'Bonak TeamViewer','2018-03-22 01:56:41'),(10,13,16,'11111111111111111111111111111','','5943271068',1,'2018-03-22 01:34:22',0,1,'Bonak TeamViewer','2018-03-22 01:56:41'),(11,12,16,'1312313213','','9572016348',1,'2018-03-22 01:34:47',0,1,'new subject','2018-03-22 01:34:49'),(12,12,16,'asdasdasdasdasdasd','','9572016348',1,'2018-03-22 01:35:00',0,1,'new subject','2018-03-24 11:41:31'),(13,12,16,'zxcxczcxz','','9572016348',1,'2018-03-22 01:35:10',0,1,'new subject','2018-03-24 11:41:31'),(14,12,16,'hoy!!!!','','9572016348',1,'2018-03-22 01:35:29',0,1,'new subject','2018-03-24 11:41:31'),(15,22,13,'<p>asdasdasdasdas</p>','','9358120467',0,'2018-03-22 01:48:20',NULL,NULL,'asdasdasdasd','2018-03-22 01:48:20'),(16,17,13,'<p>asdadasgasagasdadqe</p>','','4905687321',1,'2018-03-22 02:09:08',NULL,NULL,'12313','2018-03-22 02:23:04'),(17,22,13,'123123123123','','9358120467',0,'2018-03-22 02:16:38',1,1,'asdasdasdasd','2018-03-22 02:16:38'),(18,13,16,'hoy','','5943271068',1,'2018-03-22 02:18:18',0,1,'Bonak TeamViewer','2018-03-22 02:20:21'),(19,13,16,'zzxdsad','','5943271068',1,'2018-03-22 02:19:39',0,1,'Bonak TeamViewer','2018-03-22 02:20:21'),(20,16,13,'problema mo','','5943271068',1,'2018-03-22 02:20:30',1,1,'Bonak TeamViewer','2018-03-22 02:33:30'),(21,22,13,'123131','','9358120467',0,'2018-03-22 02:20:46',1,1,'asdasdasdasd','2018-03-22 02:20:46'),(22,17,16,'<p><b>1231313</b></p>','MEDIA-FEED-SYSTEM.docx','1974823605',1,'2018-03-22 02:28:27',NULL,NULL,'hoy!','2018-03-22 02:28:34'),(23,18,16,'<p>123123123123</p>','','6752013849',0,'2018-03-22 02:33:56',NULL,NULL,'12313123123','2018-03-22 02:33:56'),(24,17,16,'<p>123123123123</p>','','3549108267',1,'2018-03-22 02:34:07',NULL,NULL,'12512515','2018-03-22 02:37:52'),(25,17,16,'<p>asdasdasdasd</p>','','2863149507',0,'2018-03-22 02:41:19',NULL,NULL,'new message','2018-03-22 02:41:19'),(26,17,16,'<p>asdadasd</p>','','1275386490',1,'2018-03-22 02:41:37',NULL,NULL,'newa again','2018-03-22 02:42:56'),(27,17,13,'<p>12313123</p>','','5026497183',0,'2018-03-22 02:42:30',NULL,NULL,'HOY! NEW MESSAGE','2018-03-22 02:42:30'),(28,17,13,'<p>asdadasdasdasd</p>','','2960457138',0,'2018-03-22 02:55:44',NULL,NULL,'TEST 2 MESSAGE','2018-03-22 02:55:44'),(29,17,12,'<p>asdadasdas</p>','','5893041627',1,'2018-03-24 11:54:37',NULL,NULL,'staff@gmail.com','2018-03-24 11:55:14'),(30,17,12,'<p>12313</p>','','1439762850',1,'2018-03-24 11:55:04',NULL,NULL,'1123','2018-03-24 11:55:10');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Accounting Office','2018-03-17 13:19:18','0000-00-00 00:00:00'),(2,'Administration Office','2018-03-17 13:19:31','0000-00-00 00:00:00'),(3,'Cashier\'s Office','2018-03-17 13:20:03','2018-03-24 16:04:15'),(4,'College of Law','2018-03-17 13:20:15','0000-00-00 00:00:00'),(5,'Abc Sad','2018-03-24 16:04:31','2018-03-24 16:06:51'),(6,'Zxczc','2018-03-24 16:06:06','2018-03-24 16:06:21');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `forward` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `name` varchar(100) NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'Text','','asdasdsadas','2','2018-03-13 03:44:33','2018-03-24 14:12:18','Test Sta Cruz',0,13,1);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_admin`
--

DROP TABLE IF EXISTS `post_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_staff` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_admin`
--

LOCK TABLES `post_admin` WRITE;
/*!40000 ALTER TABLE `post_admin` DISABLE KEYS */;
INSERT INTO `post_admin` VALUES (1,14,1,'5','2018-03-24 04:10:52','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `post_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_sub_admin`
--

DROP TABLE IF EXISTS `post_sub_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_sub_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_admin` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_sub_admin`
--

LOCK TABLES `post_sub_admin` WRITE;
/*!40000 ALTER TABLE `post_sub_admin` DISABLE KEYS */;
INSERT INTO `post_sub_admin` VALUES (1,12,1,'3','2018-03-24 03:44:55','0000-00-00 00:00:00'),(2,14,1,'2','2018-03-24 04:06:08','0000-00-00 00:00:00'),(3,14,1,'5','2018-03-24 04:09:25','0000-00-00 00:00:00'),(4,14,1,'5','2018-03-24 04:10:04','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `post_sub_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(50) DEFAULT NULL,
  `campus_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `position` varchar(30) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-20 08:19:05',NULL,0,0,'','','default.png'),(12,'Sub Admin','Sub Admin','Sub Admin','Asdsadasd','123123','sub_admin@gmail.com','$2y$11$.OOHHWwJvkow0u12iSozEOkB674SAM9/fPqxRuw67GgWplN/J9WVq',2,1,'2018-03-19 12:35:53','2018-03-20 08:19:05','sub_admin',2,2,'','Staff','default.png'),(13,'Super Admin','Super Admin','Super Admin','Super Admin','12313','super_admin@gmail.com','$2y$11$MctlsLbJCmp4lqD6mRMeJ.PsaK0DebE0KZZ5RTVGrLzUEBpKSs4mW',1,1,'2018-03-19 12:48:47','2018-03-20 08:19:05','super_admin',3,1,'','Director','default.png'),(14,'Admin','Admin','Admin','Admin Admin','123414','admin1@gmail.com','$2y$11$jPTpVKxuJ4uVEy/4fg8jEuQN7YLNMupGFID1QcpbTxZ7JfbMo0mmm',3,1,'2018-03-19 12:53:21','2018-03-24 03:50:55','admin',2,3,'','Director','default.png'),(15,'Sub Admin 1','Sub Admin 1','Sub Admin 1','Super Admin 123123','12313123','sub_admin1@gmail.com','$2y$11$eOixCY7VpyntM2mLiItc5eROqCnZTedonG03eD1Qe1H/SY8UJ72JO',2,1,'2018-03-19 12:56:11','2018-03-20 08:19:05','sub_admin1',3,2,'','Staff','default.png'),(16,'Admin2','Admin2','Admin2','Admin2','111111','admin2@gmail.com','$2y$11$dJxN73fHZvyNEcdgzrgxGOxXIWCFC6tDH0Gdg/diZB7XkVLvnnGYC',3,1,'2018-03-20 05:33:21','2018-03-20 08:19:05','123123123',2,1,'','Associate Dean','default.png'),(17,'Staff','Staff','Staff','Staffasdad','123123','staff@gmail.com','$2y$11$9awbNXhCM96ZQihdJ8FONuFpkGvUH8kbDW1usWFAWcod3Mm9DpeKO',5,1,'2018-03-20 06:03:58','2018-03-24 04:40:58','staff',2,3,'','Dean','default.png'),(18,'Staff2','Staff2','Staff2','Staff2','123121323','staff2@gmail.com','$2y$11$fc3Wushz7OFH5f4lq/CmEeIsT/q2fZO0PtHdrtJQx0JR45oumiwbq',5,1,'2018-03-20 06:05:12','2018-03-24 04:13:54','staff2',3,4,'','Dean','default.png'),(21,'Test_image','Test_image','Test_image','12313','12313123','test_image@gmail.com','$2y$11$I1HWnu4Ym68PROSKeYuLa.3B4qFxL2sKafUuZQjiXTZIdelgEuRfy',4,1,'2018-03-20 08:18:46','2018-03-22 03:51:44','test_image',3,3,'','Student','default.png'),(22,'Test_image1','Test_image1','Test_image1','Test_image1','12313','test_image1@mail.com','$2y$11$y6PhNc1U5q4qIkRftbFCoel6PJikZyciAMcs8.yZaa1Th.U4FvlfC',1,1,'2018-03-20 08:23:55','2018-03-20 08:23:55','test_image1',1,1,'','Student','683d7aba961b310bfce2ca7ec088dd1f.jpg'),(24,'Sta_cruz_user_only','Sta_cruz_user_only','Sta_cruz_user_only','Sta_cruz_user_only','1241414124','sta_cruz_user_only@gmail.com','$2y$11$cqHo6BxN3ckzQovItfhuOOwUeUlFByS0FzqsJXbfc7E5N57hJDMry',4,1,'2018-03-24 14:57:37','2018-03-24 14:57:37','sta_cruz_user_only',2,1,'','Student','default.png');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-25 17:40:36
