<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User {

	protected $CI;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->model('Crud_model');
    }

    public function info($columnName) {

    	$sessId = $this->CI->session->id;
    	$decryptId = decrypt($sessId);

    	$where = ['users.id' => $decryptId];
    	$userinfo = $this->CI->Crud_model->joinTagRow('*,users.created_at AS u_created, campus.created_at AS c_created','users',$where,'','','campus','users.campus_id = campus.id','inner','');
    	
    	if(!$userinfo == NULL):
    		return $userinfo->$columnName;
    	endif;
    }

    public function getUserAddress() {

        $sessId = $this->CI->session->id;
        $decryptId = decrypt($sessId);

        $where = ['users.id' => $decryptId];
        $userinfo = $this->CI->Crud_model->fetch_tag_row('address','users',$where);
        
        if(!$userinfo == NULL):
            return $userinfo->address;
        endif;
    }

    public function getUserContact() {

        $sessId = $this->CI->session->id;
        $decryptId = decrypt($sessId);

        $where = ['users.id' => $decryptId];
        $userinfo = $this->CI->Crud_model->fetch_tag_row('contact_number','users',$where);
        
        if(!$userinfo == NULL):
            return $userinfo->contact_number;
        endif;
    }

}