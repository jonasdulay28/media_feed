  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Login Logs
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url().'Dashboard' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        
        <li class="active"> Login Logs</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Users Records</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table id="audit" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Role</th>
                        <th>Campus</th>
                        <th>Office / Department</th>
                        <th>Date and Time</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Description</th>
                        <th>Role</th>
                        <th>Campus</th>
                        <th>Office / Department</th>
                        <th>Date and Time</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Audit Trails
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>


        <script>
          function get_audit()
          {
             $('#audit').DataTable({
                  "ajax": "<?php echo base_url('audit/getRecords')?>",
                  "deferRender": true,
                  "stateSave": true,
                  "order":[],/*
                  "columnDefs": [ {
                  "targets": 8,
                  "orderable": false
                  },*/
                  "pagingType": "full_numbers",
                  "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                  ],
                  responsive: true,
                  language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search Audit",
                  }

              });
          }

          $(function() {
            get_audit();
          })
        </script>