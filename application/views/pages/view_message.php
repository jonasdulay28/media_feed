
<link rel="stylesheet" href="<?= base_url().'assets/css/bootstrap-select.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/css/bootstrap3-wysihtml5.min.css' ?>">
<script src="<?= base_url().'assets/js/bootstrap-select.min.js' ?>"></script>
<script src="<?= base_url().'assets/js/bootstrap3-wysihtml5.all.min.js' ?>"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Read Mail
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url().'Dashboard' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Read Mail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
            <?php  if(decrypt($this->session->role) == 1 || decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3): ?>
          <a href="<?= base_url().'message/create' ?>" class="btn btn-primary btn-block margin-bottom">Compose</a>
          <?php endif; ?>

           <?php $this->load->view('includes/messagebar');?>
         
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header">
              <div class="box-tools">
                 <?php  if(decrypt($this->session->role) == 1 || decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3): ?>
                <button type="button" id="btn-reply" class="btn btn-primary"><i class="fa fa-reply"></i> Reply</button>
            <?php endif ?>
              </div>
            </div>
            <!-- /.box-header -->
            <?php foreach($messages as $message):  
                 $where = ['id' => $message->message_from];
                $get_from = $this->Crud_model->fetch_Tag_row('*','users',$where);?>
            <div class="box-body" id="reply-body" hidden>
                 <?php echo form_open_multipart('','method="POST" id="add_message"') ?>
              <div class="form-group">
                <input type="hidden" name="ticket" value="<?= $this->uri->segment(3); ?>">
                    <input type="hidden" id="uid" name="uid">
                    <input type="text" name="username" id="username" class="form-control" value="<?= $get_from->email_address ?>">
                    <div id="suggestions">
                        <div id="search-result">
                        </div>
                    </div>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Subject:" name="subject" value="<?= $message->subject ?>">
              </div>
              <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 300px"  name="message"></textarea>
              </div>
              <div class="form-group">
                  <i class="fa fa-paperclip"></i> Attachment
                  <input type="file" name="message_file" id=="message_file" class="form-control">
                </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
               <?php echo form_close(); ?>
            </div>
        </div>
        <hr>
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                
                <h3><?= $message->subject ?></h3>
                <h5>From: <?= $get_from->email_address ?>
                  <span class="mailbox-read-time pull-right"><?php 
                  $created_at = strtotime($message->created_at);
                      $now = time();

                      echo timespan($created_at, $now) . ' ago';
                  ?></span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                
                       <?php echo $message->message;
                      ?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <ul class="mailbox-attachments clearfix">
                <?php if($message->message_file != null): ?>
                <li>
                  <span class="mailbox-attachment-icon"><i class="fa fa-file"></i></span>

                  <div class="mailbox-attachment-info">
                    <a download href="<?= base_url().'assets/uploads/'.$message->message_file ?>" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> <?= substr($message->message_file,0,10).' ...'  ?></a>
                        <span class="mailbox-attachment-size">
                          <?php 
                          $path = FCPATH."/assets/uploads/".$message->message_file;
                            $size = filesize($path);
                            echo file_format($size,null, true, '%01.1f %s');
                          ?>
                          <a  download href="<?= base_url().'assets/uploads/'.$message->message_file ?>" class="btn btn-default btn-xs pull-right"><i class="fa fa-download"></i></a>
                        </span>
                  </div>
                </li>
                <?php endif; ?>
              </ul>
            </div>

        <?php endforeach; ?>
            <!-- /.box-footer -->
           

             
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>



<script>
    $(function(){
      $("#compose-textarea").wysihtml5();
        $("#btn-reply").click(function() {
            $("#reply-body").show();
            $("#btn-reply").hide();
        })

        $("#add_message").on('submit',function(e) {
            
            e.preventDefault();
            var post_url = '<?php echo base_url("message/reply")?>';
            var form = new FormData(document.getElementById("add_message"));

            $.ajax({
                url: post_url,
                type: "POST",
                data: form,
                dataType:"json",
                processData: false,
                contentType: false, 
                beforeSend:function(){
                    loading();
                },
                success:function(data)
                {
                    close_loading();
                    if(data.message=="success"){
                        
                         setTimeout(
                            function(){
                                window.location.href= '<?php echo base_url("message/create")?>';
                            },2000);
                        notify2("Success","Replied successful","success");

                    }else{
                        notify2("Failed","Post added failed","error");
                    }

                    $('#add_message').each(function() { this.reset() });      
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });

        })
    })
</script>