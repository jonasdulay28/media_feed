<?php if($post_details): foreach($post_details as $post): ?>
            <div class="col-md-4">
                      <div class="box">
                      
                      <div class="box-header with-border text-center">
                      <?php if($post->type == "Text"): ?>  
                                  <i class="fa fa-font fa-x5" style="font-size: 5em"></i>
                                 <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                                  
                      <?php elseif($post->type=="Docx"): ?>           
                                  <i class="fa fa-file fa-x5" style="font-size: 5em"></i>
                                 <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                                  
                      <?php elseif($post->type=="Image"): ?>
                                  <i class="fa fa-image fa-x5" style="font-size: 5em"></i>
                                   <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                      <?php else: ?>
                                  <i class="fa fa-file-pdf-o fa-x5" style="font-size: 5em"></i>
                                  <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                      <?php endif; ?>
                             
                          </div>
                      </div>
                  </div>
      <!-- /.box -->

    <?php endforeach; endif?>