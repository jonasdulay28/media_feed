 <?php 

   $userWhere = ['id' => decrypt($this->session->id)];
    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);
    $campusWhere = ['id' => $getUser->campus_id];
    $getUserCampus = $this->Crud_model->fetch_tag_row('*','campus',$campusWhere);


    $campus = $this->Crud_model->fetch('campus');


    


 ?>

  <link rel="stylesheet" href="<?= base_url().'assets/css/bootstrap-select.min.css' ?>">
<script src="<?= base_url().'assets/js/bootstrap-select.min.js' ?>"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Posts
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url().'Dashboard' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> View Posts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
            <div class="col-md-12">
              <div class="filter_container pull-left">
                <select class="form-control" id="sort-post">
                  <option value="All">All</option>
                  <option value="Text">Text</option>
                  <option value="Image">Image</option>
                  <option value="Pdf">Pdf</option>
                  <option value="Docx">Docx</option>
                </select>
              </div>
              <div class="refresh_container pull-right">
                <button class="btn btn-primary"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
        </div>
        <div class="box-body">
          
            <!-- /.col -->
            
             
               
          <!-- /.row -->
        </div><br><br><br>
      <center> <a href="#" class="text-center" id="loadMore"><h4>-- Load More Post --</h4></a></center>
      <br>
      </div>

      
    </section>
    <!-- /.content -->
  </div>


<?php 
$offices = $this->Crud_model->fetch('offices');
?>
<div class="modal fade" id="forward-now">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <?= form_open('','method="POST" id="forward_to" autocomplete="off"') ?>
                  <input type="hidden" name="p_id" id="p_id">
                  <div class="form-group">
                    <input type="hidden" name="check_post" id="check_post">
                    <label for="">Forward to:</label>
                    <select name="p_forward_to[]" id="fward-select" class="form-control" multiple>
                    <?php if(decrypt($this->session->role) == 2): 
                            foreach($offices as $office): ?>
                              <option value="<?= $office->id ?>"><?= $office->office_name ?></option>
                    <?php   endforeach; 
                          else: ?>
                          <option value="4">Faculty</option>
                          <option value="5">Staff</option>
                          <option value="6">Student</option>
                        <?php endif; ?>
                    </select>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                <?= form_close(); ?>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <style>

          div.post{
            display:none;
          }

        </style>

        <script>
              wow = new WOW(
              {
                boxClass:     'wow',      // default
                animateClass: 'animated', // default
                offset:       0,          // default
                mobile:       true,       // default
                live:         true        // default
              }
            )
          wow.init();
          function get_post()
          {
             var post_url = '<?php echo base_url("post/data")?>';
            $.ajax({
              url: post_url,
              success : function(data){
                $('.box-body').html(data);
                $("div.post").slice(0, 4).show();
                 if($("div.post:hidden").length == 0){ // check if any hidden divs still exist
                    $("#loadMore").hide();
                }
              }

            })

          }
           
          $(function() {
             get_post();

            $("#loadMore").click(function(e){ 
                e.preventDefault();
                $("div.post:hidden").slice(0, 4).show(); 
                if($("div.post:hidden").length == 0){ // check if any hidden divs still exist
                    $("#loadMore").hide();
                } else {
                   $("#loadMore").show();
                }
            });

             $("#sort-post").change(function() {
              var sort_to = $("#sort-post").val();
                $.ajax({
                  url:'<?= base_url().'posting/sortType/' ?>' +  sort_to,
                  success: function(data)
                  {

                    $('.box-body').html(data);
                    $("div.post:hidden").slice(0, 4).show(); 
                    if($("div.post:hidden").length == 0){ // check if any hidden divs still exist
                        $("#loadMore").hide();
                    } else {
                       $("#loadMore").show();
                    }
                  }
                })
              });

              $("#fward-select").selectpicker();
              $(document).on('click','#fward-now',function() {
                var id = $(this).data('pid');
                var pname = $(this).data('pname');
                var checker = $(this).data('check');
                $(".modal-title").html(pname)
                $("#p_id").val(id);
                $("#check_post").val(checker);
              });

              $("#forward_to").on("submit",function(e)
              {

                  e.preventDefault();
                  var post_url = '<?php echo base_url("post/add")?>';
                  $.ajax({
                      type : 'POST',
                      url : post_url,
                      data: $("#forward_to").serialize(),
                      dataType:"json",
                      beforeSend:function(){
                          loading();
                      },
                      success : function(res){
                          close_loading();
                          if(res.message=="success"){
                              
                              setTimeout(
                                  function(){
                                      window.location.href= '<?php echo base_url("post/list")?>';
                                  },2000);
                          notify2("Success","Post added successfully.. redirecting to post list","success");
                          }else{
                              notify2("Failed","Post added failed","error");
                          }

                          $('#add_post').each(function() { this.reset() });
                      },
                      error : function() {
                          $('#modal_content').html('<p class="error">Error in submit</p>');
                      }
                  });
              })

          })
        </script>
