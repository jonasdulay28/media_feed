  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Campus
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Campus</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Campus Records</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table id="campus" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Campus Name</th>
                        <th>Address</th>
                        <th>Contact Number</th>
                        <th class="disabled-sorting text-center">Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Campus Name</th>
                        <th>Address</th>
                        <th>Contact Number</th>
                        <th class="disabled-sorting text-center">Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Campus Records
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- The Modal -->
<div class="modal fade" id="campusModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="modify_campus" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" id="edit_id" name="id" readonly="" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="control-label">Campus name & City</label>
                    <input type="text" class="form-control" name="campus_name" id="campus-name" required>
                </div>  
                 <div class="form-group ">
                    <label class="control-label">Complete Address</label>
                    <input type="text" class="form-control" name="complete_address" id="address" required>
                </div>  

                <div class="form-group ">
                    <label class="control-label">Contact Number</label>
                    <input type="text" class="form-control" name="contact_number" id="contact_number" required>
                </div>  
            </div>
        </div>
        
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">

    function get_campus()
    {
       $('#campus').DataTable({
            "ajax": "<?php echo base_url('campus/getRecords')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    
    $(function() {

            $("#contact_number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

        get_campus();
         var table = $('#campus').DataTable();
        
        table.on('click', '.modify', function(e)
        {
            e.preventDefault();
            var id = $(this).data('id');
            var post_url = '<?php echo base_url()?>campus/get_campus';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:{id: id},
                dataType:"json",
                beforeSend:function(){
                },
                success : function(res){
                    $("#edit_id").val(id);
                    $("#campus-name").val(res.campus.campus_name);
                    $("#address").val(res.campus.address);
                    $("#contact_number").val(res.campus.contact_number);
                    $(".modal-title").html("Modify "+res.campus.campus_name);
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        });

        $("#modify_campus").on("submit",function(e)
        {
            e.preventDefault();
            var post_url = '<?php echo base_url()?>campus/update_campus';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:$("#modify_campus").serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                    {
                        $("#campus").dataTable().fnDestroy();
                        get_campus();
                        notify2("Success","Campus updated successfuly","success");
                        $("#campusModal").modal("toggle");
                    }
                    else
                    {
                        notify2("Failed","Campus update falied","error");
                    }

                },
                error : function() {
                    notify2("Failed","Campus status update falied","error");
                }
            });
        })
    });


</script>