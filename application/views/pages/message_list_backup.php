<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mailbox
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
           <?php  if(decrypt($this->session->role) == 1 || decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3): ?>
          <a href="<?= base_url().'message/create' ?>" class="btn btn-primary btn-block margin-bottom">Compose</a>
          <?php endif; ?>

         <?php $this->load->view('includes/messagebar'); ?>
         
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table id="message_tbl" class="table table-hover table-striped">
                  <tbody>
                    <?php 
                    if(!empty($messages)):
                    foreach($messages as $message): 
                    $where = ['id' => $message->id];
                    
                    ?>
                  <tr>
                    <td class="mailbox-name"><a href="<?= base_url().'message/details/'.$message->ticket ?>"><?= $message->first_name.' '.$message->last_name ?></a></td>
                    <td class="mailbox-subject"><?php if($message->status == 0): ?><b><?= $message->subject ?></b>  <?php else: echo $message->subject; endif; ?>
                    </td>
                    <td class="mailbox-date"><?php 
                      $created_at = strtotime($message->created_at);
                      $now = time();

                      echo timespan($created_at, $now) . ' ago';
                    ?></td>
                  </tr>
                <?php endforeach;
                  else:
                 ?>
                  <tr>
                    <td class="mailbox-name">You have no messages.</td>
                  </tr>
                  <?php endif; ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                </button>
                <div class="btn-group">
                </div>
                <div class="pull-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script>
    $(function() {
      $("#message_tbl").DataTables();
    })
  </script>