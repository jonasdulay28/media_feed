<?php 

    $forwardTo = $post_details->forward;

    $explode = explode(',',$forwardTo);

    $campus = $this->Crud_model->fetch('campus');

    $where = ['user_id'  => $post_details->user_id];
    $postInfo = $this->Crud_model->fetch_tag_row('*','post',$where);

    $userWhere = ['id'  => $postInfo->user_id];
    $postedBy = $this->Crud_model->fetch_tag_row('*','users',$userWhere);
?>

<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="box">
          <div class="box-header with-border">
            <az><h3 class="box-title" style="font-size:22px;"><?= $post_details->name ?></h3></center>
              <?php 
             

            // if(decrypt($this->session->role) == 3 || decrypt($this->session->role) == 2): 

              $userWhere = ['id' => decrypt($this->uri->segment(3))];
              $getPost = $this->Crud_model->fetch_tag_row('*','post',$userWhere);


              if(decrypt($this->session->id) == $getPost->user_id):
                  ?>
            <a href="<?= base_url().'posting/delete/'. $this->uri->segment(3); ?>" class="btn btn-danger pull-right">DELETE</a>
              <?php endif; ?>
          </div>
          <div class="box-body">
            <div class="row">
              <!-- /.col -->
              <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default" style="border-color: #eee;box-shadow:none;">
                  <div class="panel-body">
                    <p class="pull-left" style="color:#8B8B8B;"><?= date('F j, Y h:i a',strtotime($post_details->created_at)) ?></p>
                    <p class="pull-right" style="color:#8B8B8B;">Posted By: <b><?=$postedBy->first_name.' '.$postedBy->last_name
                                ?></b> </p>
                    <br>
                    <br>
                    <?= nl2br($post_details->content) ?>
                    <br>
                    <br>
                    <?php if($post_details->type == "Pdf" && !empty($post_details->file)): ?>
                    <p class="text-center" style="font-size:18px;"><a href="<?= base_url().'assets/uploads/'.$post_details->file ?>" download>Click here to Download File</a></p>
                      <br>
                    <div class="flexible-container-embed">
                      <object data="<?= base_url().'assets/uploads/'.$post_details->file ?>" type="application/pdf" width="100%" height="600px">
                         <p>This browser does not support PDFs. Please download the PDF to view it: <a href="<?= base_url().'uploads/'.$post_details->file ?>">Download PDF</a></p>
                      </object>
                    </div>
                  <?php elseif($post_details->type == "Docx" && !empty($post_details->file)): ?>
                    <p class="text-center" style="font-size:18px;"><a href="<?= base_url().'assets/uploads/'.$post_details->file ?>" download>Click here to Download File</a></p>
                      <br>
                    <iframe src="http://docs.google.com/gview?url=<?= base_url().'assets/uploads/'.$post_details->file ?>&embedded=true" width="100%" height="600px;"></iframe>

                  <?php elseif($post_details->type == "Image" && !empty($post_details->file)): ?>
                      <p class="text-center" style="font-size:18px;"><a href="<?= base_url().'assets/uploads/'.$post_details->file ?>" download>Click here to Download File</a></p>
                      <br>
                      <img src="<?= base_url().'assets/uploads/'.$post_details->file ?>" class="img-responsive">
                  <?php endif;?>
                  </div>
                </div>

              </div>

              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
    </div>
        </section>
</div>