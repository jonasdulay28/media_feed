
<div class="content-wrapper">
<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Add Offices / Department</h3>
                    </div>
                    <?php echo form_open('offices/add','method="POST" id="add_office" autocomplete="off"') ?>
                    <div class="box-body">
                        <h4 class="card-title">Offices / Department Information</h4>
                        <div class="form-group label-floating">
                            <label class="control-label">Offices Name</label>
                            <input type="text" class="form-control" name="office_name" id="office-name" required >
                        </div>
                    </div>  
                    <div class="box-footer">
                        <center><button type="submit" class="btn btn-fill btn-success " style="padding:7px 30px;" name="submit">Submit</button></center>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function ucwords (str) {
        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
            return $1.toUpperCase();
        });
    }

    $(function() {
        
        $("#add_office").on("submit",function(e)
        {

            e.preventDefault();
            var post_url = '<?php echo base_url("offices/add")?>';
            $.ajax({
                type : 'POST',
                url : post_url,
                data: $('#add_office').serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                        notify2("Success","Offices / Department added successfully","success");
                    else
                        notify2("Failed","Offices / Department added failed","error");

                    $('#add_office').each(function() { this.reset() });
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        })

    });
</script>