
<div class="content-wrapper">
<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Add Campus</h3>
                    </div>
                    <?php echo form_open('campus/add','method="POST" id="add_campus" autocomplete="off"') ?>
                    <div class="box-body">
                        <h4 class="card-title">Campus Information</h4>
                        <div class="form-group label-floating">
                            <label class="control-label">Campus Name</label>
                            <input type="text" class="form-control" name="campus_name" id="campus-name" value="LSPU" >
                        </div>  
                        <div class="form-group label-floating">
                            <label class="control-label">Complete Address(Block/Lot Number, Street)</label>
                            <input type="text" class="form-control" name="complete_address" required>
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Contact Number</label>
                            <input type="text" class="form-control" name="contact_number"  id="contact_number" required>
                        </div>
                        <!-- <div class="form-group label-floating">
                            <label class="control-label">City/Province</label>
                            <input type="text" class="form-control" name="city" id="city" required>
                        </div> -->
                    </div>  
                    <div class="box-footer">
                        <center><button type="submit" class="btn btn-fill btn-success " style="padding:7px 30px;" name="submit">Submit</button></center>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function ucwords (str) {
        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
            return $1.toUpperCase();
        });
    }

    $(function() {

         $("#contact_number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        /*$("#city").keyup(function() {
            var city = $("#city").val();
            $("#campus-name").val("LSPU " + ucwords(city));
        })*/
        
        $("#add_campus").on("submit",function(e)
        {

            e.preventDefault();
            var post_url = '<?php echo base_url("campus/add")?>';
            $.ajax({
                type : 'POST',
                url : post_url,
                data: $('#add_campus').serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                        notify2("Success","Campus added successfully","success");
                    else
                        notify2("Failed","Campus added failed","error");

                    $('#add_campus').each(function() { this.reset() });
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        })

    });
</script>