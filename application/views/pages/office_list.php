  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Offices / Departments
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Offices / Departments</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Offices / Departments Records</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table id="offices_tbl" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Office / Department Name</th>
                        <th class="disabled-sorting text-center">Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Office / Department Name</th>
                        <th class="disabled-sorting text-center">Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Offices / Departments
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- The Modal -->
<div class="modal fade" id="officeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="modify_office" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" id="edit_id" name="id" readonly="" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="control-label">Office / Department</label>
                    <input type="text" class="form-control" name="office_name" id="office-name" required>
                </div> 
            </div>
        </div>
        
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">

    function get_offices()
    {
       $('#offices_tbl').DataTable({
            "ajax": "<?php echo base_url('offices/getRecords')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    
    $(function() {

        get_offices();

        var table = $('#offices_tbl').DataTable();
        
        table.on('click', '.modify', function(e)
        {
            e.preventDefault();
            var id = $(this).data('id');
            var post_url = '<?php echo base_url()?>offices/get_offices';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:{id: id},
                dataType:"json",
                beforeSend:function(){
                },
                success : function(res){
                    $("#edit_id").val(id);
                    $("#office-name").val(res.offices.office_name);
                    $(".modal-title").html("Update " + res.offices.office_name);
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        });

        $("#modify_office").on("submit",function(e)
        {
            e.preventDefault();
            var post_url = '<?php echo base_url()?>offices/update_offices';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:$("#modify_office").serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                    {
                        $("#offices_tbl").dataTable().fnDestroy();
                        get_offices();
                        notify2("Success","Office / Department updated successfuly","success");
                        $("#officeModal").modal("toggle");
                    }
                    else
                    {
                        notify2("Failed","Office / Department update failed","error");
                    }

                },
                error : function() {
                    notify2("Failed","Office / Department status update failed","error");
                }
            });
        })
    });


</script>