<?php 
$campus = $this->Crud_model->fetch_data('campus'); 
$offices = $this->Crud_model->fetch_data('offices');
?>
<link rel="stylesheet" href="<?= base_url().'assets/css/bootstrap-select.min.css' ?>">
<script src="<?= base_url().'assets/js/bootstrap-select.min.js' ?>"></script>
<div class="content-wrapper">
    <section class="content">
        <div class="box">
        <div class="box-header with-border">
          <center><h3 class="box-title" style="font-size:18px;">Add Post</h3></center>
        </div>
        <?php echo form_open_multipart('','method="POST" id="add_post" autocomplete="off"') ?>
        <div class="box-body">
         <div class="row">
            <div class="col-md-12">
                
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Title:</label>
                                <input type="text" name="post_name" class="form-control" required>
                            </div>  
                            <div class="row padding0">
                                <div class="form-group label-floating col-md-9">
                                    <label class="control-label">To:</label>
                                    <select name="forward_to[]" id="forward-to" class="form-control" multiple title="Forward to" required>
                                    <?php 
                                        if(decrypt($this->session->role) == 1): 
                                            foreach($campus as $row): ?>
                                                <option value="<?= $row->id; ?>"><?= $row->campus_name ?></option>
                                    <?php
                                            endforeach;

                                        elseif(decrypt($this->session->role) == 2): 
                                            foreach($offices as $office):
                                    ?>
                                            <option value="<?= $office->id; ?>"><?= $office->office_name ?></option>
                                    <?php  
                                            endforeach;
                                        else: ?>
                                            <option value="4" >Faculty</option>
                                            <option value="5" >Staff</option>
                                            <option value="6" >Student</option>
                                    <?php endif; ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating col-md-3">
                                    <label class="control-label">Priority</label>
                                    <select name="priority" id="priority" class="form-control" required>
                                        <option value="1">High</option>
                                        <option value="0">Low</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group label-floating" style="display:none;">
                                <label class="control-label">Type of Post</label>
                                <select name="post_type" id="post-type" class="form-control" required>
                                    <option value="Text" <?php if(strtolower($_GET['type'])=="text")
                                    {echo "selected";}?>>TEXT</option>
                                    <option value="Image" <?php if(strtolower($_GET['type'])=="image")
                                    {echo "selected";}?>>IMAGE</option>
                                    <option value="Pdf" <?php if(strtolower($_GET['type'])=="pdf")
                                    {echo "selected";}?>>PDF</option>
                                    <option value="Docx" <?php if(strtolower($_GET['type'])=="docx")
                                    {echo "selected";}?>>DOCX</option>
                                </select>
                            </div>
                            
                            <div class="form-group label-floating" id="text-content">
                                <label class="control-label">Text Content</label>
                                <textarea name="text_content" class="form-control" required id="text_content" rows="6" style="text-align:justify;"></textarea>
                            </div>
                            <div class="form-group label-floating" id="file-content" hidden>
                                <label class="control-label" id="file-label"></label>
                                <input type="file" name="post_file" id="post_file" class="form-control">
                            </div>
                            
                        </div>
                    </div>
                    
               
            </div>
        </div>
          <!-- /.row -->
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-fill btn-success center-block" style="padding:7px 25px;" name="submit">Submit</button>
                </div>
            </div>
        </div>
         <?php echo form_close(); ?>
      </div>
    </section>
</div>

<script>
    $(function() {
        $("#forward-to").selectpicker();
        
            var post_type = $("#post-type").val();

            if(post_type == "Text") {
                $("#file-content").hide();
            } else {
                if(post_type == "Pdf"){
                    $("#post_file").attr('accept',".pdf");
                    $("#post_file").prop('required',true);
                }else if(post_type == "Docx") {
                    $("#post_file").prop('required',true);
                    $("#post_file").attr('accept',".docx");
                }else {
                     $("#post_file").attr('accept',".jpg,.png,.gif");
                     $("#post_file").prop('required',true);
                }
                $("#file-label").html(post_type+" File:");
                 $("#file-content").show();
            }


        $("#add_post").on("submit",function(e)
        {

            e.preventDefault();
            var post_url = '<?php echo base_url("post/add")?>';
             var form = new FormData(document.getElementById("add_post"));
            $.ajax({
                type : 'POST',
                url : post_url,
                data: form,
                dataType:"json",
                processData: false,
                contentType: false, 
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success"){
                        
                        setTimeout(
                            function(){
                                window.location.href= '<?php echo base_url("post/list")?>';
                            },2000);
                    notify2("Success","Post added successfully.. redirecting to post list","success");
                    }else{
                        notify2("Failed","Post added failed","error");
                    }

                    $('#add_post').each(function() { this.reset() });
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        })


    })
</script>