<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mailbox
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
           <?php  if(decrypt($this->session->role) == 1 || decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3): ?>
          <a href="<?= base_url().'message/create' ?>" class="btn btn-primary btn-block margin-bottom">Compose</a>
          <?php endif; ?>

         <?php $this->load->view('includes/messagebar'); ?>
         
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

            
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- /.btn-group -->
                
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table id="message_tbl" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Subject</th>
                      <th>Date</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                      <th>Name</th>
                      <th>Subject</th>
                      <th>Date</th>
                    </tr>
                  </tfoot>
<tbody>
                </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
              
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script>

    function getMessage() {
      var post_url = '<?= base_url(); ?>';
      $('#message_tbl').DataTable({
            "ajax": post_url + "message/message_sent_list",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search Message",
            }

        });
    }

    $(function() {
      getMessage();
    })

  </script>