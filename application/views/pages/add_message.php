
<link rel="stylesheet" href="<?= base_url().'assets/css/bootstrap-select.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/css/bootstrap3-wysihtml5.min.css' ?>">
<script src="<?= base_url().'assets/js/bootstrap-select.min.js' ?>"></script>
<script src="<?= base_url().'assets/js/bootstrap3-wysihtml5.all.min.js' ?>"></script>
<div class="content-wrapper">

        <section class="content-header">
      <h1>
        Compose Message
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url().'Dashboard' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Compose Message</li>
      </ol>
    </section>  <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          <a href="<?= base_url().'message/records' ?>" class="btn btn-primary btn-block margin-bottom">Back to Inbox</a>
            
            <?php $this->load->view('includes/messagebar');?>
          
        
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Compose New Message</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                 <?php echo form_open_multipart('','method="POST" id="add_message"') ?>
              <div class="form-group">
                    <input type="hidden" id="uid" name="uid">
                    <input type="text" name="username" id="username" class="form-control" required placeholder="To:" autocomplete="off">
                    <div id="suggestions">
                        <div id="search-result">
                        </div>
                    </div>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Subject:" name="subject" required>
              </div>
              <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 300px"  name="message"></textarea>
              </div>
              <div class="form-group">
                  <i class="fa fa-paperclip"></i> Attachment
                  <input type="file" name="message_file" id=="message_file" class="form-control">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
               <?php echo form_close(); ?>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

<!-- <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                    </div>
                    <div class="card-content">
                        <?php echo form_open_multipart('','method="POST" id="add_message"') ?>
                            <div class="row">
                                <div class="col-md-6">
                    <h4>Create Message</h4>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="hidden" id="uid" name="uid">
                                        <input type="text" name="username" id="username" class="form-control" required placeholder="Search using email or name">
                                        <div id="suggestions">
                                            <div id="search-result">
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="form-group label-floating" id="text-content">
                                        <label class="control-label">Message</label>
                                        <textarea name="message" class="form-control" required id="message" rows="6" style="text-align:justify;" required></textarea>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">File (optional)</label>
                                        <input type="file" name="message_file" id=="message_file" class="form-control">
                                    </div>
                                    <button type="submit" class="btn btn-fill btn-success pull-right" name="submit">Submit</button>
                        <?php echo form_close(); ?>
                                </div>
                                
                            </div>
                            </div>
                            
                    </div>
                </div>
                
            </div>

        </div>
</section> -->
    </div>

<script>
    $("#compose-textarea").wysihtml5();

    $(function() {
        // get_messages();
        // var table = $('#message-details').DataTable();

        $("#username").keyup(function() {
            var username = $("#username").val();
            var post_url = '<?php echo base_url("message/getinfo/")?>';
            if($("#username").val().length === 0){
                $("#suggestions").hide();
            }else {
                $.ajax({
                    url: post_url,
                    type: "POST",
                    data: { username: username, token: getCookie('csrf_cookie')},
                    success:function(data)
                    {

                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#search-result').html(data);
                            $(document).on('click',"#result-get",function() {
                                var result_val = $(this).data('result');
                                var uid = $(this).data('uid');
                                $("#username").val(result_val);
                                $("#uid").val(uid);
                                $('#suggestions').hide();
                            })
                        } else {
                            $("#search-result").html("<h4><b>No result found.</b></h4>");
                        }
                        
                    }
                });
            }
        })

        $("#add_message").on('submit',function(e) {
            
            e.preventDefault();
            var post_url = '<?php echo base_url("message/create")?>';
            var form = new FormData(document.getElementById("add_message"));

            $.ajax({
                url: post_url,
                type: "POST",
                data: form,
                dataType:"json",
                processData: false,
                contentType: false, 
                beforeSend:function(){
                    loading();
                },
                success:function(data)
                {
                    close_loading();
                    if(data.message=="success"){
                       
                        notify2("Success","Message created successfully","success");
                        
                    }else{
                        notify2("Failed","Post added failed","error");
                    }

                    $('#add_message').each(function() { this.reset() });      
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });

        })

    })
</script>

<style>
    #suggestions{
        position: relative;
        z-index: 9999;
    }
    #search-result > li {
        background: none repeat scroll 0 0 #F3F3F3;
        border-bottom: 1px solid #E3E3E3;
        list-style: none outside none;
        padding: 3px 15px 3px 15px;
        text-align: left;
    }
    #search-result > li a {  color: #1d80c2; }
    .auto_list {
        border: 1px solid #E3E3E3;
        border-radius: 5px 5px 5px 5px;
        position: absolute;
        width: 100%;
    }
</style>