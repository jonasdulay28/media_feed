<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LSPU Media Feed</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" type="images/gif" href="<?php echo base_url()?>assets/img/icon.gif">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/plugins/iCheck/square/blue.css">
  <link href="<?php echo base_url()?>assets/css/sweetalert2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/plugins/pace/pace.min.css">
  <link href="<?php echo base_url()?>assets/css/styles.css" rel="stylesheet" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- jQuery 3 -->
	<script src="<?php echo base_url()?>adminlte/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url()?>adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>adminlte/bower_components/PACE/pace.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/global.js"></script>
</head>
<body >
  <div class="hold-transition login-page">
  <div class="header_top"><!--header_top-->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <div class="contactinfo">
            <ul class="nav nav-pills" style="display: inline-block;">
              <li><a href="http://lspu.edu.ph" class="text-center"><i class="fa fa-globe"></i> http://lspu.edu.ph</a></li>
              <li><a href="#" class="text-center"><i class="fa fa-envelope"></i> icts@lspu.edu.ph</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="social-icons pull-right">
            <ul class="nav navbar-nav">
              <li><a href="https://www.facebook.com/lspusanpablocity/"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!--/header_top-->
  <div class="login-logo">
    <img src="<?php echo base_url('assets/img/')?>head_banner.png" class="img-responsive center-block">
  </div>
  <div class="login-box">

    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="text-center" style="font-size: 18px;"><b>LSPU</b> Media Feed</p>
      <p class="login-box-msg">Sign in to start your session</p>

      <?php echo form_open('','class="login100-form validate-form" id="login_form" autocomplete="off"');?>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="email" placeholder="Username">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <center><p style="color:red;" class="error"></p></center>
        <div class="row">
          <!-- /.col -->
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      <?php echo form_close();?>
      <!-- /.social-auth-links -->
      
      
    </div>
    <!-- /.login-box-body -->
    <div class="login-footer">
    	<div class="col-xs-12">
    		<p class="text-center"><a href="#" class="forgot_pass">Forgot your password?</a><br></p>
    	</div>
    </div>
  </div>
</div>
<!-- /.login-box -->
<footer class="container-fluid home_footer">
  <div class="row">
    <div class="col-md-12" style="">
        <p class="text-center" style="color:white;margin-top: 15px;">Laguna State Polytechnic University © Copyright 2018. All Rights Reserved.</p>
    </div>
  </div>
</footer>
<script src="<?php echo base_url()?>assets/js/sweetalert2.min.js"></script>
<script>
	$(document).ready(function()
	{
	  $('#login_form').submit(function(e){
	    e.preventDefault();
	    var post_url = '<?php echo base_url()?>Home/index';
	      $.ajax({
	          type : 'POST',
	          url : post_url,
	          data: $('#login_form').serialize(),
	          dataType:"json",
	          beforeSend:function(){
	          	loading();
	          },
	          success : function(res){
	          	close_loading();
	            if(res.message=="success")
	            {
	              window.location.href= res.url; // the redirect goes here
	            }
	            else
	            {
	            	$(".error").html(res.message);
	            }
	              
	          },
	          error : function(res) {
	               console.log(res);
	          }
	      });
	  });
    $(".forgot_pass").on("click",function(e)
    {
      e.preventDefault();
      notify2("Forgot Password?","Please contact the administrator for a password","info");
    })
	});
</script>
</body>
</html>
