  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content">
      <div class="box">
          <div class="box-header with-border">
  <img src="<?php echo base_url('assets/img/')?>head_banner.png" class="img-responsive center-block">
           
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <!-- /.col -->
                  <div class="col-md-12">
                   <div id="ribbon" class="vision_bg">
                    <p>Vision</p>
                    </div>
                    <p class="lspu_info"><b>LSPU</b> shall be the Center for sustainable Development, transforming Lives and Communities.</p>
                  </div>
                  <!-- /.col -->
                </div>
                <div class="row">
                  <!-- /.col -->
                  <div class="col-md-12">
                   <div id="ribbon" class="mission_bg">
                    <p>Mission</p>
                    </div>
                    <p class="lspu_info"><b>LSPU</b> Provides quality education through responsive instruction, distinctive research, sustainable extension and production services for improved quality of life towards nation-building.</p>
                  </div>
                  <!-- /.col -->
                </div>
                <div class="row">
                  <!-- /.col -->
                  <div class="col-md-12">
                   <div id="ribbon" class="mandate_bg">
                    <p>Mandate & Functions</p>
                    </div>
                    <p class="lspu_info">The <b>University</b> shall primarily provide advanced education, professional, technological and vocational instruction in agriculture, fisheries, forestry, science, engineering, industrial technologies, teacher education, medicine, law, arts and sciences, information technology and other fields. It shall also undertake research and extension services, and provide progressive leadership in its areas of specialization</p>
                  </div>
                  <!-- /.col -->
                </div>
                <div class="row">
                  <!-- /.col -->
                  <div class="col-md-12">
                   <div id="ribbon" class="quality_bg">
                    <p>Quality Policy</p>
                    </div>
                    <p class="lspu_info"><b>We</b> at LSPU are committed with continual improvement to provide quality, efficient and effective services to the University Stakeholder's highest level of satisfaction through a dynamic and excellent management system imbued with utmost integrity, professionalism and innovation.</p>
                  </div>
                  <!-- /.col -->
                </div>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <!-- /.col -->
                  <div class="col-md-12">
                  <center>
                    <div id="ribbon" class="contact_bg ">
                    <p>Contact Us</p>
                    </div>
                    <div class="lspu_branch_container panel panel_default">
                      <div class="panel-body">
                        <p class="lspu_branch">Los Baños</p>
                      <p class="lspu_info2">
                        <i class="fa fa-mobile fa-lg"></i>(049)530-9200
                        <br>
                        <i class="fa fa-envelope-o fa-lg"></i>losbaNos@lspu.edu.ph
                        <br>
                        <i class="fa fa-map-marker fa-lg"></i>Mayondon, Los Banos, Laguna
                      </p>
                      </div>
                    </div>
                    <div class="lspu_branch_container panel panel_default">
                      <div class="panel-body">
                        <p class="lspu_branch">San Pablo City</p>
                      <p class="lspu_info2">
                        <i class="fa fa-mobile fa-lg"></i>(+63)503-3706
                        <br>
                        <i class="fa fa-envelope-o fa-lg"></i>icts@lspuspcc.edu.ph
                        <br>
                        <i class="fa fa-map-marker fa-lg"></i>Del Remedios, San Pablo City, Laguna
                      </p>
                      </div>
                    </div>
                    <div class="lspu_branch_container panel panel_default">
                      <div class="panel-body">
                        <p class="lspu_branch">Santa Cruz</p>
                      <p class="lspu_info2">
                        <i class="fa fa-mobile fa-lg"></i> (049)557-3026
                        <br>
                        <i class="fa fa-envelope-o fa-lg"></i>admin.stacruz@lspu.edu.ph
                        <br>
                        <i class="fa fa-map-marker fa-lg"></i>Brgy. Bubukal, Santa Cruz, Laguna
                      </p>
                      </div>
                    </div>
                    <div class="lspu_branch_container panel panel_default">
                      <div class="panel-body">
                        <p class="lspu_branch">Siniloan</p>
                      <p class="lspu_info2">
                        <i class="fa fa-mobile fa-lg"></i>(049)813-0273
                        <br>
                        <i class="fa fa-envelope-o fa-lg"></i>icts@lspusc.edu.ph
                        <br>
                        <i class="fa fa-map-marker fa-lg"></i>L. de Leon St., Siniloan, Laguna
                      </p>
                      </div>
                    </div>
                  </center>
                  </div>
                  <!-- /.col -->
                </div>
              </div>
            </div>
            
            <!-- /.row -->
          </div>
      </div>
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->