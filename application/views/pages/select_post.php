  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <center><h3 class="box-title" style="font-size:24px;">SELECT A TYPE OF POSTING YOU WANT TO CREATE.</h3></center>
        </div>
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
              <a href="<?php echo base_url('post/add/')?>?type=Text">
              <div class="col-md-3 ">
                <div class="post_type post_type_text wow  fadeInUp  animated ">
                  <center>
                    <i class="fa fa-font fa-4x "></i>
                    <p class="">Text</p>
                  </center>
                </div>
              </div>
              </a>
              <a href="<?php echo base_url('post/add/')?>?type=Image">
              <div class="col-md-3 ">
                <div class="post_type post_type_image wow  fadeInUp  animated" data-wow-delay=".5s">
                  <center>
                    <i class="fa fa-image fa-4x "></i>
                    <p class="">Image</p>
                  </center>
                </div>
              </div>
              </a>
              <a href="<?php echo base_url('post/add/')?>?type=Pdf">
              <div class="col-md-3 ">
                <div class="post_type post_type_pdf wow  fadeInUp  animated" data-wow-delay="1s">
                  <center>
                    <i class="fa fa-file-pdf-o fa-4x "></i>
                    <p class="">PDF</p>
                  </center>
                </div>
              </div>
              </a>
              <a href="<?php echo base_url('post/add/')?>?type=Docx">
              <div class="col-md-3 ">
                <div class="post_type post_type_doc wow  fadeInUp  animated" data-wow-delay="1.5s">
                  <center>
                    <i class="fa fa-file-word-o fa-4x "></i>
                    <p class="">Document</p>
                  </center>
                </div>
              </div>
              </a>

            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">

    wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();

  </script>