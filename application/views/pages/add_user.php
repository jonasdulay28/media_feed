
<div class="content-wrapper">
    <section class="content">
        <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Add User</h3>
            </div>
            <?php echo form_open_multipart('Users/add','method="POST" id="add_user" autocomplete="off"') ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title">User Information</h4>
                                    <div class="row padding0">
                                        <div class="form-group label-floating col-xs-12 col-sm-3 col-md-3">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" class="form-control" name="last_name" value="" required>
                                        </div>
                                        <div class="form-group label-floating col-xs-12 col-sm-4 col-md-4">
                                            <label class="control-label">First Name</label>
                                            <input type="text" class="form-control" name="first_name" required>
                                        </div> 
                                        <div class="form-group label-floating col-xs-8 col-sm-3 col-md-3">
                                            <label class="control-label">Middle Name</label>
                                            <input type="text" class="form-control" name="middle_name">
                                        </div>
                                        <div class="form-group label-floating col-xs-4 col-sm-2 col-md-2">
                                            <label class="control-label">Suffix</label>
                                            <input type="text" class="form-control" name="suffix" >
                                        </div>
                                    </div>
                                    <div class="row padding0">
                                        <div class="form-group label-floating col-sm-8 col-md-8">
                                            <label class="control-label">Full Address (Optional)</label>
                                            <input type="text" class="form-control" name="address">
                                        </div>  
                                        <div class="form-group label-floating col-sm-4 col-md-4">
                                            <label class="control-label">Contact Number</label>
                                            <input type="number" class="form-control" name="contact_number" required>
                                        </div>
                                    </div>
                                     <div class="row padding0">
                                         <div class="form-group label-floating col-xs-6 col-sm-6 col-md-6">
                                            <label class="control-label">Campus</label>
                                            <select name="campus" class="form-control" required>
                                                <?php foreach($campus as $campusRow): ?>
                                                    <option value="<?= encrypt($campusRow->id) ?>"><?= $campusRow->campus_name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="form-group label-floating col-xs-6 col-sm-6 col-md-6">
                                            <label class="control-label">Office</label>
                                            <select name="office" class="form-control" required>
                                                <?php foreach($offices as $office): ?>
                                                    <option value="<?= encrypt($office->id) ?>"><?= $office->office_name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                     </div>
                                    <div class="row padding0">
                                        <div class="form-group col-xs-6 col-sm-6 col-md-6 ">
                                            <label class="control-label">Position</label>
                                            <select class="form-control" name="status" required>
                                                <option value="President" >President</option>
                                                <option value="Vice President" >Vice President</option>
                                                <option value="Student" >Student</option>
                                                <option value="Faculty" >Faculty</option>
                                                <option value="Staff" >Staff</option>
                                                <option value="Dean" >Dean</option>
                                                <option value="Associate Dean" >Associate Dean</option>
                                                <option value="Chairperson" >Chairperson</option>
                                                <option value="Director" >Director</option>
                                                <option value="Campus Director" >Campus Director</option>
                                                <option value="Officer in Charge" >Officer in Charge</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-xs-6 col-sm-6 col-md-6 ">
                                            <label class="control-label">Role</label>
                                            <select class="form-control" name="role" required>
                                                <option value="1" >Super Admin</option>
                                                <option value="2" >Sub Admin</option>
                                                <option value="3" >Admin</option>
                                                <option value="4" >Faculty</option>
                                                <option value="5" >Staff</option>
                                                <option value="6" >Student</option>

                                            </select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="card-title">Account Information</h4>
                            <div class="form-group label-floating">
                                <label class="control-label">Email Address</label>
                                <input type="email" class="form-control" name="email_address" required>
                            </div>

                            <div class="form-group label-floating">
                                <label class="control-label">Picture</label>
                                <input type="file" class="form-control" name="profile_picture">
                            </div>

                            <div class="form-group label-floating">
                                <label class="control-label">User Name</label>
                                <input type="text" class="form-control" name="username" required>
                            </div>  
                           
                            <div class="row padding0">
                                <div class="form-group label-floating col-md-6">
                                    <label class="control-label">Password</label>
                                    <input type="password" class="form-control" name="password" id="password"
                                    minlength="6" maxlength="20" required>
                                </div>  

                                <div class="form-group label-floating col-md-6">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" minlength="6" maxlength="20" required>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="row">
                    <div class="col-md-12 ">
                        <center><button type="submit" class="btn btn-fill btn-success" style="padding:7px 30px;" name="submit">Submit</button></center>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

    </section>
</div>

<script type="text/javascript">
    var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
    $("#add_user").on("submit",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Users/add")?>';
        var form = new FormData(document.getElementById("add_user"));
        $.ajax({
            type : 'POST',
            url : post_url,
            data: form,
            dataType:"json",
            processData: false,
            contentType: false,

            beforeSend:function(){
                loading();
            },
            success : function(res){
                close_loading();
                if(res.message=="success")
                    notify2("Success","User added successfully","success");
                else if(res.message == "sub_admin_exist")
                    notify2("Failed",res.school+" has Sub Admin already" ,"error");
                else if(res.message == "admin_exist")
                    notify2("Failed",res.school+ "'s " + res.office +" has Admin already" ,"error");
                else
                    notify2("Failed","User added failed","error");

                $('#add_user').each(function() { this.reset() });
            },
            error : function() {
                $('#modal_content').html('<p class="error">Error in submit</p>');
            }
        });
    });
</script>