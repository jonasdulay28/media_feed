      <!-- Default box -->
    <?php if($posts): 
    /* 
        sub admin = post tbl
        admin = post_sub_admin
    */
            if(decrypt($this->session->role) == 2):
            foreach($posts as $post): 
              $explode = explode(",", $post->forward);
              if(in_array($getUser->campus_id, $explode)):
    ?>
                <div class="col-md-4">
                      <div class="box">
                      
                      <div class="box-header with-border text-center">
                      <?php if($post->type == "Text"): ?>  
                                  <i class="fa fa-font fa-x5" style="font-size: 5em"></i>
                                 <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                                  
                      <?php elseif($post->type=="Docx"): ?>           
                                  <i class="fa fa-file fa-x5" style="font-size: 5em"></i>
                                 <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                                  
                      <?php elseif($post->type=="Image"): ?>
                                  <i class="fa fa-image fa-x5" style="font-size: 5em"></i>
                                   <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                      <?php else: ?>
                                  <i class="fa fa-file-pdf-o fa-x5" style="font-size: 5em"></i>
                                  <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                      <?php endif; ?>
                             
                              
                          <center>
                                <button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="<?= encrypt('sub_admin_post') ?>" data-pname="<?= $post->name ?>" data-target="#forward-now" data-pid="<?= encrypt($post->id) ?>" id="forward-now">Forward Now</button>
                              
                            </center>
                          </div>
                      </div>
                  </div>

                <?php endif; endforeach;  
            

            elseif(decrypt($this->session->role) == 3):

              $postUser = $this->Crud_model->joinTagRow('*,users.created_at AS u_c, users.id AS uid, post.id AS pid, post.created_at AS pc','post','','','','users','post.user_id = users.id','inner','');

              $orderBy = 'priority desc, post.created_at desc'; 

              
              $repostBySubAdmins = $this->Crud_model->joinTagResult('*,post_sub_admin.created_at AS psa_c, post_sub_admin.id AS psa_id, post.id AS pid, post.created_at AS pc','post','','','','post_sub_admin','post.id = post_sub_admin.post_id','inner',$orderBy);
               
                  foreach($repostBySubAdmins as $subadmin):
                    $explode = explode(',',$subadmin->forward_to_admin);
                  if(in_array($getUser->office_id,$explode)):
                    ?>
                    <div class="col-md-4">
                          <div class="box">
                          
                          <div class="box-header with-border text-center">
                          <?php if($subadmin->type == "Text"): ?>  
                                      <i class="fa fa-font fa-x5" style="font-size: 5em"></i>
                                     <a href="<?= base_url() .'post/view/'. encrypt($subadmin->pid) ?>"><h3><?= $subadmin->name ?></h3> </a>
                                      
                          <?php elseif($subadmin->type=="Docx"): ?>           
                                      <i class="fa fa-file fa-x5" style="font-size: 5em"></i>
                                     <a href="<?= base_url() .'post/view/'. encrypt($subadmin->pid) ?>"><h3><?= $subadmin->name ?></h3> </a>
                                      
                          <?php elseif($subadmin->type=="Image"): ?>
                                      <i class="fa fa-image fa-x5" style="font-size: 5em"></i>
                                       <a href="<?= base_url() .'post/view/'. encrypt($subadmin->pid) ?>"><h3><?= $subadmin->name ?></h3> </a>
                          <?php else: ?>
                                      <i class="fa fa-file-pdf-o fa-x5" style="font-size: 5em"></i>
                                      <a href="<?= base_url() .'post/view/'. encrypt($subadmin->pid) ?>"><h3><?= $subadmin->name ?></h3> </a>
                          <?php endif; ?>
                                 
                              </div>
                              <center>
                                <button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="<?= encrypt('admin_post') ?>" data-pname="<?= $subadmin->name ?>" data-target="#forward-now" data-pid="<?= encrypt($subadmin->pid) ?>" id="forward-now">Forward Now</button>
                              
                            </center>
                          </div>
                      </div>
                   <?php endif;  
                    endforeach;
            elseif(decrypt($this->session->role) == 4 || decrypt($this->session->role) == 5 || decrypt($this->session->role) == 6):

                $orderBy = 'priority desc, post.created_at desc';
                $repostByAdmins = $this->Crud_model->joinTagResult('*,post_admin.created_at AS pa_c, post_admin.id AS pa_id, post.id AS pid, post.created_at AS pc','post','','','','post_admin','post.id = post_admin.post_id','inner',$orderBy);
               
                  foreach($repostByAdmins as $admin):
                    $explode = explode(',',$admin->forward_to_staff);
                  if(in_array($getUser->role,$explode)):
                    ?>
                    <div class="col-md-4">
                          <div class="box">
                          
                          <div class="box-header with-border text-center">
                          <?php if($admin->type == "Text"): ?>  
                                      <i class="fa fa-font fa-x5" style="font-size: 5em"></i>
                                     <a href="<?= base_url() .'post/view/'. encrypt($admin->pid) ?>"><h3><?= $admin->name ?></h3> </a>
                                      
                          <?php elseif($admin->type=="Docx"): ?>           
                                      <i class="fa fa-file fa-x5" style="font-size: 5em"></i>
                                     <a href="<?= base_url() .'post/view/'. encrypt($admin->pid) ?>"><h3><?= $admin->name ?></h3> </a>
                                      
                          <?php elseif($admin->type=="Image"): ?>
                                      <i class="fa fa-image fa-x5" style="font-size: 5em"></i>
                                       <a href="<?= base_url() .'post/view/'. encrypt($admin->pid) ?>"><h3><?= $admin->name ?></h3> </a>
                          <?php else: ?>
                                      <i class="fa fa-file-pdf-o fa-x5" style="font-size: 5em"></i>
                                      <a href="<?= base_url() .'post/view/'. encrypt($admin->pid) ?>"><h3><?= $admin->name ?></h3> </a>
                          <?php endif; ?>
                                 
                              </div>
                          </div>
                      </div>
                   <?php endif;  
                    endforeach;

            else: ?>
              <?php foreach($posts as $post): ?>
            <div class="col-md-4">
                      <div class="box">
                      
                      <div class="box-header with-border text-center">
                      <?php if($post->type == "Text"): ?>  
                                  <i class="fa fa-font fa-x5" style="font-size: 5em"></i>
                                 <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                                  
                      <?php elseif($post->type=="Docx"): ?>           
                                  <i class="fa fa-file fa-x5" style="font-size: 5em"></i>
                                 <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                                  
                      <?php elseif($post->type=="Image"): ?>
                                  <i class="fa fa-image fa-x5" style="font-size: 5em"></i>
                                   <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                      <?php else: ?>
                                  <i class="fa fa-file-pdf-o fa-x5" style="font-size: 5em"></i>
                                  <a href="<?= base_url() .'post/view/'. encrypt($post->id) ?>"><h3><?= $post->name ?></h3> </a>
                      <?php endif; ?>
                             
                          </div>
                      </div>
                  </div>
      <!-- /.box -->

    <?php endforeach; endif;  endif; ?>