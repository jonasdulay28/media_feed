
<div class="content-wrapper">
    <section class="content">
        <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Update profile</h3>
            </div>
            <?php echo form_open_multipart('Users/update_profile','method="POST" id="update_user" autocomplete="off"') ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title">User Information <?= $this->user->info('address')?></h4>
                                    <div class="row padding0">
                                        <div class="form-group label-floating col-xs-12 col-sm-3 col-md-3">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" class="form-control" name="last_name" value="<?= $this->user->info('last_name')?>" readonly="">
                                        </div>
                                        <div class="form-group label-floating col-xs-12 col-sm-4 col-md-4">
                                            <label class="control-label">First Name</label>
                                            <input type="text" class="form-control" value="<?= $this->user->info('first_name')?>"  name="first_name" readonly>
                                        </div> 
                                        <div class="form-group label-floating col-xs-8 col-sm-3 col-md-3">
                                            <label class="control-label">Middle Name</label>
                                            <input type="text" class="form-control" value="<?= $this->user->info('middle_name')?>" name="middle_name" readonly>
                                        </div>
                                        <div class="form-group label-floating col-xs-4 col-sm-2 col-md-2">
                                            <label class="control-label">Suffix</label>
                                            <input type="text" class="form-control" value="<?= $this->user->info('suffix')?>" name="suffix" readonly>
                                        </div>
                                    </div>
                                    <div class="row padding0">
                                        <div class="form-group label-floating col-sm-8 col-md-8">
                                            <label class="control-label">Full Address (Optional)</label>
                                            <input type="text" class="form-control" value="<?= $this->user->getUserAddress()?>" name="address" required>
                                        </div>  
                                        <div class="form-group label-floating col-sm-4 col-md-4">
                                            <label class="control-label">Contact Number</label>
                                            <input type="number" class="form-control" value="<?= $this->user->getUserContact()?>" name="contact_number" required>
                                        </div>
                                    </div>
                                     <div class="row padding0">
                                         <div class="form-group label-floating col-xs-6 col-sm-6 col-md-6">
                                            <label class="control-label">Campus</label>
                                            <select name="campus" class="form-control campus"  disabled="">
                                                <?php foreach($campus as $campusRow): ?>
                                                    <option value="<?= encrypt($campusRow->id) ?>"><?= $campusRow->campus_name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="form-group label-floating col-xs-6 col-sm-6 col-md-6">
                                            <label class="control-label">Office</label>
                                            <select name="office" class="form-control office" disabled>
                                                <?php foreach($offices as $office): ?>
                                                    <option value="<?= encrypt($office->id) ?>"><?= $office->office_name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                     </div>
                                    <div class="row padding0">
                                        <div class="form-group col-xs-6 col-sm-6 col-md-6 ">
                                            <label class="control-label">Position</label>
                                            <select class="form-control position" name="status" disabled>
                                                <option value="Student" >Student</option>
                                                <option value="Faculty" >Faculty</option>
                                                <option value="Staff" >Staff</option>
                                                <option value="Chairperson" >Chairperson</option>
                                                <option value="Director" >Director</option>
                                                <option value="Associate Dean" >Associate Dean</option>
                                                <option value="Dean" >Dean</option>
                                                <option value="Campus Director" >Campus Director</option>
                                                <option value="Vice President" >Vice President</option>
                                                <option value="President" >President</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-xs-6 col-sm-6 col-md-6 ">
                                            <label class="control-label">Role</label>
                                            <select class="form-control role" name="role" disabled>
                                                <option value="1" >Super Admin</option>
                                                <option value="2" >Sub Admin</option>
                                                <option value="3" >Admin</option>
                                                <option value="4" >Faculty</option>
                                                <option value="5" >Staff</option>
                                                <option value="6" >Student</option>

                                            </select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="card-title">Account Information</h4>
                            <div class="form-group label-floating">
                                <label class="control-label">Email Address</label>
                                <input type="email" class="form-control" name="email_address" value="<?= $this->user->info('email_address')?>" readonly>
                            </div>

                            <div class="form-group label-floating">
                                <label class="control-label">Picture</label>
                                <input type="file" class="form-control" name="profile_picture">
                            </div>

                            <div class="form-group label-floating">
                                <label class="control-label">User Name</label>
                                <input type="text" class="form-control" name="username" value="<?= $this->user->info('username')?>" readonly>
                            </div>  
                           
                            <div class="row padding0">
                                <div class="form-group label-floating col-md-6">
                                    <label class="control-label">Password</label>
                                    <input type="password" class="form-control" name="password"  id="password"
                                    minlength="6" maxlength="20" >
                                </div>  

                                <div class="form-group label-floating col-md-6">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" minlength="6" maxlength="20" >
                                </div> 
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="row">
                    <div class="col-md-12 ">
                        <center><button type="submit" class="btn btn-fill btn-success" style="padding:7px 30px;" name="submit">Submit</button></center>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

    </section>
</div>

<script type="text/javascript">
    $(".campus").val("<?= encrypt($this->user->info('campus_id'))?>");
    $(".office").val("<?= encrypt($this->user->info('office_id'))?>");
    $(".position").val("<?= $this->user->info('position')?>");
    $(".role").val("<?= $this->user->info('role')?>");
    var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
    $("#update_user").on("submit",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Users/update_profile")?>';
        var form = new FormData(document.getElementById("update_user"));
        $.ajax({
            type : 'POST',
            url : post_url,
            data: form,
            dataType:"json",
            processData: false,
            contentType: false,

            beforeSend:function(){
                loading();
            },
            success : function(res){
                close_loading();
                if(res.message=="success")
                    notify2("Success","User updated successfully","success");
                else if(res.message == "Invalid image")
                    notify2("Failed","Invalid image" ,"error");
                else
                    notify2("Failed","User update failed","error");
                if(res.message=="success")
                {
                    setTimeout(function(){location.reload();},3000);
                }
                
            },
            error : function() {
                $('#modal_content').html('<p class="error">Error in submit</p>');
            }
        });
    });
</script>