<!DOCTYPE html>
<html>
<head>
  <?php 
        $segment1 = strtolower($this->uri->segment(1));
        $segment2 = strtolower($this->uri->segment(2));
        $segments_arr['segment1']= $segment1;
     ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LSPU Media Feed</title>
  <link rel="icon" type="images/gif" href="<?php echo base_url()?>assets/img/icon.gif">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/dist/css/AdminLTE.min.css">
   <link rel="stylesheet" href="<?php echo base_url()?>adminlte/plugins/pace/pace.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/styles.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/dist/css/skins/skin-blue-light.min.css">
  <link href="<?php echo base_url()?>assets/css/sweetalert2.min.css" rel="stylesheet" />
  <!-- jQuery 3 -->
  <script src="<?php echo base_url()?>adminlte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url()?>adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- PACE -->
  <script src="<?php echo base_url()?>adminlte/bower_components/PACE/pace.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/global.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <?php $this->load->view('includes/sidebar',$segments_arr); ?>
    <div class="main-panel">
        <?php 

            $this->load->view('includes/navbar'); 
            switch ($segment1) {
                case 'message':
                    if($segment2 == "records")
                        $this->load->view('pages/message_list');
                    elseif($segment2 == "create")
                        $this->load->view('pages/add_message');
                    elseif($segment2 == "details")
                        $this->load->view('pages/view_message');
                    elseif($segment2 == "sent")
                        $this->load->view('pages/sent_list');
                    else
                        show_404();
                    break;
                default:
                    echo $segment2;
                    break;
            }
            $this->load->view('includes/footer'); 
        ?>
        
    </div>
</div>
<!-- ./wrapper -->


<script src="<?php echo base_url()?>assets/js/sweetalert2.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url()?>adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url()?>adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>adminlte/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.datatables.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>adminlte/dist/js/demo.js"></script>
</body>
</html>
