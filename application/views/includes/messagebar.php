 <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Folders</h3>

      <div class="box-tools">
        
      </div>
    </div>
    <div class="box-body no-padding">
      <ul class="nav nav-pills nav-stacked">
        <li <?php if($this->uri->segment(2) == 'records'): echo 'class="active"'; endif?>><a href="<?= base_url().'message/records' ?>"><i class="fa fa-inbox"></i> Inbox
          <span class="label label-primary pull-right"><?= $this->db->where(['message_to' => decrypt($this->session->id),'status' => 0])->group_by('ticket')->count_all_results('messages')?></span></a></li>
       <?php  if(decrypt($this->session->role) == 1 || decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3): ?>
        <li <?php if($this->uri->segment(2) == 'sent'): echo 'class="active"'; endif?>><a href="<?= base_url().'message/sent' ?>"><i class="fa fa-envelope-o"></i> Sent</a></li>
      <?php endif ?>
      </ul>
    </div>
    <!-- /.box-body -->
  </div>