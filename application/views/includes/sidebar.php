<?php $segment2 = $this->uri->segment(2);?>
<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img style="height: 45px; width: 45px" src="<?php echo base_url('assets/uploads/').$this->user->info('picture');?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= $this->user->info('first_name').' '.$this->user->info('last_name') ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <li <?php if($segment1=="dashboard" || $segment1==""){echo 'class="active"';}?>>
          <a href="<?php echo base_url('Dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
      <?php if(decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3 || decrypt($this->session->role) == 1): ?>
        <li class="treeview <?php if($segment1=="users"){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-users"></i> <span>Users Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($segment2=="add"){echo 'class="active"';}?>><a href="<?php echo base_url('users/add')?>"><i class="fa fa-circle-o"></i> Add User</a></li>
            <li <?php if($segment2=="records"){echo 'class="active"';}?>><a href="<?php echo base_url('Users/records')?>"><i class="fa fa-circle-o"></i> View Users</a></li>
            
          </ul>

        </li>

        <?php if(decrypt($this->session->role) == 1): ?>
          <li class="treeview <?php if($segment1=="campus"){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-university"></i> <span>Campus Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($segment2=="add"){echo 'class="active"';}?>><a href="<?php echo base_url('campus/add')?>"><i class="fa fa-circle-o"></i> Add Campus</a></li>
            <li <?php if($segment2=="records"){echo 'class="active"';}?>><a href="<?php echo base_url('campus/records')?>"><i class="fa fa-circle-o"></i> View Campus</a></li>
            
          </ul>

        </li>
           <li class="treeview <?php if($segment1=="offices"){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-certificate"></i> <span>Office / Dept. Mgmt.</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($segment2=="add"){echo 'class="active"';}?>><a href="<?php echo base_url('offices/add')?>"><i class="fa fa-circle-o"></i> Add Offices</a></li>
            <li <?php if($segment2=="records"){echo 'class="active"';}?>><a href="<?php echo base_url('offices/records')?>"><i class="fa fa-circle-o"></i> View Offices</a></li>
            
          </ul>

        </li>

        <?php endif; ?>

        <li class="treeview <?php if($segment1=="post"){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-file-o"></i> <span>Post Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($segment2=="add"){echo 'class="active"';}?>><a href="<?php echo base_url('post/add')?>"><i class="fa fa-circle-o"></i> Add Post</a></li>
            
            <li <?php if($segment2=="list" || $segment2=="view"){echo 'class="active"';}?>><a href="<?php echo base_url('post/list')?>"><i class="fa fa-circle-o"></i> View Posts</a></li>
            
            <?php if(decrypt($this->session->role) == 2 || decrypt($this->session->role) == 3): ?>
            <li <?php if($segment2=="mylist"){echo 'class="active"';}?>><a href="<?php echo base_url('post/mylist')?>"><i class="fa fa-circle-o"></i> My Post</a></li>
          <?php endif; ?>
            
          </ul>

        </li>
        <?php endif; ?>
        
        <?php if(decrypt($this->session->role) != 2 && decrypt($this->session->role) != 3 && decrypt($this->session->role) != 1): ?>

        <li <?php if($segment2=="list" || $segment2=="view"){echo 'class="active"';}?>><a href="<?php echo base_url('post/list')?>"><i class="fa fa-file-o"></i> Posting</a></li>
      <?php endif; ?>
        <li class="<?php if($segment1=="message"){echo 'active';}?>">
         <a href="<?php echo base_url('message/records')?>"><i class="fa fa-book"></i> <span>Messages</span></a></li>
        <?php if(decrypt($this->session->role) == 1): ?>
        

        <li class="treeview">
          <a href="#">
            <i class="fa fa-gears"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('Backup')?>"><i class="fa fa-circle-o"></i> Backup Database</a></li>
            <li><a href="<?php echo base_url('Audit/records')?>"><i class="fa fa-circle-o"></i> Login Logs</a></li>
            
          </ul>
          
        </li>
      <?php endif ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->