<header class="main-header">
<!-- Logo -->
<a href="<?php echo base_url()?>" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><img src="<?php echo base_url('assets/img/')?>lspu_logo.png" style="height: 35px;"></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><img src="<?php echo base_url('assets/img/')?>lspu_logo.png" style="height: 35px;"> LSPU</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- Messages: style can be found in dropdown.less-->
      <li class="dropdown messages-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-envelope-o"></i>
          <span class="label label-success"><?= $this->db->where(['message_to' => decrypt($this->session->id),'status' => 0])->group_by('ticket')->count_all_results('messages')?></span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have <?= $this->db->where(['message_to' => decrypt($this->session->id),'status' => 0])->group_by('ticket')->count_all_results('messages')?> messages</li>
          <!-- <li>
            <ul class="menu">
              <li>
                <a href="#">
                  <div class="pull-left">
                    <img src="<?php echo base_url('assets/uploads/').$this->user->info('picture');?>" class="img-circle" alt="User Image">
                  </div>
                  <h4>
                    Support Team
                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                  </h4>
                  <p>Why not buy a new awesome theme?</p>
                </a>
              </li>
            </ul>
          </li> -->
          <li class="footer"><a href="<?= base_url().'message/records' ?>">See All Messages</a></li>
        </ul>
      </li>

      <li class="dropdown messages-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-file-o"></i>
          <?php if(decrypt($this->session->role) == 1): ?>
          <?php elseif(decrypt($this->session->role) == 2): ?>
          <span class="label label-success"><?= $this->db->where(['user_id' => decrypt($this->session->id),'status' => 0])->count_all_results('post_notif')?></span>
          <?php elseif(decrypt($this->session->role) == 3): ?>
          <span class="label label-success"><?= $this->db->where(['user_id' => decrypt($this->session->id),'status' => 0])->count_all_results('post_sub_admin_notif')?></span>
        <?php else: ?>
          <span class="label label-success"><?= $this->db->where(['user_id' => decrypt($this->session->id),'status' => 0])->count_all_results('post_admin_notif')?></span>
          <?php endif; ?>
        </a>
        <ul class="dropdown-menu">
          <?php if(decrypt($this->session->role) == 1): ?>
          <?php elseif(decrypt($this->session->role) == 2): ?>
          <li class="header">You have <?= $this->db->where(['user_id' => decrypt($this->session->id),'status' => 0])->count_all_results('post_notif')?> posts</li>
          <li class="footer"><a href="<?= base_url().'post/list' ?>">See All Posts</a></li>
          <?php elseif(decrypt($this->session->role) == 3): ?>
          <li class="header">You have <?= $this->db->where(['user_id' => decrypt($this->session->id),'status' => 0])->count_all_results('post_sub_admin_notif')?> posts</li>
          <li class="footer"><a href="<?= base_url().'post/list' ?>">See All Posts</a></li>
        <?php else: ?>
           <li class="header">You have <?= $this->db->where(['user_id' => decrypt($this->session->id),'status' => 0])->count_all_results('post_admin_notif')?> posts</li>
          <li class="footer"><a href="<?= base_url().'post/list' ?>">See All Posts</a></li>
          <?php endif ?>
        </ul>
      </li>
      <!-- Notifications: style can be found in dropdown.less -->
      
      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo base_url('assets/uploads/').$this->user->info('picture');?>" class="user-image" alt="User Image">
          <span class="hidden-xs"><?= $this->user->info('first_name').' '.$this->user->info('last_name') ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="<?php echo  base_url('assets/uploads/').$this->user->info('picture');?>" class="img-circle" alt="User Image">
            <?php 
              $role = "Student";
              switch ($this->user->info('role')) {
                case 1:
                  $role = "Super Admin";
                  break;
                case 2:
                  $role = "Sub Admin";
                  break;
                case 3:
                  $role = "Admin";
                  break;
                case 4:
                  $role = "Faculty";
                  break;  
                default:
                  $role = "Student";
                  break;
              }
              ?>
            <p>
              <?= $this->user->info('first_name').' '.$this->user->info('last_name') ?>
              <small><?=  $role.' - '.$this->user->info('campus_name');?></small>
              <small>Member since <?php echo date("F-j-Y",strtotime($this->user->info('u_created')));?></small>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="<?php echo base_url('Users/profile')?>" class="btn btn-default btn-flat change_password">Profile</a>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url('Logout/index')?>" class="btn btn-default btn-flat ">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</header>