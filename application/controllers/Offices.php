<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/offices_template');
	}

	public function add()
	{
		if($_POST)
		{
			$office_name = ucwords(clean_data(post('office_name')));

			$data = [
				'office_name'	=> $office_name,
			];
			$result = $this->Crud_model->insert("offices",$data);
			$res["message"]= ($result ? "success" : "failed");
			echo json_encode($res);
		}
		else
		{
			$this->load->view('templates/offices_template');
		}
	}

	public function records()
	{
		$this->load->view('templates/offices_template');
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getRecords()
	{
		$order_by = "office_name asc";
		$data['offices'] = $this->Crud_model->fetch_data("offices","","",$order_by);
		$offices['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['offices'])
		{
			foreach($data['offices'] as $key)
			{

				$encrypt_id = encrypt($key->id);
				$offices['data']['data'][$id][] = $row;
				$offices['data']['data'][$id][] = $key->office_name;
				
					$offices['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#officeModal" ><i class="fa fa-edit"></i> Edit </a>
	                   
	                </td>';
				$id++;
				$row++;
			}
		}
		
		echo json_encode($offices['data']);
	}
	
	public function get_offices()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['offices'] = $this->Crud_model->fetch_tag_row("*","offices",$filter);
		$res["message"] = ($res["offices"]?"success":"failed");
		echo json_encode($res);
	}

	public function update_offices()
	{
		if($_POST)
		{
			$office_name = ucwords(clean_data(post('office_name')));

			$data = [
				'office_name'	=> $office_name,
			];

			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			
			$query_status = $this->Crud_model->update('offices',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}
	
}