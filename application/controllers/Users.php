<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/users_template');
	}

	public function add()
	{
		if($_POST)
		{

			$config['upload_path']          = './assets/uploads/';
			    $config['allowed_types']        = 'gif|jpg|jpeg|png';
			    $config['max_size'] = 0;
			    $config['max_width'] = 0;
			    $config['max_height'] = 0;
			    $config['encrypt_name'] = TRUE;
			    $this->load->library('upload', $config);
			$this->form_validation->set_rules('post_file',post('profile_picture').' File','callback_handleImage');

			if($this->form_validation->run() == FALSE):
				echo json_encode(validation_errors());
			else:

				$last_name = ucwords(clean_data(post('last_name')));
				$first_name = ucwords(clean_data(post('first_name')));
				$middle_name = ucwords(clean_data(post('middle_name')));
				$address = ucwords(clean_data(post('address')));
				$suffix = ucwords(clean_data(post('suffix')));
				$contact_number = clean_data(post('contact_number'));
				$campus = decrypt(post('campus'));
				$office = decrypt(post('office'));
				$role = post('role');
				$status = post('status');
				$email_address = strtolower(clean_data(post('email_address')));
				$password = hash_password(clean_data(post('password')));
				$username = clean_data(post('username'));


				if($this->upload->data('file_name')==null):
					$picture = "default.png";
				else:
					$picture = $this->upload->data('file_name');
				endif;

				$data = [
					"last_name"=>$last_name,
					"first_name"=>$first_name,
					"middle_name"=>$middle_name,
					"address"=>$address,
					"contact_number"=>$contact_number,
					"email_address"=>$email_address,
					"position"=>$status,
					"role"=>$role,
					"campus_id"	=> $campus,
					"office_id"	=> $office,
					"password"=>$password,
					"username"	=> $username,
					"suffix"	=> $suffix,
					'picture'	=> $picture,
				];

				/* 
					roles 
					1 = super admin
					2 = sub admin
					3 = admin
					4 = faculty
					5 = student
				*/

				$campusWhere = ['id'	=> $campus];
				$checkSchool = 	$this->Crud_model->fetch_tag_row('*','campus',$campusWhere);

				$officeWhere = ['id'	=> $office];
				$checkOffice = $this->Crud_model->fetch_tag_row('*','offices',$officeWhere);

				if($role==2):

					$subAdminWhere =  ['campus_id'	=> $campus, 'role'	=> 2];
					$checkSubAdmin = $this->Crud_model->fetch_tag_row('*','users',$subAdminWhere);

					if($checkSubAdmin):
						$res['message']	= "sub_admin_exist";
						$res['school']	= $checkSchool->campus_name;
					else:
						$result = $this->Crud_model->insert("users",$data);
						$res["message"]= ($result ? "success" : "failed");
					endif;

					echo json_encode($res);

				elseif($role==3):

					$adminWhere = ['campus_id' => $campus, 'role'	=> 3, 'office_id'	=>  $office];
					$checkAdmin = $this->Crud_model->fetch_tag_row('*','users',$adminWhere);
					
					if($checkAdmin):
						$res['message']	= "admin_exist";
						$res['school']	= $checkSchool->campus_name;
						$res['office']	= $checkOffice->office_name;
					else:
						$result = $this->Crud_model->insert("users",$data);
						$res["message"]= ($result ? "success" : "failed");
					endif;

					echo json_encode($res);
				else:
					$result = $this->Crud_model->insert("users",$data);
					$res["message"]= ($result ? "success" : "failed");
					echo json_encode($res);
				endif;
			endif;
			
		}
		else
		{
			$this->load->view('templates/users_template');
		}
	}

	public function records()
	{
		
		$this->load->view('templates/users_template');
	}

	public function edit()
	{
		
	}

	public function profile()
	{
		$this->load->view('templates/users_template');
	}

	public function update_profile()
	{
		if($_POST)
		{
			$config['upload_path']          = './assets/uploads/';
		    $config['allowed_types']        = 'gif|jpg|jpeg|png';
		    $config['max_size'] = 0;
		    $config['max_width'] = 0;
		    $config['max_height'] = 0;
		    $config['encrypt_name'] = TRUE;
		    $this->load->library('upload', $config);
			$this->form_validation->set_rules('post_file',post('profile_picture').' File','callback_handleImage');
			$id = decrypt(clean_data($this->session->id));
				$filter = array('id'=>$id);
			if($this->form_validation->run() == FALSE):
				$res["message"] = "Invalid image";
			else:
				$address = ucwords(clean_data(post('address')));
				$contact_number = clean_data(post('contact_number'));
				$password = hash_password(clean_data(post('password')));
				if($this->upload->data('file_name')==null):
					$picture = "default.png";
				else:
					$picture = $this->upload->data('file_name');
				endif;
				// $role = clean_data(post('role'));
				
				if(!empty(post('password')))
				{
					$data = array("address"=>$address,"contact_number"=>$contact_number,"password"=>$password,"picture"=>$picture);
					$query_status = $this->Crud_model->update('users',$data,$filter);
					$res["message"] = ($query_status?"success":"failed");
				}
				else
				{
					$data = array("address"=>$address,"contact_number"=>$contact_number,"picture"=>$picture);
					$query_status = $this->Crud_model->update('users',$data,$filter);
					$res["message"] = ($query_status?"success":"failed");
				}
			endif;
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}

	public function delete()
	{
		
	}

	public function getRecords()
	{
		$order_by = "users.id desc";
		// $data['users'] = $this->Crud_model->fetch_data("users","","",$order_by);
		// $where = ['campus_id ' => ]
		if(decrypt($this->session->role) == 1):
			$data['users'] = $this->Crud_model->joinTagResult('*, users.id AS uid, offices.id AS oid','users','','','','offices','users.office_id = offices.id','inner',$order_by);
		elseif(decrypt($this->session->role) == 2):

			$userWhere = ['id' => decrypt($this->session->id)];
			$user = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

			$where = ['campus_id' => $user->campus_id];
			$data['users'] = $this->Crud_model->joinTagResult('*, users.id AS uid, offices.id AS oid','users',$where,'','','offices','users.office_id = offices.id','inner',$order_by);

		else:
			$userWhere = ['id' => decrypt($this->session->id)];
			$user = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

			$where = ['campus_id' => $user->campus_id,'office_id' => $user->office_id];
			$data['users'] = $this->Crud_model->joinTagResult('*, users.id AS uid, offices.id AS oid','users',$where,'','','offices','users.office_id = offices.id','inner',$order_by);

		endif;
		$users['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['users'])
		{
			foreach($data['users'] as $key)
			{
				$middle_initial = substr($key->middle_name,0,1).'.';
				$full_name = $key->last_name.', '.$key->first_name.' '.$middle_initial;
				$current_status = ($key->status==1 ? "Active":"Inactive");
				$status = ($key->status==1 ? "Deactivate":"Activate");
				if($key->role == 1):
					$role = "Super Admin";
				elseif($key->role == 2):
					$role = "Sub Admin";
				elseif($key->role == 3):
					$role = "Admin";
				elseif($key->role == 4):
					$role = "Faculty";
				else:
					$role = "Student";
				endif;

				$where = ['id' => $key->campus_id];
				$getCampus = $this->Crud_model->fetch('campus',$where);

				$encrypt_id = encrypt($key->uid);
				// $users['data']['data'][$id][] = $row;
				$users['data']['data'][$id][] = $full_name;
				$users['data']['data'][$id][] = $key->email_address;
				$users['data']['data'][$id][] = $key->contact_number;

				foreach($getCampus as $row):
					$campus_name = $row->campus_name;
				$users['data']['data'][$id][] = $campus_name;
				endforeach;	
				$users['data']['data'][$id][] = $key->position;
				$users['data']['data'][$id][] = $key->office_name;

				$users['data']['data'][$id][] = $current_status;
				if($key->status==1){
					$users['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#userModal" ><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple red btn-icon deactivate" data-id="'.$encrypt_id.'" >
	                    <i class="fa fa-close"></i></a>
	                </td>';
				}
				else
				{
					$users['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#userModal"><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple green btn-icon activate" data-id="'.$encrypt_id.'">
	                    <i class="fa fa-check"></i></a>
	                </td>';
				}
				


				/*$users['data']['data'][$id][] = '
				<div class="btn-group">
					   <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					     Action <span class="caret"></span>
					   </button>
					   <ul class="dropdown-menu">
					   		<li><a href="#update_user_modal"  data-toggle="modal" class="update_user_btn" data-id="'.encrypt($key->id).'">Update</a></li>
					   		<li role="separator" class="divider"></li>
					   		<li><a href="javascript:void(0);" name="update_status" class="update_status" data-id="'.encrypt($key->id).'">
					   		'.$status.'</a></li>
					   </ul> 
				  </div>';*/
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($users['data']);
	}

	function update_status()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$user_status = $this->Crud_model->fetch_tag_row("status","users",$filter);
		if($user_status)
		{
			$update_status = ($user_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('users',$data,$filter);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}
	
	public function get_user()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['user'] = $this->Crud_model->fetch_tag_row("*","users",$filter);
		$res["message"] = ($res["user"]?"success":"failed");
		echo json_encode($res);
	}

	public function update_user()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$address = ucwords(clean_data(post('address')));
			$contact_number = clean_data(post('contact_number'));
			$email_address = strtolower(clean_data(post('email_address')));
			$password = hash_password(clean_data(post('password')));
			// $role = clean_data(post('role'));
			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			if(!empty(post('password')))
			{
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"address"=>$address,"contact_number"=>$contact_number,"password"=>$password);
			}
			else
			{
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"address"=>$address,"contact_number"=>$contact_number);
			}
			
			$query_status = $this->Crud_model->update('users',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}

	public function handleImage()
	{
		if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])):
	        if ($this->upload->do_upload('profile_picture')):
	        	// $this->session->set_userdata('p_img1',$this->upload->data());
	            return true;
	        else:
	        	$this->form_validation->set_message('handleImage', $this->upload->display_errors());
	            return false;
	        endif;
	    endif;
	}
	
}