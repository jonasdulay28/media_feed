<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campus extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/users_template');
	}

	public function add()
	{
		if($_POST)
		{
			$campus_name = ucwords(clean_data(post('campus_name')));
			$complete_address = ucwords(clean_data(post('complete_address')));
			$contact_number = ucwords(clean_data(post('contact_number')));

			$data = [
				'campus_name'	=> $campus_name,
				'address'	=> $complete_address,
				'contact_number'	=> $contact_number
			];
			$result = $this->Crud_model->insert("campus",$data);
			$res["message"]= ($result ? "success" : "failed");
			echo json_encode($res);
		}
		else
		{
			$this->load->view('templates/campus_template');
		}
	}

	public function records()
	{
		
		$this->load->view('templates/campus_template');
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getRecords()
	{
		$order_by = "campus_name asc";
		$data['campus'] = $this->Crud_model->fetch_data("campus","","",$order_by);
		$campus['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['campus'])
		{
			foreach($data['campus'] as $key)
			{

				$encrypt_id = encrypt($key->id);
				$campus['data']['data'][$id][] = $row;
				$campus['data']['data'][$id][] = $key->campus_name;
				$campus['data']['data'][$id][] = $key->address;
				$campus['data']['data'][$id][] = $key->contact_number;
				
					$campus['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#campusModal" ><i class="fa fa-edit"></i> Edit </a>
	                   
	                </td>';

				/*$users['data']['data'][$id][] = '
				<div class="btn-group">
					   <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					     Action <span class="caret"></span>
					   </button>
					   <ul class="dropdown-menu">
					   		<li><a href="#update_user_modal"  data-toggle="modal" class="update_user_btn" data-id="'.encrypt($key->id).'">Update</a></li>
					   		<li role="separator" class="divider"></li>
					   		<li><a href="javascript:void(0);" name="update_status" class="update_status" data-id="'.encrypt($key->id).'">
					   		'.$status.'</a></li>
					   </ul> 
				  </div>';*/
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($campus['data']);
	}
	
	public function get_campus()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['campus'] = $this->Crud_model->fetch_tag_row("*","campus",$filter);
		$res["message"] = ($res["campus"]?"success":"failed");
		echo json_encode($res);
	}

	public function update_campus()
	{
		if($_POST)
		{
			$campus_name = ucwords(clean_data(post('campus_name')));
			$complete_address = ucwords(clean_data(post('complete_address')));
			$contact_number = ucwords(clean_data(post('contact_number')));

			$data = [
				'campus_name'	=> $campus_name,
				'address'	=> $complete_address,
				'contact_number'	=> $contact_number
			];

			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			
			$query_status = $this->Crud_model->update('campus',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}
	
}