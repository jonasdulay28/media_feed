<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/audit_template');
	}

	public function records()
	{
		
		$this->load->view('templates/audit_template');
	}

	public function getRecords()
	{
		$order_by = "id desc";
		$data['audits'] = $this->Crud_model->fetch_data("audit_trails","","","",$order_by);
		$audits['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['audits'])
		{
			foreach($data['audits'] as $key)
			{

				$encrypt_id = encrypt($key->id);
				$audits['data']['data'][$id][] = $key->description;

				if($key->role == 1):
					$audits['data']['data'][$id][] = "Super Admin";
				elseif($key->role == 2):
					$audits['data']['data'][$id][] = "Sub Admin";
				elseif ($key->role == 3):
					$audits['data']['data'][$id][] = "Admin";
				elseif($key->role == 4):
					$audits['data']['data'][$id][] = "Faculty";
				elseif($key->role == 5):
					$audits['data']['data'][$id][] = "Staff";
				else:
					$audits['data']['data'][$id][] = "Student";
				endif;
				$audits['data']['data'][$id][] = $key->campus;
				$audits['data']['data'][$id][] = $key->office;
				$audits['data']['data'][$id][] = date('F j, Y h:i:a', strtotime($key->created));
				
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($audits['data']);
	}
	
}