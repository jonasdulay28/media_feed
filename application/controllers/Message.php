<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->helper('date');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/message_template');
	}

	public function create()
	{
		$config['upload_path']          = './assets/uploads/';
	    $config['allowed_types']        = 'gif|jpg|png|docx|pdf';
	    $config['max_size'] = 0;
	    $config['max_width'] = 0;
	    $config['max_height'] = 0;
	    $this->load->library('upload', $config);

	    $this->form_validation->set_rules('post_file',post('post_type').' File','callback_handleImage');
		if($_POST)
		{	
			if($this->form_validation->run() == FALSE):
				echo json_encode(validation_errors());
			else:

				$decryptId = decrypt(post('uid'));
				$ticket = random_string('numeric','10');
				$insertMssage = [
					'message_to'	=> $decryptId,
					'message_from'	=> clean_data(decrypt($this->session->id)),
					'message'		=> post('message'),
					'message_file'	=> $this->upload->data('file_name'),
					'subject'	=> clean_data(post('subject')),
					'ticket'	=> $ticket,
					'status'	=> 0,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];

				$result = $this->Crud_model->insert('messages',$insertMssage);

				$res["message"] = ($result ? "success" : "failed");
				echo json_encode($res);
			endif;
			
		}
		else
		{
			$this->load->view('templates/message_template');
		}
	}

	public function getinfo()
	{

		$input = clean_data(post('username'));
		$decryptId = decrypt($this->session->id);
		$decryptRole = decrypt($this->session->role);
		if($decryptRole == 2):
			$userWhere = ['id'	=> $decryptId];
			$fetchUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);
			$where = ['id != ' => decrypt($this->session->id),'campus_id' => $fetchUser->campus_id];

		elseif($decryptRole == 3):
			
			$userWhere = ['id'	=> $decryptId];
			$fetchUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);
			$where = ['id != ' => decrypt($this->session->id),'campus_id' => $fetchUser->campus_id];

		else:

			$where = ['id != ' => decrypt($this->session->id)];
		endif;

		$this->db->select("id,email_address,username,first_name, last_name");
        $this->db->from('users');
        $this->db->where($where);
        $this->db->group_start();
        $this->db->like('email_address', $input);
        $this->db->or_like('first_name', $input);
        $this->db->or_like('last_name', $input);
        $this->db->group_end();
        $this->db->limit(5);
        $this->db->order_by("id", 'desc');
        $query = $this->db->get();

        $getEmail = $query->result();

        if($getEmail != NULL):
        	foreach($getEmail as $row):
	        	 echo "<li><a id='result-get' data-uid='".encrypt($row->id)."' data-result='".$row->email_address."' style='cursor: pointer'>" . $row->first_name.' '.$row->last_name.' ('.$row->email_address.')'. "</a></li>";
	        endforeach;
        endif;
        
		
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function records()
	{
		
		$this->load->view('templates/message_template');
	}

	public function message_sent_list()
	{
		$session = decrypt($this->session->id);
		$query = "SELECT u.*, me.*
					FROM users u
					RIGHT JOIN
					(
					SELECT  m.*
					FROM messages m
					INNER JOIN
			        ( 
			        	SELECT 
			              	ticket, MAX(created_at) AS latest
			          	FROM
			              	messages
			          	WHERE
			              	message_from = $session

			          	GROUP BY 
			              	ticket

			        ) AS groupedm
			      	ON  groupedm.ticket = m.ticket
			      	AND groupedm.latest = m.created_at) as me
					ON me.message_from = u.id 
					ORDER BY me.id desc";

		$where = ['message_from' => $session];
		$order = 'id desc';

		$sent_messages = $this->db->query($query)->result();

		$sent_messages_data['data']['data']=array();
       	$id=0;
		$row = 1;

        // if(!empty($sent_messages)):
	        foreach($sent_messages as $message): 
		        $userWhere = ['id'  => $message->message_to];
              	$getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

			    $sent_messages_data['data']['data'][$id][] = '
			    <a href="'.base_url().'message/details/'.$message->ticket.'">'.$getUser->first_name.' '.$getUser->last_name.'</a>';
				
			   	
				$sent_messages_data['data']['data'][$id][] = $message->subject;

				$created_at = strtotime($message->created_at);
                $now = time();
				$sent_messages_data['data']['data'][$id][] =  
				

				timespan($created_at, $now) . ' ago';

       	 	$id++;
			$row++;
				
	        endforeach;
	        echo json_encode($sent_messages_data['data']);
	    // else:
     //        echo json_encode("You have no message");
	    // endif;   


	}

	public function sent()
	{
		$session = decrypt($this->session->id);
		$query = "SELECT u.*, me.*
					FROM users u
					RIGHT JOIN
					(
					SELECT  m.*
					FROM messages m
					INNER JOIN
			        ( 
			        	SELECT 
			              	ticket, MAX(created_at) AS latest
			          	FROM
			              	messages
			          	WHERE
			              	message_from = $session

			          	GROUP BY 
			              	ticket

			        ) AS groupedm
			      	ON  groupedm.ticket = m.ticket
			      	AND groupedm.latest = m.created_at) as me
					ON me.message_from = u.id 
					ORDER BY me.id desc";

		$where = ['message_from' => $session];
		$order = 'id desc';
		// $data['messages'] = $this->Crud_model->joinTagResult('*','users',$where,'','','messages','messages.message_from = users.id','inner',$order,'ticket');
		// $data['messages']	= $this->Crud_model->fetch_data('messages',$where,'','',$order,'ticket');
		$data['messages'] = $this->db->query($query)->result();

		$this->load->view('templates/message_template',$data);
	}

	public function message_list()
	{

		$session = decrypt($this->session->id);
		$query = "SELECT u.*, me.*
					FROM users u
					RIGHT JOIN
					(
					SELECT  m.*
					FROM messages m
					INNER JOIN
			        ( 
			        	SELECT 
			              	ticket, MAX(created_at) AS latest
			          	FROM
			              	messages
			          	WHERE
			              	message_to = $session

			          	GROUP BY 
			              	ticket

			        ) AS groupedm
			      	ON  groupedm.ticket = m.ticket
			      	AND groupedm.latest = m.created_at) as me
					ON me.message_from = u.id 
					ORDER BY me.id desc";

       	$messages = $this->db->query($query)->result();

       	$messages_data['data']['data']=array();
       	$id=0;
		$row = 1;

        foreach($messages as $message): 
	        $where = ['id' => $message->id];
		    $messages_data['data']['data'][$id][] = '
		    <a href="'.base_url().'message/details/'.$message->ticket.'">'.$message->first_name.' '.$message->last_name.'</a>';
			
		   	if($message->status == 0):
				$messages_data['data']['data'][$id][] =
				"<b>".$message->subject."</b>";
			else:
				$messages_data['data']['data'][$id][] =
				$message->subject;

			endif;
			$created_at = strtotime($message->created_at);
            $now = time();
			$messages_data['data']['data'][$id][] =  
			

			timespan($created_at, $now) . ' ago';

   	 	$id++;
		$row++;
			
        endforeach;
        echo json_encode($messages_data['data']);
                
	}


	public function getRecords()
	{
		$order_by = "created_at asc";
		
		$where = ['message_to' =>  decrypt($this->session->id)];
		$or_where = ['message_from' =>  decrypt($this->session->id)];
		$data['message'] = $this->Crud_model->or_where("messages",$where,$or_where,'',$order_by,'ticket');


		$message['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['message'])
		{
			foreach($data['message'] as $key)
			{
				$encrypt_id = encrypt($key->id);
				$where = ['id' => $key->message_to];
				$countWhere = ['message_to' => decrypt($this->session->id), 'status_to' => 1];
				$count = $this->db->where($countWhere)->group_by('ticket')->count_all_results('messages');

				$message['data']['data'][$id][] = $key->ticket;
				$message['data']['data'][$id][] = $count;				
				
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($message['data']);
	}

	public function reply()
	{
		$config['upload_path']          = './assets/uploads/';
	    $config['allowed_types']        = 'gif|jpg|png|docx|pdf';
	    $config['max_size'] = 0;
	    $config['max_width'] = 0;
	    $config['max_height'] = 0;
	    $config['encrypt_name'] = TRUE;
	    $this->load->library('upload', $config);

	    $this->form_validation->set_rules('post_file',post('post_type').' File','callback_handleImage');
		if($_POST)
		{	
			if($this->form_validation->run() == FALSE):
				echo json_encode(validation_errros());
			else:
				$where = ['ticket'	=> post('ticket')];
				$getMessage = $this->Crud_model->fetch_tag_row('*','messages',$where);

				if($getMessage->message_from == decrypt($this->session->id)):

					$reply = [
						'message_to'	=> $getMessage->message_to,
						'message_from'	=> $getMessage->message_from,
						'message'		=> clean_data(post('message')),
						'message_file'	=> $this->upload->data('file_name'),
						'ticket'	=> post('ticket'),
						'status_from'	=> 1, //0 = received 1 = sent
						'status_to'	=> 1,
						'subject'	=> clean_data(post('subject'))
					];
					$result = $this->Crud_model->insert('messages',$reply);

				else:

					$reply = [
						'message_to'	=> $getMessage->message_from,
						'message_from'	=> decrypt($this->session->id),
						'message'		=> clean_data(post('message')),
						'message_file'	=> $this->upload->data('file_name'),
						'ticket'	=> post('ticket'),
						'status_from'	=> 0, //0 = received 1 = sent
						'status_to'	=> 1,
						'subject'	=> clean_data(post('subject')),
						'created_at' => date('Y-m-d H:i:s'),
    					'updated_at' => date('Y-m-d H:i:s'),
					];
					$result = $this->Crud_model->insert('messages',$reply);
				endif;
				
				$res["message"] = ($result ? "success" : "failed");
				echo json_encode($res);
			endif;
			

		}
		else
		{
			$this->load->view('templates/message_template');
		}
	}


	public function details($ticket)
	{
		$where = ['ticket' => $ticket];
		$order_by = 'created_at desc';
		$data['messages']	= $this->Crud_model->fetch('messages',$where,'','',$order_by);

		$getLastRow = $this->db->select('*')->where($where)->order_by('id','desc')->limit(1)->get('messages')->row();

		$getMessage = $this->Crud_model->fetch_tag_row('*','messages',$where);

		if($getLastRow->message_to == decrypt($this->session->id)):
			$changeStatus = [
				'status'	=> 1,
				'updated_at' => date('Y-m-d H:i:s'),
			];

			$this->Crud_model->update('messages',$changeStatus,$where);
		endif;

		$this->load->view('templates/message_template',$data);
	
	}

	public function handleImage()
	{
		if (isset($_FILES['message_file']) && !empty($_FILES['message_file']['name'])):
	        if ($this->upload->do_upload('message_file')):
	            return true;
	        else:
	        	$this->form_validation->set_message('handleImage', $this->upload->display_errors());
	            return false;
	        endif;
	    endif;
	}

	
}