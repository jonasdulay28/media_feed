<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posting extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/posting_template');

		/* 
 
			with exp date 10 days
			priority sya una lalabas

			super admin
				- campus (per campus = sub admin)
			
			sub admin
				- department = admin

			admin
				- student, faculty, staff
		*/

	}

	public function add()
	{
		if($_POST)
		{
			
			if(decrypt(post('check_post')) == "sub_admin_post"):

				// $postWhere = ['id' => decrypt(post('p_id'))];
				// $getPost = $this->Crud_model->fetch_tag_row('*','post',$$postWhere);

				$insertData = [
			    	'user_id'	=> decrypt($this->session->id),
			    	'post_id'	=> decrypt(post('p_id')),
			    	'forward_to_admin' => implode(',',post('p_forward_to')), // office / department
			    	'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
			    ];

			    $lastRes = $this->Crud_model->last_inserted_row('post_sub_admin',$insertData);

			    $whereUser = ['role' => 3];
			    $users = $this->Crud_model->fetch('users',$whereUser);

			    //ex. sub = los banos admin = los banos with specific department / office
			    foreach($users as $user):

			    	$whereUser = ['id' => $lastRes->user_id];
				    $getUser = $this->Crud_model->fetch_tag_row('*','users',$whereUser);

			    	$explode = explode(",",$lastRes->forward_to_admin);

			    	if($getUser->campus_id == $user->campus_id):

				    	if(in_array($user->office_id, $explode)):

				    		$insertNotif = [
				    			'post_id'	=> $lastRes->post_id,
				    			'user_id'	=> $user->id,
				    			'status'	=> 0
				    		];

				    		$result = $this->Crud_model->insert('post_sub_admin_notif',$insertNotif);

				    	endif;

				    endif;


			    endforeach;	


			    $res["message"] = ($result ? "success" : "failed");
				echo json_encode($res);

			elseif(decrypt(post('check_post')) == "admin_post"):

				$insertData = [
			    	'user_id'	=> decrypt($this->session->id),
			    	'post_id'	=> decrypt(post('p_id')),
			    	'forward_to_staff' => implode(',',post('p_forward_to')), //student faculty staff
			    	'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
			    ];

			    $lastRes = $this->Crud_model->last_inserted_row('post_admin',$insertData);

			    $whereUser = ['role >' => 3];
			    $users = $this->Crud_model->fetch('users',$whereUser);

			    //ex. sub = los banos admin = los banos with specific department / office
			    foreach($users as $user):

			    	$whereUser = ['id' => $lastRes->user_id];
				    $getUser = $this->Crud_model->fetch_tag_row('*','users',$whereUser);

			    	$explode = explode(",",$lastRes->forward_to_staff);

			    	if($getUser->campus_id == $user->campus_id && $getUser->office_id == $user->office_id):

				    	if(in_array($user->role, $explode)):

				    		$insertNotif = [
				    			'post_id'	=> $lastRes->post_id,
				    			'user_id'	=> $user->id,
				    			'status'	=> 0
				    		];

				    		$result = $this->Crud_model->insert('post_admin_notif',$insertNotif);

				    	endif;

				    endif;


			    endforeach;	



			    $res["message"] = ($result ? "success" : "failed");
				echo json_encode($res);

			else:

				$config['upload_path']          = './assets/uploads/';
			    $config['allowed_types']        = 'gif|jpg|jpeg|png|docx|pdf';
			    $config['max_size'] = 0;
			    $config['max_width'] = 0;
			    $config['max_height'] = 0;
			    $config['encrypt_name'] = TRUE;
			    $this->load->library('upload', $config);

			    $this->form_validation->set_rules('post_file',post('post_type').' File','callback_handleImage');

			    if($this->form_validation->run() == FALSE):
			    	echo json_encode(validation_errors());
			    else:

			    	if(decrypt($this->session->role) == 3):

			    		$forwardTo = implode(',',post('forward_to'));
					    $insertData = [
					    	'type'	=> clean_data(post('post_type')),
					    	'file'	=> $this->upload->data('file_name'),
					    	'content'	=> clean_data(post('text_content')),
					    	'forward'	=> $forwardTo,
					    	'name'	=> clean_data(ucwords(post('post_name'))),
					    	'status'	=> 1, 
					    	'user_id'	=> decrypt($this->session->id),
					    	'priority'	=> post('priority'),
					    	'created_at' => date('Y-m-d H:i:s'),
	    					'updated_at' => date('Y-m-d H:i:s'),
					    ];

					    $lastRes = $this->Crud_model->last_inserted_row('post',$insertData);
					    
					    $insertAdminData = [
					    	'user_id'	=> decrypt($this->session->id),
					    	'post_id'	=> $lastRes->id,
					    	'forward_to_staff' => implode(',',post('forward_to')),
					    	'created_at' => date('Y-m-d H:i:s'),
	    					'updated_at' => date('Y-m-d H:i:s'),
					    ];

					    $lastRes2 = $this->Crud_model->last_inserted_row('post_admin',$insertAdminData);

					    $whereUser = ['role >' => 3];
					    $users = $this->Crud_model->fetch('users',$whereUser);

					    foreach($users as $user):
					    	$userWhere = ['id' => $lastRes2->user_id];
					    	$getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

					    	$explode = explode(",",$lastRes2->forward_to_staff);

					    	if($getUser->campus_id == $user->campus_id && $getUser->office_id == $user->office_id):
					    		
					    		if(in_array($user->role, $explode)):

					    			$insertNotif = [
						    			'post_id'	=> $lastRes2->post_id,
						    			'user_id'	=> $user->id,
						    			'status'	=> 0
						    		];

						    		$result = $this->Crud_model->insert('post_admin_notif',$insertNotif);

					    		endif;

					    	endif;


					    endforeach;

					    $res["message"] = ($result ? "success" : "failed");

						echo json_encode($res);

			    	elseif(decrypt($this->session->role) == 2):

			    		$forwardTo = implode(',',post('forward_to'));
					    $insertData = [
					    	'type'	=> clean_data(post('post_type')),
					    	'file'	=> $this->upload->data('file_name'),
					    	'content'	=> clean_data(post('text_content')),
					    	'forward'	=> $forwardTo,
					    	'name'	=> clean_data(ucwords(post('post_name'))),
					    	'status'	=> 1, 
					    	'user_id'	=> decrypt($this->session->id),
					    	'priority'	=> post('priority'),
					    	'created_at' => date('Y-m-d H:i:s'),
	    					'updated_at' => date('Y-m-d H:i:s'),
					    ];

					    $lastRes = $this->Crud_model->last_inserted_row('post',$insertData);

					    $insertSubAdminData = [
					    	'user_id'	=> decrypt($this->session->id),
					    	'post_id'	=> $lastRes->id,
					    	'forward_to_admin' => implode(',',post('forward_to')),
					    	'created_at' => date('Y-m-d H:i:s'),
	    					'updated_at' => date('Y-m-d H:i:s'),
					    ];

					    $lastRes2 = $this->Crud_model->last_inserted_row('post_sub_admin',$insertSubAdminData);

					    $whereUser = ['role' => 3];
					    $users = $this->Crud_model->fetch('users',$whereUser);

					    foreach($users as $user):
					    	$userWhere = ['id' => $lastRes2->user_id];
					    	$getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

					    	$explode = explode(",",$lastRes2->forward_to_admin);

					    	if($user->campus_id == $getUser->campus_id):
					    		
					    		if(in_array($user->office_id, $explode)):

					    			$insertNotif = [
						    			'post_id'	=> $lastRes2->post_id,
						    			'user_id'	=> $user->id,
						    			'status'	=> 0
						    		];

						    		$result = $this->Crud_model->insert('post_sub_admin_notif',$insertNotif);

					    		endif;

					    	endif;


					    endforeach;


					    $res["message"] = ($result ? "success" : "failed");
						echo json_encode($res);
			    	else:
			    		
			    		$forwardTo = implode(',',post('forward_to'));
					    $insertData = [
					    	'type'	=> clean_data(post('post_type')),
					    	'file'	=> $this->upload->data('file_name'),
					    	'content'	=> clean_data(post('text_content')),
					    	'forward'	=> $forwardTo,
					    	'name'	=> clean_data(ucwords(post('post_name'))),
					    	'status'	=> 1, 
					    	'user_id'	=> decrypt($this->session->id),
					    	'priority'	=> post('priority'),
					    	'created_at' => date('Y-m-d H:i:s'),
	    					'updated_at' => date('Y-m-d H:i:s'),
					    ];

					    $lastRes = $this->Crud_model->last_inserted_row('post',$insertData);

					    $whereUSer = ['role' => '2'];
					    $users = $this->Crud_model->fetch('users',$whereUSer);

					    foreach($users as $user):

					    	$explode = explode(",", $lastRes->forward);

					    	if(in_array($user->campus_id, $explode)):

					    		$insertNotif = [
					    			'post_id'	=> $lastRes->id,
					    			'user_id'	=> $user->id,
					    			'status'	=> 0
					    		];

					    		$result = $this->Crud_model->insert('post_notif',$insertNotif);

					    	endif;

					    endforeach;

					    $res["message"] = ($result ? "success" : "failed");
						echo json_encode($res);
					    // $result = $this->Crud_model->insert('post',$insertData);

					    
			    	endif;
			    	
			    endif;

			endif;	

		}
		else
		{

			$this->load->view('templates/posting_template');
		}
	}

	public function getPost()
	{
		/* 
			post/list 

			sub admin = post tbl
	        admin = post_sub_admin

		*/

		$orderBy = 'priority desc, post.created_at desc';
		// $postWhere = ['user_id' => decrypt($this->session->id)];
		$postWhere = ['status'	=> 1];
        $data['posts']   = $this->Crud_model->fetch('post',$postWhere,'','',$orderBy);

	    // $campusWhere = ['id' => $getUser->campus_id];
	    // $getUserCampus = $this->Crud_model->fetch_tag_row('*','campus',$campusWhere);

	    $campus = $this->Crud_model->fetch('campus');


        if($data['posts']):
			if(decrypt($this->session->role) == 1):
	        	echo '<div class="row  wow fadeIn  animated"><div class="col-md-12"> <div class="row text-center"  id="page-loader" >';
		        foreach ($data['posts'] as $post):
		        	$where = ['id' => $post->user_id];
		        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address','users',$where);
	                if($post->type == "Docx"):
		            	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3">
			                  <div class="post" >
			                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                         if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
			        elseif($post->type == "Pdf"):
			        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
			                  <div class="post" >
			                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
			        elseif($post->type=="Image"):
			        	$file_name = base_url()."assets/uploads/".$post->file;
			        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
			                  <div class="post ">
			                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                      if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
			        else:
			        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
			                  <div class="post ">
			                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
	                endif;
		        endforeach;
		        echo '</div></div></div>';
		    elseif(decrypt($this->session->role) == 2):

		    	foreach($data['posts'] as $post): 
		    		$userWhere = ['id' => decrypt($this->session->id)];
				    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

				    $where = ['id' => $post->user_id];
		        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address','users',$where);

	             	$explode = explode(",", $post->forward);
	              	if(in_array($getUser->campus_id, $explode)):

	              		if($post->type == "Docx"):
			            	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 text-center">
				                  <div class="post" >
				                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                  </div>
				                </div></a>';

				        elseif($post->type == "Pdf"):
				        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3  text-center">
				                  <div class="post" >
				                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				               echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                  </div>
				                </div></a>';

				        elseif($post->type=="Image"):
				        	$file_name = base_url()."assets/uploads/".$post->file;
				        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3  text-center">
				                  <div class="post ">
				                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                      if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                  </div>
				                </div></a>';

				        else:
				        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 text-center ">
				                  <div class="post ">
				                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                         if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                 echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                    </div>
				                  </div>
				                </div></a>';

		                endif;

	              	endif;

	            endforeach;


	       	elseif(decrypt($this->session->role) == 3):
	       		$userWhere = ['id' => decrypt($this->session->id)];
				    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);
	       		$postUser = $this->Crud_model->joinTagRow('*,users.created_at AS u_c, users.id AS uid, post.id AS pid, post.created_at AS pc','post','','','','users','post.user_id = users.id','inner','');

              $orderBy = 'priority desc, post.created_at desc'; 

              
              $repostBySubAdmins = $this->Crud_model->joinTagResult('*,post_sub_admin.created_at AS psa_c, post_sub_admin.id AS psa_id, post.id AS pid, post.created_at AS pc','post','','','','post_sub_admin','post.id = post_sub_admin.post_id','inner',$orderBy);

                 foreach($repostBySubAdmins as $subadmin):
			    $where = ['id' => $subadmin->user_id];
	        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address,campus_id,office_id','users',$where);

	        	if($getUser->campus_id == $name->campus_id):
                	 $explode = explode(',',$subadmin->forward_to_admin);
	                  if(in_array($getUser->office_id,$explode)):
		            		if($subadmin->type == "Docx"):
				            	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3 text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                          if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
					                  </div>
					                </div></a>';

					        elseif($subadmin->type == "Pdf"):
					        	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                          if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					               echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
					                  </div>
					                </div></a>';

					        elseif($subadmin->type=="Image"):
					        	$file_name = base_url()."assets/uploads/".$subadmin->file;
					        	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                      if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
					                  </div>
					                </div></a>';

					        else:
					        	// print_r($post->id);die;
					        	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3 text-center ">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                         if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                 echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                    </div>
				                  </div>
				                </div></a>';

			                endif;
		            		
		                endif;
		             endif;
		        endforeach;

		    else:

		    	$orderBy = 'priority desc, post.created_at desc';

                $repostByAdmins = $this->Crud_model->joinTagResult('*,post_admin.user_id AS pa_uid,post_admin.created_at AS pa_c, post_admin.id AS pa_id, post.id AS pid, post.created_at AS pc','post','','','','post_admin','post.id = post_admin.post_id','inner',$orderBy);

               	$userWhere = ['id' => decrypt($this->session->id)];

			    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

                 foreach($repostByAdmins as $admin):
				    $where = ['id' => $admin->pa_uid];
	        		$name = $this->Crud_model->fetch_tag_row('*','users',$where);

                    $explode = explode(',',$admin->forward_to_staff);
                    if($getUser->campus_id == $name->campus_id && $getUser->office_id == $name->office_id):
	                  if(in_array($getUser->role,$explode)):


	                  	if($admin->type == "Docx"):
				            	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3 text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                          if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '</div>
					                  </div>
					                </div></a>';

					        elseif($admin->type == "Pdf"):
					        	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                          if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					               echo    '</div>
					                  </div>
					                </div></a>';

					        elseif($admin->type=="Image"):
					        	$file_name = base_url()."assets/uploads/".$admin->file;
					        	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                      if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '</div>
					                  </div>
					                </div></a>';

					        else:
					        	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3 text-center ">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                         if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                 echo    '</div>
				                    </div>
				                  </div>
				                </div></a>';

			                endif;

	                  endif;
	                 endif;

	              endforeach;


		    endif;
		   else:
		   	echo "<p>No post found.</p>";
	    endif;

	}

	public function delete($id)
	{
		$postWhere = ['id' => decrypt($id)];
		$deletePost = $this->Crud_model->delete('post',$postWhere);

		if(decrypt($this->session->role) == 2):
			$postSubAdminWhere = ['post_id' =>  decrypt($id)];
			$deletePost = $this->Crud_model->delete('post_sub_admin',$postSubAdminWhere);
		else:
			$postAdminWhere = ['post_id' =>  decrypt($id)];
			$deletePost = $this->Crud_model->delete('post_admin',$postAdminWhere);
		endif;

		$this->session->set_flashdata('delete_success','You have successfully deleted post');

		redirect('post/mylist');

	}

	public function getMyList()
	{
		$where = ['user_id' => decrypt($this->session->id)];
		$data['post_details'] = $this->Crud_model->fetch('post',$where);
		echo '<div class="row  wow fadeIn  animated"><div class="col-md-12"> <div class="row text-center" >';
		if($data['post_details']):

			foreach($data['post_details'] as $post):
				$where = ['id' => $post->user_id];
	        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address,campus_id,office_id','users',$where);
				if($post->type == "Docx"):
	        		echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';
		        elseif($post->type == "Pdf"):
		        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-paperclip"></i></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';

		       elseif($post->type == "Image"):
		       	$file_name = base_url()."assets/uploads/".$post->file;
		       	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                     <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';
		        else:
		        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';

	        	endif;

			endforeach;
		else:
			echo "<p>No post found.</p>";
		endif;
		echo '</div></div></div>';
	}

	public function sortType($type)
	{

		$orderBy = 'priority desc, post.created_at desc';

		if($type == 'All'):
			$postWhere = ['status'	=> 1];
		else:
			$postWhere = ['status'	=> 1,'type' => $type];
		endif;
		
        $data['posts']   = $this->Crud_model->fetch('post',$postWhere,'',$orderBy);

	    $campus = $this->Crud_model->fetch('campus');

        if($data['posts']):
			if(decrypt($this->session->role) == 1):
	        	echo '<div class="row  wow fadeIn  animated"><div class="col-md-12"> <div class="row text-center" >';
		        foreach ($data['posts'] as $post):
		        	$where = ['id' => $post->user_id];
		        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address','users',$where);
	                if($post->type == "Docx"):
		            	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3">
			                  <div class="post" >
			                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                         if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
			        elseif($post->type == "Pdf"):
			        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
			                  <div class="post" >
			                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
			        elseif($post->type=="Image"):
			        	$file_name = base_url()."assets/uploads/".$post->file;
			        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
			                  <div class="post ">
			                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                      if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
			        else:
			        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
			                  <div class="post ">
			                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
			                    <div class="mailbox-attachment-info ">
			                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
			                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '</div>
			                  </div>
			                </div></a>';
	                endif;
		        endforeach;
		        echo '</div></div></div>';
		    elseif(decrypt($this->session->role) == 2):

		    	foreach($data['posts'] as $post): 
		    		$userWhere = ['id' => decrypt($this->session->id)];
				    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

				    $where = ['id' => $post->user_id];
		        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address','users',$where);

	             	$explode = explode(",", $post->forward);
	              	if(in_array($getUser->campus_id, $explode)):

	              		if($post->type == "Docx"):
			            	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 text-center">
				                  <div class="post" >
				                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                  </div>
				                </div></a>';

				        elseif($post->type == "Pdf"):
				        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3  text-center">
				                  <div class="post" >
				                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                          if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				               echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                  </div>
				                </div></a>';

				        elseif($post->type=="Image"):
				        	$file_name = base_url()."assets/uploads/".$post->file;
				        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3  text-center">
				                  <div class="post ">
				                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                      if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                  </div>
				                </div></a>';

				        else:
				        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 text-center ">
				                  <div class="post ">
				                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
				                    <div class="mailbox-attachment-info ">
				                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>';
				                         if($post->user_id == decrypt($this->session->id)):
				                       echo   '<span class="mailbox-attachment-size">
				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
				                          </span>';
				                      else:
				                      	echo	'<span class="mailbox-attachment-size">

				                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
				                          </span>';

				                      endif;
				                 echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("sub_admin_post").'" data-pname="'.$post->name.'" data-target="#forward-now" data-pid="'.encrypt($post->id).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                    </div>
				                  </div>
				                </div></a>';

		                endif;

	              	endif;

	            endforeach;


	       	elseif(decrypt($this->session->role) == 3):
	       		$userWhere = ['id' => decrypt($this->session->id)];
				    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);
	       		$postUser = $this->Crud_model->joinTagRow('*,users.created_at AS u_c, users.id AS uid, post.id AS pid, post.created_at AS pc','post','','','','users','post.user_id = users.id','inner','');

              $orderBy = 'priority desc, post.created_at desc'; 

              if($type == 'All'):
					$postWhere = ['status'	=> 1];
				else:
					$postWhere = ['status'	=> 1,'type' => $type];
				endif;
              $repostBySubAdmins = $this->Crud_model->joinTagResult('*,post_sub_admin.created_at AS psa_c, post_sub_admin.id AS psa_id, post.id AS pid, post.created_at AS pc','post',$postWhere,'','','post_sub_admin','post.id = post_sub_admin.post_id','inner',$orderBy);

                foreach($repostBySubAdmins as $subadmin):
				    $where = ['id' => $subadmin->user_id];
		        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address,campus_id,office_id','users',$where);

	        	if($getUser->campus_id == $name->campus_id):
                	 $explode = explode(',',$subadmin->forward_to_admin);
	                  if(in_array($getUser->office_id,$explode)):
		            		if($subadmin->type == "Docx"):
				            	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3 text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                          if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
					                  </div>
					                </div></a>';

					        elseif($subadmin->type == "Pdf"):
					        	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                          if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					               echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
					                  </div>
					                </div></a>';

					        elseif($subadmin->type=="Image"):
					        	$file_name = base_url()."assets/uploads/".$subadmin->file;
					        	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                      if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
					                  </div>
					                </div></a>';

					        else:
					        	// print_r($post->id);die;
					        	echo '<a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><div class="col-md-3 text-center ">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$subadmin->name.'</a>';
					                         if($subadmin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($subadmin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                 echo    '<button id="fward-now" class="btn btn-primary" data-toggle="modal" data-check="'.encrypt("admin_post").'" data-pname="'.$subadmin->name.'" data-target="#forward-now" data-pid="'.encrypt($subadmin->pid).'"  id="forward-now">Forward <i class="fa fa-mail-forward"></i></button></div>
				                    </div>
				                  </div>
				                </div></a>';

			                endif;
		            		
		                endif;

				    endif;

		        endforeach;

		    else:

		    	$orderBy = 'priority desc, post.created_at desc';
		    	if($type == 'All'):
					$postWhere = ['status'	=> 1];
				else:
					$postWhere = ['status'	=> 1,'type' => $type];
				endif;
                $repostByAdmins = $this->Crud_model->joinTagResult('*,post_admin.user_id AS pa_uid,post_admin.created_at AS pa_c, post_admin.id AS pa_id, post.id AS pid, post.created_at AS pc','post',$postWhere,'','','post_admin','post.id = post_admin.post_id','inner',$orderBy);

               	$userWhere = ['id' => decrypt($this->session->id)];

			    $getUser = $this->Crud_model->fetch_tag_row('*','users',$userWhere);

                 foreach($repostByAdmins as $admin):
				    $where = ['id' => $admin->pa_uid];
	        		$name = $this->Crud_model->fetch_tag_row('*','users',$where);

                    $explode = explode(',',$admin->forward_to_staff);
                    if($getUser->campus_id == $name->campus_id && $getUser->office_id == $name->office_id):
	                  if(in_array($getUser->role,$explode)):


	                  	if($admin->type == "Docx"):
				            	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3 text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                          if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '</div>
					                  </div>
					                </div></a>';

					        elseif($admin->type == "Pdf"):
					        	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post" >
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa fa-file-pdf-o"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                          if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					               echo    '</div>
					                  </div>
					                </div></a>';

					        elseif($admin->type=="Image"):
					        	$file_name = base_url()."assets/uploads/".$admin->file;
					        	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3  text-center">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                      if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                echo    '</div>
					                  </div>
					                </div></a>';

					        else:
					        	echo '<a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><div class="col-md-3 text-center ">
					                  <div class="post ">
					                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
					                    <div class="mailbox-attachment-info ">
					                      <a href="'.base_url()."post/view/". encrypt($admin->pid).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$admin->name.'</a>';
					                         if($admin->user_id == decrypt($this->session->pid)):
					                       echo   '<span class="mailbox-attachment-size">
					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>Me</a>
					                          </span>';
					                      else:
					                      	echo	'<span class="mailbox-attachment-size">

					                            <a href="'.base_url()."post/view/". encrypt($admin->pid).'" ><span style="color:#999">Posted by:</span>'.$name->first_name .' '. $name->last_name.' </a>
					                          </span>';

					                      endif;
					                 echo    '</div>
				                    </div>
				                  </div>
				                </div></a>';

			                endif;

	                  endif;
	                 endif;

	              endforeach;


		    endif;
		   else:
		   	echo "<p>No post found.</p>";
	    endif;
	}


	public function records()
	{

		if(decrypt($this->session->role) == 1):
			// redirect('post/list');
		elseif(decrypt($this->session->role) == 2):
			$where = ['user_id' => decrypt($this->session->id)];
			$updateNotif = [
				'status'	=> 1, //read
			];
			$this->Crud_model->update('post_notif',$updateNotif,$where);
		elseif(decrypt($this->session->role) == 3):
			$where = ['user_id' => decrypt($this->session->id)];
			$updateNotif = [

				'status'	=> 1, //read
			];
			$this->Crud_model->update('post_sub_admin_notif',$updateNotif,$where);
		else:
			$where = ['user_id' => decrypt($this->session->id)];
			$updateNotif = [

				'status'	=> 1, //read
			];
			$this->Crud_model->update('post_admin_notif',$updateNotif,$where);
		endif;
		
		$this->load->view('templates/posting_template');
	}

	public function mylist()
	{
		$where = ['user_id' => decrypt($this->session->id)];
		$data['post_details'] = $this->Crud_model->fetch('post',$where);
		$this->load->view('templates/posting_template',$data);
	}

	public function myPostListSort($type)
	{

		if($type == "All"):
			$postWhere = ['user_id' => decrypt($this->session->id)];
		else:
			$postWhere = ['user_id' => decrypt($this->session->id),'type' => $type];
		endif;

		$data['post_details'] = $this->Crud_model->fetch('post',$postWhere);

		echo '<div class="row  wow fadeIn  animated"><div class="col-md-12"> <div class="row text-center" >';
		if($data['post_details']):

			foreach($data['post_details'] as $post):
				$where = ['id' => $post->user_id];
	        	$name = $this->Crud_model->fetch_tag_row('first_name,last_name,email_address,campus_id,office_id','users',$where);
	        	if($post->type == "Docx"):
	        		echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-file"></i></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';
		        elseif($post->type == "Pdf"):
		        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-paperclip"></i></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';

		        elseif($post->type == "Image"):
		       	$file_name = base_url()."assets/uploads/".$post->file;
		       	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                     <span class="mailbox-attachment-icon post_icon" style="background-image:url('.$file_name.');height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';
		        else:
		        	echo '<a href="'.base_url()."post/view/". encrypt($post->id).'" ><div class="col-md-3 ">
		                  <div class="post ">
		                    <span class="mailbox-attachment-icon post_icon" style="height:220px;max-width: 100%;background-size: cover;background-repeat: no-repeat;"><i class="fa fa-font"></i></span>
		                    <div class="mailbox-attachment-info ">
		                      <a href="'.base_url()."post/view/". encrypt($post->id).'"  class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>'.$post->name.'</a>
		                          <span class="mailbox-attachment-size">
		                            <a href="'.base_url()."post/view/". encrypt($post->id).'" ><span style="color:#999">Posted by:</span>Me</a>
		                          </span>
		                    </div>
		                  </div>
		                </div></a>';

	        	endif;
				
			endforeach;
		else:
			echo "<p>No post found.</p>";

		endif;
		echo '</div></div></div>';
	}

	public function view($id)
	{
		// if(decrypt($this->session->role) == 1):
		// 	$where = ['id' => decrypt($id)];
		// 	$data['post_details'] = $this->Crud_model->fetch_tag_row('*','post',$where);
		// elseif(decrypt($this->session->role) == 2):
		// 	$where = ['id' => decrypt($id)];
		// 	$data['post_details'] = $this->Crud_model->fetch_tag_row('*','post',$where);
		// elseif(decrypt($this->session->role) == 3):
			
		// endif;
		
		$where = ['id' => decrypt($id)];
			$data['post_details'] = $this->Crud_model->fetch_tag_row('*','post',$where);
		$this->load->view('templates/posting_template',$data);
	}

	public function edit()
	{
		
	}

	public function handleImage()
	{
		if (isset($_FILES['post_file']) && !empty($_FILES['post_file']['name'])):
	        if ($this->upload->do_upload('post_file')):
	        	// $this->session->set_userdata('p_img1',$this->upload->data());
	            return true;
	        else:
	        	$this->form_validation->set_message('handleImage', $this->upload->display_errors());
	            return false;
	        endif;
	    endif;
	}
	
}