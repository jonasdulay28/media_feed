<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/dashboard_template');
	}

	public function add()
	{
		if($_POST)
		{

		}
		else
		{
			$this->load->view('templates/dashboard_template');
		}
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}