<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expiration extends CI_Controller {

	public function __construct() {

		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->setExpDate();

	}

	public function setExpDate()
	{
		$query = "update post set status = 0 where created_at < CurDate() - 10";
        $this->db->query($query);
	}

}