<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller	extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        if(isset($this->session->id))
        {
            $this->load->model("Crud_model");
        }
        else
        {
            redirect(base_url());
        }
    }

}