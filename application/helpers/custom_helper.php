<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// ------------------------------------------------------------------------

if(! function_exists('file_format')) {
    function file_format($size, $max = null, $system = 'si', $retstring = '%01.2f %s')
    {
            // Pick units
        $systems['si']['prefix'] = array('B', 'K', 'MB', 'GB', 'TB', 'PB');
        $systems['si']['size']   = 1000;
        $systems['bi']['prefix'] = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
        $systems['bi']['size']   = 1024;
        $sys = isset($systems[$system]) ? $systems[$system] : $systems['si'];
     
        // Max unit to display
        $depth = count($sys['prefix']) - 1;
        if ($max && false !== $d = array_search($max, $sys['prefix'])) {
            $depth = $d;
        }
     
        // Loop
        $i = 0;
        while ($size >= $sys['size'] && $i < $depth) {
            $size /= $sys['size'];
            $i++;
        }
     
        return sprintf($retstring, $size, $sys['prefix'][$i]);
    }

}
if ( ! function_exists('encrypt')) {

    function encrypt($string) {
        $output = FALSE;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
        $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';

        // hash
        $key = hash('sha256', $secret_key);
        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }
}

if ( ! function_exists('decrypt')) {

    function decrypt($string) {
        $output = FALSE;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
        $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';
        // hash
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }
}

if ( ! function_exists('clean_data')) {
	function clean_data($value) {
	   $value = trim($value);
    $value = str_replace('\\','',$value);
    $value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
    $value = strip_tags($value);
    $value = htmlspecialchars($value);
    return $value;
	}
}


if ( ! function_exists('test')) {
    function test() {
        return "fuck";
    }
}


if ( ! function_exists('hash_password')) {
	function hash_password($password) {
		$options = array(
		    'cost' => 11,
		);
		return password_hash($password, PASSWORD_BCRYPT, $options);
	}

}

if ( ! function_exists('check_privilege')) {
	function check_privilege($page_privilege,$privilege) {
		$privilege_array = explode(',',$privilege);
		$exists = in_array($page_privilege,$privilege_array);
		return $exists;
	}

}

if ( ! function_exists('logged_in')) {
    function logged_in(CI_Controller $controller) {
        if(isset($controller->session->id))
            return true;
        else
            return false;
    }

}

if ( ! function_exists('post')) {
    function post($name) {
        $controller = & get_instance();
        return $controller->input->post($name);
    }
}

if ( ! function_exists('sendMail')) {
	function sendMail($heading='',$content='',$from='',$receiver='')//lagay nalang parameter ilalagay natin sa helper para global
	{
 		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "ssl://smtp.googlemail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "pabilestore@gmail.com";  // user email address
        $mail->Password   = "Watheshet028";            // password in GMail
        $mail->SetFrom($receiver, 'Mail');  //Who is sending 
        $mail->isHTML(true);
        $mail->Subject    = "PABILE - ".$heading;
        $mail->Body      = '
            <html>
            <head>
                <title>'.$heading.'</title>
            </head>
            <body>
            <h3>'.$heading.'</h3>
            <br>
            '.$content.'
            <br>
            <p>With Regards</p>
            <p>'.$from.'</p>
            </body>
            </html>
        ';
        $mail->AddAddress($receiver, $receiver);
        if(!$mail->Send()) {
            return 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return 'success';
        }
	}
}

if ( ! function_exists('sendMail_file')) {
	function sendMail_file($heading='',$content='',$from='',$receiver='',$path)//lagay nalang parameter ilalagay natin sa helper para global
	{
 		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer();
		$mail->addAttachment($path,$name = '', $encoding = 'base64', $type = 'application/octet-stream');
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "ssl://smtp.googlemail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "jonasdulay28@gmail.com";  // user email address
        $mail->Password   = "Watheshet288";            // password in GMail
        $mail->SetFrom($receiver, 'Mail');  //Who is sending 
        $mail->isHTML(true);
        $mail->Subject    = $heading." - PACUCOA";
        $mail->Body      = '
            <html>
            <head>
                <title>Title</title>
            </head>
            <body>
            <h3>'.$heading.'</h3>
            <br>
            '.$content.'
            <br>
            <p>With Regards</p>
            <p>'.$from.'</p>
            </body>
            </html>
        ';
        $mail->AddAddress($receiver, $receiver);
        if(!$mail->Send()) {
            return false;
        } else {
            return true;
        }
	}
}


				


/*if ( ! function_exists('get_level_name')) {
	function get_level_name($level) {
		$this->load->model('Crud_model');
		$filter = array('level_id'=>$level);
		$row = $this->Crud_model->fetch_tag_row('level_name','tbllevel',$filter);
		return $row->level_name;
	}

}*/



?>