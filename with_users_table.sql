-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `geolocation` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-17 12:00:40','San Pablo'),(2,'LSPU Sta. Cruz','Sta Cruz','2018-03-17 12:04:59','2018-03-17 12:05:08','Sta. Cruz'),(3,'LSPU Siniloan','Siniloan','2018-03-18 11:09:35','0000-00-00 00:00:00','Siniloan');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('k6fbbif621350ajkn9t7jpjqfp0sd65n','::1',1521359728,'__ci_last_regenerate|i:1521359728;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('3vk53sg98hu4g7b7vu06p0l15dbu8raa','::1',1521360053,'__ci_last_regenerate|i:1521360053;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('cd11t9t4ovr2n2f6pks0g6ch6k7ldt5v','::1',1521360532,'__ci_last_regenerate|i:1521360532;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('edoljuj4jpkfu3lkm4649kti3gtvq2ll','::1',1521361588,'__ci_last_regenerate|i:1521361588;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('fu9g67o1ep3dt4mhtgke39odt07cuodv','::1',1521362025,'__ci_last_regenerate|i:1521362025;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('4jigcm0bb9i8jbpm8n0ggvkr1jc8jvu3','::1',1521362782,'__ci_last_regenerate|i:1521362782;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('0atdf7tnrb2svah281rak44os9bsmop9','::1',1521363537,'__ci_last_regenerate|i:1521363537;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('2gbm4p3jvbj601gvnjp0q5gfp6k32kj9','::1',1521364084,'__ci_last_regenerate|i:1521364084;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('6rj4bdc1gf698plf778ejh4urrmq93pr','::1',1521364492,'__ci_last_regenerate|i:1521364492;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('92iqv5u4gn4r3qhm3m48akkrv7agsds2','::1',1521365073,'__ci_last_regenerate|i:1521365073;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1ek825ut3op3splmeueeqcogp2diitfj','::1',1521365391,'__ci_last_regenerate|i:1521365391;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1f7fvm351pfhjvnff5bqtc527uj2chi2','::1',1521365713,'__ci_last_regenerate|i:1521365713;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1mv2pejsod5m33a0kb0l4v5mmthps31o','::1',1521369088,'__ci_last_regenerate|i:1521369088;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ilge72kmf4m7ibu2vmo0d3q1b2psikbd','::1',1521369472,'__ci_last_regenerate|i:1521369472;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('qa1s802ifska06i07poa8vq65rfd801h','::1',1521371161,'__ci_last_regenerate|i:1521371161;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('f8unoi9f8mtk2uhvsrrdesp7nstu0mtj','::1',1521372133,'__ci_last_regenerate|i:1521372133;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('c2jg9l419sef79dhjj3k07b6ps50pf7o','::1',1521372302,'__ci_last_regenerate|i:1521372133;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Accounting Office','2018-03-17 13:19:18','0000-00-00 00:00:00'),(2,'Administration Office','2018-03-17 13:19:31','0000-00-00 00:00:00'),(3,'Cashier\'s Office','2018-03-17 13:20:03','0000-00-00 00:00:00'),(4,'College of Law','2018-03-17 13:20:15','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(50) DEFAULT NULL,
  `campus_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `position` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-17 09:19:13',NULL,0,0,'',''),(6,'Test','Test','Test','123','12313','12313@mail.com','$2y$11$RIk8L9uLVhRGiUtZ2bmz/.JI1McQOLnhTkl3KJ/K7Z70M8.JSVE8O',3,1,'2018-03-18 08:03:11','2018-03-18 08:03:11','12313',2,2,'Jr','Staff'),(7,'123213','12321','Test123','123213','123213','13213213@mail.com','$2y$11$CAZ8scDvhF4rp8fgdHMtzudpSFurOnM.uxThcBU70k1750XJtuMUO',1,1,'2018-03-18 08:04:29','2018-03-18 08:04:29','123213',2,4,'11231321','President'),(9,'Test','Test','Test','12313','132213213','13123@mail.com','$2y$11$h1OpbXszY099ki2EuuZX/eue1LQehOcpvB/ttsCS.HwfRhXCpPCfa',2,1,'2018-03-18 08:36:18','2018-03-18 08:36:18','123213',2,4,'Jr','Staff'),(10,'Asdsad','Asdsad','Asdsad','Asdsad','12313','1313131@mail.com','$2y$11$yZ88mTTz/SSVMF.QEn/EZusYFZOuNP1qIj8x4zHDj7OSEaeVmpfFm',3,1,'2018-03-18 09:30:21','2018-03-18 09:30:21','13131',2,1,'Asdsad','Student'),(11,'12313','123123','1231313','123123','12313123','1231232@mail.com','$2y$11$AfuvCFyR3STnJ52AlyHojOgaWb9SUTSjoOF8M62n8dsthb.nGfiQi',3,1,'2018-03-18 11:23:07','2018-03-18 11:23:07','123123',3,3,'12313','Staff');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-18 19:26:01
