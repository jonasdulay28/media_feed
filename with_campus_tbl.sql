-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `geolocation` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-17 12:00:40','San Pablo');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('l7d9a67983uj4f4fksuh7m7uf4heq4o4','::1',1521278247,'__ci_last_regenerate|i:1521278247;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('29gnbuffmo7aoa3v59fcfbdmjln4tlb8','::1',1521281614,'__ci_last_regenerate|i:1521281614;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('v0mev84ppt8dcjfsioaadgr9cms9idd0','::1',1521281918,'__ci_last_regenerate|i:1521281918;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('nnelm0imnqcmvb0c6t3girgj5k4huknm','::1',1521282395,'__ci_last_regenerate|i:1521282395;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1rj6bih1lp00oiar8gclibdvkshhn87v','::1',1521282804,'__ci_last_regenerate|i:1521282804;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('sf663f9lqu81mic9tjf6dn2e3vf45ef5','::1',1521283116,'__ci_last_regenerate|i:1521283116;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1u3smoi62hmvh60c639h64b5ob9umit4','::1',1521283469,'__ci_last_regenerate|i:1521283469;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('9sv8s9jap7pipkapeovsc4gorm4oj8dd','::1',1521283806,'__ci_last_regenerate|i:1521283806;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('rf805m89ls1f0k0sggme5acqk39715go','::1',1521284191,'__ci_last_regenerate|i:1521284191;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('nnf64crfc29itrunh43n2k5tairdcjhe','::1',1521284511,'__ci_last_regenerate|i:1521284511;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('is551dq2i772cldrmsc7qfsgmdufvagg','::1',1521284812,'__ci_last_regenerate|i:1521284812;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('lu3lvb4lcivk82e52doeubi6sqdevl66','::1',1521285135,'__ci_last_regenerate|i:1521285135;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('62b2vi7dg5dddfe3547g6megnnd4p0kl','::1',1521285450,'__ci_last_regenerate|i:1521285450;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gioq7n6bsvhp99uun6bevahaegckn7m7','::1',1521285842,'__ci_last_regenerate|i:1521285842;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('dfm39pmfaa2d8pqpo66afdff3iolr3dp','::1',1521286174,'__ci_last_regenerate|i:1521286174;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('vpkan8b37m0jdtu7ng0kgkp4am802ac1','::1',1521286622,'__ci_last_regenerate|i:1521286622;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('p33h8k11hfio82bmkj0k0jkoj2n1mjdr','::1',1521286943,'__ci_last_regenerate|i:1521286943;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('6kj0qnvaru69eq2dpgia7jad8ojmqs20','::1',1521287283,'__ci_last_regenerate|i:1521287283;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('k3ebkf40t4rfmn0l5ph9okkt50n9g6aj','::1',1521287585,'__ci_last_regenerate|i:1521287585;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('a5ek2blg6irot3s7e2pveu747aivcr7i','::1',1521287923,'__ci_last_regenerate|i:1521287923;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ppigneuuaf667egebcdbni3adjd4cesa','::1',1521288071,'__ci_last_regenerate|i:1521287923;id|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";name|s:60:\"VFBXOWIreElOa1dwV1dFdjFLZXIxaEVvTTRCOUtsc3RjTzRFcWZMMlEybz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-17 09:19:13');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-17 20:02:45
