-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `geolocation` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `city` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-17 12:00:40','San Pablo'),(2,'LSPU Sta. Cruz','SSta Cruz','2018-03-17 12:04:59','2018-03-20 05:25:50','Sta. Cruz'),(3,'LSPU Siniloan','Siniloan','2018-03-18 11:09:35','0000-00-00 00:00:00','Siniloan');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('ob84381vpbp64e5q3ake31galuddonnb','::1',1521529443,'__ci_last_regenerate|i:1521529443;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('knbhuk9731oocq34l7true880gnjejtf','::1',1521528332,'__ci_last_regenerate|i:1521528332;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('fmqsbimfoaug1p2pimmd9hsjj3m6um2a','::1',1521528754,'__ci_last_regenerate|i:1521528754;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('al49cpceovb2o4ecsfgvbo10i06ga8o6','::1',1521529066,'__ci_last_regenerate|i:1521529066;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('i8d6vcdsni2ndljp5tp2q5tmhugmkvda','::1',1521529799,'__ci_last_regenerate|i:1521529799;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('qdvh7jce31jtmh591te5r88edf58b0lc','::1',1521529751,'__ci_last_regenerate|i:1521529751;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('1kam576lsh1qtrtr1acs2jholkbesr4i','::1',1521530256,'__ci_last_regenerate|i:1521530256;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ogtoquajufve5ai4i4aeuuiheeuemrtl','::1',1521529938,'__ci_last_regenerate|i:1521529929;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1rqud5u6qn37ptfcbnr0khmbgqp9ncm7','::1',1521532221,'__ci_last_regenerate|i:1521532221;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('a7e4tkuc6o885ms4sf98qiphoi0t937i','::1',1521531832,'__ci_last_regenerate|i:1521531831;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('uso5naip4soas66s0kptu9ghf5dr6kiq','::1',1521533163,'__ci_last_regenerate|i:1521533163;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('57142f6jlo9ncga2r879ick8199f2jap','::1',1521533523,'__ci_last_regenerate|i:1521533523;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('b8nf16gtqogpkdn93r1lqovn2shsj7hl','::1',1521533831,'__ci_last_regenerate|i:1521533831;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('sc3bb8lpjsj6jnofbk753gtru0992k4d','::1',1521534210,'__ci_last_regenerate|i:1521534210;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('eenctqhj33ltbco9fj32lnfi866f621f','::1',1521534596,'__ci_last_regenerate|i:1521534596;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('a58hhasr7b04jauvupq1sj3i6e8bbl1q','::1',1521534990,'__ci_last_regenerate|i:1521534990;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('9sl5so7rr77u5mhc154houg3pc7f7e46','::1',1521535319,'__ci_last_regenerate|i:1521535319;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('asdlkab677o8mfflirpa330nmt02lr5r','::1',1521535626,'__ci_last_regenerate|i:1521535626;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('dvslqqgeqiug7c1j4pard1reh8e7pchu','::1',1521535978,'__ci_last_regenerate|i:1521535978;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('h5eg8em66qr8tcfj4arm3rfm8t5pr55a','::1',1521536376,'__ci_last_regenerate|i:1521536376;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('6j78mmugv90f8ma662n51r3j2q60e3jc','::1',1521536712,'__ci_last_regenerate|i:1521536712;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('rt1jrepi96p7gdeur2qa02jg0srrstg5','::1',1521537031,'__ci_last_regenerate|i:1521537031;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('e69v0tn24ptdpceisndq9u4q03gimdl7','::1',1521537341,'__ci_last_regenerate|i:1521537341;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('i435r3gvtsf8q0sfc0ooiu90nrnherhh','::1',1521537644,'__ci_last_regenerate|i:1521537644;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('c7p08pbnuunmqc9usbm1ganjo5f3jc41','::1',1521537645,'__ci_last_regenerate|i:1521537644;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Accounting Office','2018-03-17 13:19:18','0000-00-00 00:00:00'),(2,'Administration Office','2018-03-17 13:19:31','0000-00-00 00:00:00'),(3,'Cashier\'s Office','2018-03-17 13:20:03','0000-00-00 00:00:00'),(4,'College of Law','2018-03-17 13:20:15','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `forward` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `name` varchar(100) NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'Docx','8a3f9487e1d4d0ac96eae8ca19741c29.docx','','2,3','2018-03-19 12:50:05','2018-03-20 06:01:09','Test Doc',1,13,0),(2,'Image','9dec3faff52a5cb9f331b8acc4e5882e.png','','3','2018-03-19 12:50:24','2018-03-20 06:01:09','Test Image',1,13,1),(3,'Text','','aaaaa','2,3','2018-03-19 12:50:39','2018-03-20 06:01:09','Test Text',1,13,1),(4,'Pdf','477c7dd9e5a6053d07a35b3e087d6ac3.pdf','','2','2018-03-19 12:52:18','2018-03-20 06:01:09','Test Pdf',1,13,0),(5,'Text','','12313123123','6','2018-03-20 06:58:03','0000-00-00 00:00:00','Hello World',1,14,1),(8,'Text','','312312312312','4,5,6','2018-03-20 07:01:57','0000-00-00 00:00:00','Admin Post',1,14,1),(9,'Text','','2313123123','2,3,4','2018-03-20 07:13:53','0000-00-00 00:00:00','Sub Admin Test',1,12,0);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_admin`
--

DROP TABLE IF EXISTS `post_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_staff` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_admin`
--

LOCK TABLES `post_admin` WRITE;
/*!40000 ALTER TABLE `post_admin` DISABLE KEYS */;
INSERT INTO `post_admin` VALUES (1,14,4,'4,5','2018-03-20 05:53:09','2018-03-20 06:37:47'),(2,14,8,'4,5,6','2018-03-20 07:01:57','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `post_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_sub_admin`
--

DROP TABLE IF EXISTS `post_sub_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_sub_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_admin` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_sub_admin`
--

LOCK TABLES `post_sub_admin` WRITE;
/*!40000 ALTER TABLE `post_sub_admin` DISABLE KEYS */;
INSERT INTO `post_sub_admin` VALUES (1,12,4,'2,3','2018-03-20 02:43:19','2018-03-20 03:36:09'),(2,12,1,'2,4','2018-03-20 02:48:27','2018-03-20 05:36:22');
/*!40000 ALTER TABLE `post_sub_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(50) DEFAULT NULL,
  `campus_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `position` varchar(30) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-20 08:19:05',NULL,0,0,'','','default.png'),(12,'Sub Admin','Sub Admin','Sub Admin','Asdsadasd','123123','sub_admin@gmail.com','$2y$11$.OOHHWwJvkow0u12iSozEOkB674SAM9/fPqxRuw67GgWplN/J9WVq',2,1,'2018-03-19 12:35:53','2018-03-20 08:19:05','sub_admin',2,2,'','Staff','default.png'),(13,'Super Admin','Super Admin','Super Admin','Super Admin','12313','super_admin@gmail.com','$2y$11$MctlsLbJCmp4lqD6mRMeJ.PsaK0DebE0KZZ5RTVGrLzUEBpKSs4mW',1,1,'2018-03-19 12:48:47','2018-03-20 08:19:05','super_admin',3,1,'','Director','default.png'),(14,'Admin','Admin','Admin','Admin Admin','123414','admin1@gmail.com','$2y$11$jPTpVKxuJ4uVEy/4fg8jEuQN7YLNMupGFID1QcpbTxZ7JfbMo0mmm',3,1,'2018-03-19 12:53:21','2018-03-20 08:19:05','admin',3,3,'','Director','default.png'),(15,'Sub Admin 1','Sub Admin 1','Sub Admin 1','Super Admin 123123','12313123','sub_admin1@gmail.com','$2y$11$eOixCY7VpyntM2mLiItc5eROqCnZTedonG03eD1Qe1H/SY8UJ72JO',2,1,'2018-03-19 12:56:11','2018-03-20 08:19:05','sub_admin1',3,2,'','Staff','default.png'),(16,'Admin2','Admin2','Admin2','Admin2','111111','admin2@gmail.com','$2y$11$dJxN73fHZvyNEcdgzrgxGOxXIWCFC6tDH0Gdg/diZB7XkVLvnnGYC',3,1,'2018-03-20 05:33:21','2018-03-20 08:19:05','123123123',2,1,'','Associate Dean','default.png'),(17,'Staff','Staff','Staff','Staffasdad','123123','staff@gmail.com','$2y$11$9awbNXhCM96ZQihdJ8FONuFpkGvUH8kbDW1usWFAWcod3Mm9DpeKO',6,1,'2018-03-20 06:03:58','2018-03-20 08:19:05','staff',2,1,'','Dean','default.png'),(18,'Staff2','Staff2','Staff2','Staff2','123121323','staff2@gmail.com','$2y$11$fc3Wushz7OFH5f4lq/CmEeIsT/q2fZO0PtHdrtJQx0JR45oumiwbq',6,1,'2018-03-20 06:05:12','2018-03-20 08:19:05','staff2',3,4,'','Dean','default.png'),(21,'Test_image','Test_image','Test_image','12313','12313123','test_image@gmail.com','$2y$11$I1HWnu4Ym68PROSKeYuLa.3B4qFxL2sKafUuZQjiXTZIdelgEuRfy',1,1,'2018-03-20 08:18:46','2018-03-20 08:18:46','test_image',1,1,'','Student','default.png'),(22,'Test_image1','Test_image1','Test_image1','Test_image1','12313','test_image1@mail.com','$2y$11$y6PhNc1U5q4qIkRftbFCoel6PJikZyciAMcs8.yZaa1Th.U4FvlfC',1,1,'2018-03-20 08:23:55','2018-03-20 08:23:55','test_image1',1,1,'','Student','683d7aba961b310bfce2ca7ec088dd1f.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-20 17:22:51
