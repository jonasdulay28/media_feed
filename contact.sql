-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: media_feed
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trails`
--

DROP TABLE IF EXISTS `audit_trails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `geolocation` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trails`
--

LOCK TABLES `audit_trails` WRITE;
/*!40000 ALTER TABLE `audit_trails` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `contact_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'LSPU San Pablo','San Pablo City','2018-03-17 11:33:04','2018-03-22 09:12:12','111111'),(2,'LSPU Sta. Cruz','SSta Cruz','2018-03-17 12:04:59','2018-03-22 09:12:06','123'),(3,'LSPU Siniloan','Siniloan','2018-03-18 11:09:35','2018-03-22 09:12:20','51'),(4,'LSPU - HELLO','HELLO','2018-03-22 09:32:10','2018-03-22 09:32:24','555');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` longtext NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('qm19jl1gmgvlbfjmi41krfd4cuhbuj04','::1',1521629139,'__ci_last_regenerate|i:1521629139;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('kdi8fjqtfda7lv6jh5ffk8b3toge32lv','::1',1521628976,'__ci_last_regenerate|i:1521628976;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('r6ed9hpot2r5cpthl62jpnrvnj22vneg','::1',1521629314,'__ci_last_regenerate|i:1521629314;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('p02f4d96imc5qjf1v4h488lvtpiv1edn','::1',1521629499,'__ci_last_regenerate|i:1521629499;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1ep946slajcvea0iitgt52gu9hjmk879','::1',1521629641,'__ci_last_regenerate|i:1521629641;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('iocl4mbccm3ja4omsdau3a5rmfm9el9p','::1',1521629802,'__ci_last_regenerate|i:1521629802;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('qr7g9ulr13g1guhp3urns7234o43e8b5','::1',1521629947,'__ci_last_regenerate|i:1521629947;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('6sp4l21q3q9ubpofludvgagf277vdvo2','::1',1521630292,'__ci_last_regenerate|i:1521630292;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('i65ik1n07gaulg6ujim4jfs1u5mbip8k','::1',1521630302,'__ci_last_regenerate|i:1521630302;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qvnh9bb3duieet1qmgiln8avnl30v4a1','::1',1521630512,'__ci_last_regenerate|i:1521630292;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('5kku6uvmctvv8r436o5e6sqd2ij8kic8','::1',1521631192,'__ci_last_regenerate|i:1521631192;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('2vs8qb9mt777l7c300mkjb5h0lsi8k51','::1',1521631881,'__ci_last_regenerate|i:1521631881;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('uns37a8mt9dcoqsvbb6l18pmkc9fu137','::1',1521632940,'__ci_last_regenerate|i:1521632940;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('6c3m9qgtg2gvkuc16skfovhetkdubd63','::1',1521633294,'__ci_last_regenerate|i:1521633294;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gocnvbmulvess0tl3i8o6k9vp0mg9cr0','::1',1521633652,'__ci_last_regenerate|i:1521633652;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ro5lm5voh4ichdl1bgupgr1oa7psmbrk','::1',1521634050,'__ci_last_regenerate|i:1521634050;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('jl0f3n6us0jj8pugfcspp65j3d0vm9ug','::1',1521634352,'__ci_last_regenerate|i:1521634352;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('1v7ff3p29qp15nodtf4kjc9orlqkpcvu','::1',1521634684,'__ci_last_regenerate|i:1521634684;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('qjulpu3mfamen0n6nl669eb7hi7h2gah','::1',1521634988,'__ci_last_regenerate|i:1521634988;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('cfnq6hb8tj99ut1ce0ppgm0k55h1ekg9','::1',1521635364,'__ci_last_regenerate|i:1521635364;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('k2afla4rf4aqnp2615piiecj6469nom4','::1',1521635714,'__ci_last_regenerate|i:1521635714;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('nk3le65edlm6abbhudpjs2n76e5us9r9','::1',1521636024,'__ci_last_regenerate|i:1521636024;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('m2t8nv8pig7hvgh2hme1eqvk0sg8pbvh','::1',1521636348,'__ci_last_regenerate|i:1521636348;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('3givo31nocut3osib5gd5cpfkj5ls4eu','::1',1521636707,'__ci_last_regenerate|i:1521636707;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('clmhbb7skhsss7apb4kmi00mn8nsps5h','::1',1521637027,'__ci_last_regenerate|i:1521637027;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('t80m6ihpbcule4n7chjsihlum78ehbcb','::1',1521637356,'__ci_last_regenerate|i:1521637356;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('aostq1j3m8qvq04sqei5ev1j7e39eaeh','::1',1521637872,'__ci_last_regenerate|i:1521637872;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('25s2gg7c0kuuq69npl4hdvuo0pt72hen','::1',1521637463,'__ci_last_regenerate|i:1521637444;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('mc1r73eq62na9117sl76vhnqcjbougtb','::1',1521638187,'__ci_last_regenerate|i:1521638187;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('jodnpjg3dkbksivvgo203t5bpc5hh3tf','::1',1521638526,'__ci_last_regenerate|i:1521638526;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('8uc4gjig7qnq4909o3kscglvgm7s5169','::1',1521638973,'__ci_last_regenerate|i:1521638973;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('0m7igb8olug3sb0o2iit95lsaqg3jicj','::1',1521639295,'__ci_last_regenerate|i:1521639295;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('dj38qlf1pcve4a82323poe9jpv1tap0u','::1',1521639679,'__ci_last_regenerate|i:1521639679;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('fuj64uc0qijahljivqq0hsirc1e6l2ok','::1',1521640034,'__ci_last_regenerate|i:1521640034;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('0i71qj0m3914j5qf588osdus8aj6o4s3','::1',1521640438,'__ci_last_regenerate|i:1521640438;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('ugad6d6nh5oq5hgq1haiipkil4vcjvj8','::1',1521641468,'__ci_last_regenerate|i:1521641468;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('t611je6r62ucca0vj400ha9o7i808vr6','::1',1521640753,'__ci_last_regenerate|i:1521640753;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('ndgfut9e2g9kcpbke25e2q5pvs12oipt','::1',1521641097,'__ci_last_regenerate|i:1521641097;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('tfhf7spuksjqj220nd7s3ee7buekk0jv','::1',1521641430,'__ci_last_regenerate|i:1521641430;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('8n9676tultk8m2dne6d50innjdsv9d9m','::1',1521641748,'__ci_last_regenerate|i:1521641748;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('m57i6g79e3nbr286qf6naoqq5tbidbo5','::1',1521645027,'__ci_last_regenerate|i:1521645027;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('m45vbf9idb939mspg9brulfssgga7cj9','::1',1521642245,'__ci_last_regenerate|i:1521642245;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('4c9jij54kjph90l0hc41gaf4al0fe2d8','::1',1521642548,'__ci_last_regenerate|i:1521642548;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('sbnpt7odj7cgnpna8mtvdqmfrg0h6tt9','::1',1521642875,'__ci_last_regenerate|i:1521642875;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('8lspm9vr3pehg54413c267dlr011708k','::1',1521643188,'__ci_last_regenerate|i:1521643188;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('nb61bqblk5dm7a85e0qrqrd6la0lr34b','::1',1521643525,'__ci_last_regenerate|i:1521643525;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('lhf2trsunv5msfap8htev0ifdu6utgme','::1',1521643855,'__ci_last_regenerate|i:1521643855;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('8ra2bmokgfkk8t7pd28nscpfljd2pfgf','::1',1521644253,'__ci_last_regenerate|i:1521644253;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('e3e4darghbrsrpnn6jbpnohc2vs69ppe','::1',1521644556,'__ci_last_regenerate|i:1521644556;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('7ubuldm56or2en7v89c9395mmlgb5vbn','::1',1521644903,'__ci_last_regenerate|i:1521644903;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('a34f67arkudcc5akp6dqlb7m8qj8a3a3','::1',1521645290,'__ci_last_regenerate|i:1521645290;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('rbe4n4in8o565crff90urm0nq6gq7sli','::1',1521645329,'__ci_last_regenerate|i:1521645329;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('6o9lqvnbk8t5333idu7183sijrgpvrme','::1',1521645650,'__ci_last_regenerate|i:1521645650;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('00kl8e26p1a9q8qmh9f7jaaistk3f6s9','::1',1521645764,'__ci_last_regenerate|i:1521645764;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('2hg2idkpeoku0qoic1bov53cv2l5s5oj','::1',1521646913,'__ci_last_regenerate|i:1521646913;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('8a2of71d0rvmcljthssavka3vqiotu6m','::1',1521647434,'__ci_last_regenerate|i:1521647434;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('4aersu4i94tjvldftc85hvvt3lkr9b22','::1',1521647798,'__ci_last_regenerate|i:1521647798;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('j66ujndcl6l2th2u8srar6h84cksdpe9','::1',1521648651,'__ci_last_regenerate|i:1521648651;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('e0sba74qh71ie99v31kancn4ocilib8f','::1',1521648492,'__ci_last_regenerate|i:1521648492;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('an3o7449acrrrvbbqj4kh7gp8dogh3ic','::1',1521648803,'__ci_last_regenerate|i:1521648803;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('1f2am8edq8k8bnsi8b64o8qjho2247b2','::1',1521648747,'__ci_last_regenerate|i:1521648651;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('0ip2h81l1bvge9li36u212cabojrlg9n','::1',1521649232,'__ci_last_regenerate|i:1521649232;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('ieaus47n5plrj2pimmv2ra073csurolb','::1',1521649361,'__ci_last_regenerate|i:1521649361;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('gjcubht38he0vv7oi9atshce4p70ualu','::1',1521649533,'__ci_last_regenerate|i:1521649533;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('ofjhael2j98gnoc3ndfho1s9a5rnn1s1','::1',1521649776,'__ci_last_regenerate|i:1521649776;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('g676anojq9b6e06sb1b76dku8hlpfoje','::1',1521651719,'__ci_last_regenerate|i:1521651719;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('g603hnjcolh7iqism63l2earjkb6je8d','::1',1521651715,'__ci_last_regenerate|i:1521651715;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('lkgdqrqf7f4p895vmempcjji91rf8b4r','::1',1521652016,'__ci_last_regenerate|i:1521652016;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('m1jcl93kcp21t8ohf9vilodh9sfkk87l','::1',1521653030,'__ci_last_regenerate|i:1521653030;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('1bje27gq928scahf8vlsk10el2nc1k5l','::1',1521653106,'__ci_last_regenerate|i:1521653106;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('raqatv4bos40m7u37lc7jo9lg7fu6al8','::1',1521653916,'__ci_last_regenerate|i:1521653916;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('e8a7n3a6tergggdesh72itatl6ea29ia','::1',1521654110,'__ci_last_regenerate|i:1521654110;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('ercfuc6a1870e62r1itj1vr0rnltg78r','::1',1521654236,'__ci_last_regenerate|i:1521654236;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('91fjto244sgch4t5svpe4beofiq7lvid','::1',1521660450,'__ci_last_regenerate|i:1521660450;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('m7ktulsvrksqn828vlsl1sm3kq11lv9o','::1',1521654604,'__ci_last_regenerate|i:1521654604;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('behapjfj70v237kp7uvpdp5h9e4jl2ql','::1',1521655348,'__ci_last_regenerate|i:1521655348;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('d1650lm8eoqetu42dmqu9scu6n6f2qc3','::1',1521655827,'__ci_last_regenerate|i:1521655827;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('q1vulj3uq95dqug3m5o12u8dite7s5f8','::1',1521656981,'__ci_last_regenerate|i:1521656981;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('jqi85cu9e1i2ifc33fvgm57han3gr794','::1',1521657473,'__ci_last_regenerate|i:1521657473;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('mevfq4damupiorvaflnde4vafgqopcb3','::1',1521657934,'__ci_last_regenerate|i:1521657934;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qgqtv0gvnd7fhumja4d1s5b372gfrrjm','::1',1521658243,'__ci_last_regenerate|i:1521658243;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('3fqg9brjk189ts1s30v7ldbu8d6o4qnd','::1',1521658581,'__ci_last_regenerate|i:1521658581;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('lsbd6gmjig2nv9rkc5et8f2jg6ja20cl','::1',1521658888,'__ci_last_regenerate|i:1521658888;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('vqrj5c3ht4l7i4hc6g2a1dc2gu54m2q8','::1',1521659197,'__ci_last_regenerate|i:1521659197;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('skprq4f8lipijq25333bbj6lvhl1mrno','::1',1521659521,'__ci_last_regenerate|i:1521659521;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('q980hgdmggc6a7osgl4kr2hqrh1arje5','::1',1521659824,'__ci_last_regenerate|i:1521659824;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('ffe4ml8744e3eho9mgtskn3kksb91iu6','::1',1521660181,'__ci_last_regenerate|i:1521660181;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('n9taukf3km15a6ac3csl8lpos0ar30tn','::1',1521660538,'__ci_last_regenerate|i:1521660538;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('dlciq7bvlmtk9t4cgahake820lp1l35c','::1',1521660638,'__ci_last_regenerate|i:1521660450;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('ao3l0dklk1npjhhcb1ck74kfmgq8529l','::1',1521660642,'__ci_last_regenerate|i:1521660538;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('incpiou4j36bkid4075tee10qm896hhb','::1',1521681970,'__ci_last_regenerate|i:1521681970;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('jm4dcimmar2it6ve1vs83cqjshhiv9s4','::1',1521682464,'__ci_last_regenerate|i:1521682464;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('hdpqeva92dtsqq9qoc3mjlnoql4lvkr3','::1',1521682792,'__ci_last_regenerate|i:1521682792;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('o6fmevdgkdv1s91hk0gtmp2bm785gtr2','::1',1521682885,'__ci_last_regenerate|i:1521682885;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('nbaql06vtlf2e0u97ul94s340j0uuttj','::1',1521683126,'__ci_last_regenerate|i:1521683126;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('m73266h49597nqc5nu402pe8gu2f2jeo','::1',1521683211,'__ci_last_regenerate|i:1521683211;id|s:32:\"OUFpcVZrT2hiakUrVkpVSXJnZzlFUT09\";name|s:60:\"Tjd6QzZQS1FTa0RDaVFiZ3FSdmlnT3JZYy92emhLTWgxV1pBcTBPQXhQUT0=\";role|s:32:\"OEJZSmRsc1Q4MlZEV0N6T3FPeFZZQT09\";'),('tbe2t8s077k7ehkitlnnvadi7eb11lmc','::1',1521683605,'__ci_last_regenerate|i:1521683605;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('2s981bnq8cei7idd3jb305todvgl44e6','::1',1521683673,'__ci_last_regenerate|i:1521683673;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('nh9arotjipdfi2lelj9p8g1oopqc0ckc','::1',1521684833,'__ci_last_regenerate|i:1521684833;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qc0v6g1ir1lkq3904aogu53ithvp2lcq','::1',1521684345,'__ci_last_regenerate|i:1521684345;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('3l1h4r5jvvbulmujn8abtfb34vmijgd7','::1',1521684694,'__ci_last_regenerate|i:1521684694;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('r86g38t53t581bv6sfb04lnd39fq8cnn','::1',1521685338,'__ci_last_regenerate|i:1521685338;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('54rcqj1gl28s2h7arn80kp0bme2kv9bv','::1',1521685175,'__ci_last_regenerate|i:1521685175;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('qbb5bo45e68v32foulcdgs6n2a9oc2oh','::1',1521685001,'__ci_last_regenerate|i:1521685000;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('obl8b4a5s3p05b20s88t34vuoggdgp32','::1',1521685501,'__ci_last_regenerate|i:1521685501;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('q1ng02ets92rdu7nrbrsssijctbrujrg','::1',1521685881,'__ci_last_regenerate|i:1521685881;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('asefu1jnds23griqii1kr5jmmp93mutd','::1',1521686025,'__ci_last_regenerate|i:1521686025;id|s:32:\"dEl0N1JZdTAzamRRQWpWMjZaL0lqQT09\";name|s:60:\"UElia1NiWENCL1RBd2JXM2MzT1JzSkxkbTJTYzdIS2tDRFdla0ZwTkZ4ST0=\";role|s:32:\"Y01aMm9hTTMxU1l2OG1OZHRGWUt2UT09\";'),('74nuiu5b1nec8iu31slf260r5aat29kf','::1',1521686278,'__ci_last_regenerate|i:1521686278;id|s:32:\"cHFpZi91NG9zbXdkZUxMU09nVkNYQT09\";name|s:60:\"d29kTjdjKzNMc25MZ1ZQS2dpbFpEd1R4SmxBR3hKUGhPc3oyUXNtLzZvUT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('c7mom8opokt4merobte2pqu3pvuu2sfk','::1',1521686483,'__ci_last_regenerate|i:1521686483;id|s:32:\"dEl0N1JZdTAzamRRQWpWMjZaL0lqQT09\";name|s:60:\"UElia1NiWENCL1RBd2JXM2MzT1JzSkxkbTJTYzdIS2tDRFdla0ZwTkZ4ST0=\";role|s:32:\"Y01aMm9hTTMxU1l2OG1OZHRGWUt2UT09\";'),('g48muqd05cmn5vlt8ks0aq4d6mbq5rbm','::1',1521686666,'__ci_last_regenerate|i:1521686483;id|s:32:\"dEl0N1JZdTAzamRRQWpWMjZaL0lqQT09\";name|s:60:\"UElia1NiWENCL1RBd2JXM2MzT1JzSkxkbTJTYzdIS2tDRFdla0ZwTkZ4ST0=\";role|s:32:\"Y01aMm9hTTMxU1l2OG1OZHRGWUt2UT09\";'),('83677cph32tqjlc8fldqicij3d60da8g','::1',1521687091,'__ci_last_regenerate|i:1521687091;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('q926at8ts45rjftbtoavbpcsvkok2mvm','::1',1521689804,'__ci_last_regenerate|i:1521689804;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('3cl4bbrj0jm7484fjlleflkf46teoiba','::1',1521689792,'__ci_last_regenerate|i:1521689792;id|s:32:\"dEl0N1JZdTAzamRRQWpWMjZaL0lqQT09\";name|s:60:\"UElia1NiWENCL1RBd2JXM2MzT1JzSkxkbTJTYzdIS2tDRFdla0ZwTkZ4ST0=\";role|s:32:\"Y01aMm9hTTMxU1l2OG1OZHRGWUt2UT09\";'),('75iefg5dt7r8779u670alf39t27sqb53','::1',1521690580,'__ci_last_regenerate|i:1521690580;id|s:32:\"dEl0N1JZdTAzamRRQWpWMjZaL0lqQT09\";name|s:60:\"UElia1NiWENCL1RBd2JXM2MzT1JzSkxkbTJTYzdIS2tDRFdla0ZwTkZ4ST0=\";role|s:32:\"Y01aMm9hTTMxU1l2OG1OZHRGWUt2UT09\";'),('0bs51sgq6tfv81s4de8mo4dtnd63t99v','::1',1521689843,'__ci_last_regenerate|i:1521689804;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('aqfmc4c8p3imd3lolgfkshqij0v2608r','::1',1521690374,'__ci_last_regenerate|i:1521690364;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('u4nlcnk41t7lt7ukcj3qgd1ure1b14nj','::1',1521690732,'__ci_last_regenerate|i:1521690584;id|s:32:\"MlA5NVJ3bDh3Z1htRXRwSHowaG14dz09\";name|s:60:\"ZXJNaXMrN1g3ZmFQcGk0UDRYTGdsSGpIT3cwVFprYjZydXlETzBkUS9RVT0=\";role|s:32:\"bUxveWpaNk5CM3VxU2tJVCtZYy9ZZz09\";'),('i62ed9kmmij41sa135grnc1v3qjsspui','::1',1521698630,'__ci_last_regenerate|i:1521698629;'),('vca0sk9phsg7195kuuonj182lnkadolv','::1',1521709891,'__ci_last_regenerate|i:1521709891;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('17b5a4b94jeouljp6he0h9evjiq4uojk','::1',1521710290,'__ci_last_regenerate|i:1521710290;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('n71cr9ommiuv65djfit6vfjeurprcib1','::1',1521710638,'__ci_last_regenerate|i:1521710638;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('0uo7ukh1439ft80de2rr7vs15q62knkb','::1',1521710939,'__ci_last_regenerate|i:1521710939;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";'),('sh6ob1gm6cje7dft9fusb708on6j9q8k','::1',1521711144,'__ci_last_regenerate|i:1521710939;id|s:32:\"blh5K2g4QTVuNDV4bnBwbUZscXRmUT09\";name|s:60:\"ck1Cc3YxU0ZMRHRHcEdOWmZhbjJSNVhhRC9kRVF4L1ZGNTI4ZzRwd2R0cz0=\";role|s:32:\"b2tYaldpTFVyMGE3SXFYbGxvdUFuUT09\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_to` int(11) NOT NULL,
  `message_from` int(11) NOT NULL,
  `message` text NOT NULL,
  `message_file` varchar(100) DEFAULT NULL,
  `ticket` varchar(50) NOT NULL,
  `status` tinyint(2) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status_from` tinyint(2) DEFAULT NULL,
  `status_to` tinyint(2) DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,16,13,'ANG BONAK NG TEAMVIEWER','','5943271068',1,'2018-03-21 17:03:46',NULL,NULL,'Bonak TeamViewer','2018-03-21 18:06:41'),(2,13,16,'mag rereply ako bonak talaga ng team viewer!','','5943271068',1,'2018-03-21 17:06:22',0,1,'Bonak TeamViewer','2018-03-21 17:06:53'),(3,16,13,'test 123','','5943271068',1,'2018-03-21 17:08:23',1,1,'Bonak TeamViewer','2018-03-21 17:41:54'),(4,13,16,'latest na to!','','5943271068',1,'2018-03-21 17:42:02',0,1,'Bonak TeamViewer','2018-03-21 17:42:12'),(5,16,12,'new message','','9572016348',1,'2018-03-21 17:42:53',NULL,NULL,'new subject','2018-03-21 19:30:33'),(6,12,16,'rep','','9572016348',1,'2018-03-21 19:30:37',0,1,'new subject','2018-03-22 01:29:46'),(7,16,12,'asdadasdasdad','','9572016348',1,'2018-03-22 01:29:55',1,1,'new subject','2018-03-22 01:30:54'),(8,12,16,'asd123','','9572016348',1,'2018-03-22 01:31:05',0,1,'new subject','2018-03-22 01:34:12'),(9,13,16,'asdadada123','','5943271068',1,'2018-03-22 01:34:07',0,1,'Bonak TeamViewer','2018-03-22 01:56:41'),(10,13,16,'11111111111111111111111111111','','5943271068',1,'2018-03-22 01:34:22',0,1,'Bonak TeamViewer','2018-03-22 01:56:41'),(11,12,16,'1312313213','','9572016348',1,'2018-03-22 01:34:47',0,1,'new subject','2018-03-22 01:34:49'),(12,12,16,'asdasdasdasdasdasd','','9572016348',0,'2018-03-22 01:35:00',0,1,'new subject','2018-03-22 01:35:00'),(13,12,16,'zxcxczcxz','','9572016348',0,'2018-03-22 01:35:10',0,1,'new subject','2018-03-22 01:35:10'),(14,12,16,'hoy!!!!','','9572016348',0,'2018-03-22 01:35:29',0,1,'new subject','2018-03-22 01:35:29'),(15,22,13,'<p>asdasdasdasdas</p>','','9358120467',0,'2018-03-22 01:48:20',NULL,NULL,'asdasdasdasd','2018-03-22 01:48:20'),(16,17,13,'<p>asdadasgasagasdadqe</p>','','4905687321',1,'2018-03-22 02:09:08',NULL,NULL,'12313','2018-03-22 02:23:04'),(17,22,13,'123123123123','','9358120467',0,'2018-03-22 02:16:38',1,1,'asdasdasdasd','2018-03-22 02:16:38'),(18,13,16,'hoy','','5943271068',1,'2018-03-22 02:18:18',0,1,'Bonak TeamViewer','2018-03-22 02:20:21'),(19,13,16,'zzxdsad','','5943271068',1,'2018-03-22 02:19:39',0,1,'Bonak TeamViewer','2018-03-22 02:20:21'),(20,16,13,'problema mo','','5943271068',1,'2018-03-22 02:20:30',1,1,'Bonak TeamViewer','2018-03-22 02:33:30'),(21,22,13,'123131','','9358120467',0,'2018-03-22 02:20:46',1,1,'asdasdasdasd','2018-03-22 02:20:46'),(22,17,16,'<p><b>1231313</b></p>','MEDIA-FEED-SYSTEM.docx','1974823605',1,'2018-03-22 02:28:27',NULL,NULL,'hoy!','2018-03-22 02:28:34'),(23,18,16,'<p>123123123123</p>','','6752013849',0,'2018-03-22 02:33:56',NULL,NULL,'12313123123','2018-03-22 02:33:56'),(24,17,16,'<p>123123123123</p>','','3549108267',1,'2018-03-22 02:34:07',NULL,NULL,'12512515','2018-03-22 02:37:52'),(25,17,16,'<p>asdasdasdasd</p>','','2863149507',0,'2018-03-22 02:41:19',NULL,NULL,'new message','2018-03-22 02:41:19'),(26,17,16,'<p>asdadasd</p>','','1275386490',1,'2018-03-22 02:41:37',NULL,NULL,'newa again','2018-03-22 02:42:56'),(27,17,13,'<p>12313123</p>','','5026497183',0,'2018-03-22 02:42:30',NULL,NULL,'HOY! NEW MESSAGE','2018-03-22 02:42:30'),(28,17,13,'<p>asdadasdasdasd</p>','','2960457138',0,'2018-03-22 02:55:44',NULL,NULL,'TEST 2 MESSAGE','2018-03-22 02:55:44');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Accounting Office','2018-03-17 13:19:18','0000-00-00 00:00:00'),(2,'Administration Office','2018-03-17 13:19:31','0000-00-00 00:00:00'),(3,'Cashier\'s Office','2018-03-17 13:20:03','0000-00-00 00:00:00'),(4,'College of Law','2018-03-17 13:20:15','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `forward` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `name` varchar(100) NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'Docx','8a3f9487e1d4d0ac96eae8ca19741c29.docx','','2,3','2018-03-19 12:50:05','2018-03-20 06:01:09','Test Doc',1,13,0),(2,'Image','9dec3faff52a5cb9f331b8acc4e5882e.png','','3','2018-03-19 12:50:24','2018-03-20 06:01:09','Test Image',1,13,1),(3,'Text','','aaaaa','2,3','2018-03-19 12:50:39','2018-03-20 06:01:09','Test Text',1,13,1),(4,'Pdf','477c7dd9e5a6053d07a35b3e087d6ac3.pdf','','2','2018-03-19 12:52:18','2018-03-20 06:01:09','Test Pdf',1,13,0),(5,'Text','','12313123123','6','2018-03-20 06:58:03','0000-00-00 00:00:00','Hello World',1,14,1),(8,'Text','','312312312312','4,5,6','2018-03-20 07:01:57','0000-00-00 00:00:00','Admin Post',1,14,1),(9,'Text','','2313123123','2,3,4','2018-03-20 07:13:53','0000-00-00 00:00:00','Sub Admin Test',1,12,0),(10,'','','asdadasdasd','1,2,3','2018-03-20 09:48:25','0000-00-00 00:00:00','Aasdasdasd',1,13,1),(11,'Pdf','363b17388ebbba8f6bbb224bfa98ea6f.pdf','pdf \r\npdf \r\npdf pdf pdf','2','2018-03-20 09:53:47','0000-00-00 00:00:00','Pdf',1,13,0);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_admin`
--

DROP TABLE IF EXISTS `post_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_staff` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_admin`
--

LOCK TABLES `post_admin` WRITE;
/*!40000 ALTER TABLE `post_admin` DISABLE KEYS */;
INSERT INTO `post_admin` VALUES (1,14,4,'4,5','2018-03-20 05:53:09','2018-03-20 06:37:47'),(2,14,8,'4,5,6','2018-03-20 07:01:57','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `post_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_sub_admin`
--

DROP TABLE IF EXISTS `post_sub_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_sub_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `forward_to_admin` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_sub_admin`
--

LOCK TABLES `post_sub_admin` WRITE;
/*!40000 ALTER TABLE `post_sub_admin` DISABLE KEYS */;
INSERT INTO `post_sub_admin` VALUES (1,12,4,'2,3','2018-03-20 02:43:19','2018-03-20 03:36:09'),(2,12,1,'2,4','2018-03-20 02:48:27','2018-03-20 05:36:22');
/*!40000 ALTER TABLE `post_sub_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `username` varchar(50) DEFAULT NULL,
  `campus_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `position` varchar(30) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diana Jean','Malle','Mico','San Pablo','9206008947','admin@gmail.com','$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm',1,1,'2018-03-10 08:41:31','2018-03-20 08:19:05',NULL,0,0,'','','default.png'),(12,'Sub Admin','Sub Admin','Sub Admin','Asdsadasd','123123','sub_admin@gmail.com','$2y$11$.OOHHWwJvkow0u12iSozEOkB674SAM9/fPqxRuw67GgWplN/J9WVq',2,1,'2018-03-19 12:35:53','2018-03-20 08:19:05','sub_admin',2,2,'','Staff','default.png'),(13,'Super Admin','Super Admin','Super Admin','Super Admin','12313','super_admin@gmail.com','$2y$11$MctlsLbJCmp4lqD6mRMeJ.PsaK0DebE0KZZ5RTVGrLzUEBpKSs4mW',1,1,'2018-03-19 12:48:47','2018-03-20 08:19:05','super_admin',3,1,'','Director','default.png'),(14,'Admin','Admin','Admin','Admin Admin','123414','admin1@gmail.com','$2y$11$jPTpVKxuJ4uVEy/4fg8jEuQN7YLNMupGFID1QcpbTxZ7JfbMo0mmm',3,1,'2018-03-19 12:53:21','2018-03-20 08:19:05','admin',3,3,'','Director','default.png'),(15,'Sub Admin 1','Sub Admin 1','Sub Admin 1','Super Admin 123123','12313123','sub_admin1@gmail.com','$2y$11$eOixCY7VpyntM2mLiItc5eROqCnZTedonG03eD1Qe1H/SY8UJ72JO',2,1,'2018-03-19 12:56:11','2018-03-20 08:19:05','sub_admin1',3,2,'','Staff','default.png'),(16,'Admin2','Admin2','Admin2','Admin2','111111','admin2@gmail.com','$2y$11$dJxN73fHZvyNEcdgzrgxGOxXIWCFC6tDH0Gdg/diZB7XkVLvnnGYC',3,1,'2018-03-20 05:33:21','2018-03-20 08:19:05','123123123',2,1,'','Associate Dean','default.png'),(17,'Staff','Staff','Staff','Staffasdad','123123','staff@gmail.com','$2y$11$9awbNXhCM96ZQihdJ8FONuFpkGvUH8kbDW1usWFAWcod3Mm9DpeKO',6,1,'2018-03-20 06:03:58','2018-03-20 08:19:05','staff',2,1,'','Dean','default.png'),(18,'Staff2','Staff2','Staff2','Staff2','123121323','staff2@gmail.com','$2y$11$fc3Wushz7OFH5f4lq/CmEeIsT/q2fZO0PtHdrtJQx0JR45oumiwbq',6,1,'2018-03-20 06:05:12','2018-03-20 08:19:05','staff2',3,4,'','Dean','default.png'),(21,'Test_image','Test_image','Test_image','12313','12313123','test_image@gmail.com','$2y$11$I1HWnu4Ym68PROSKeYuLa.3B4qFxL2sKafUuZQjiXTZIdelgEuRfy',4,1,'2018-03-20 08:18:46','2018-03-22 03:51:44','test_image',3,3,'','Student','default.png'),(22,'Test_image1','Test_image1','Test_image1','Test_image1','12313','test_image1@mail.com','$2y$11$y6PhNc1U5q4qIkRftbFCoel6PJikZyciAMcs8.yZaa1Th.U4FvlfC',1,1,'2018-03-20 08:23:55','2018-03-20 08:23:55','test_image1',1,1,'','Student','683d7aba961b310bfce2ca7ec088dd1f.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-22 17:33:43
